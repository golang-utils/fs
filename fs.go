package fs

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"io"
	iofs "io/fs"
	"sort"
	"strings"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/updatechecker"
)

func init() {
	updatechecker.PrintVersionCheck("gitlab.com/golang-utils/fs")
}

type FileMode = iofs.FileMode

var SkipDir = iofs.SkipDir
var SkipAll = iofs.SkipAll

type WalkFunc func(p path.Relative, err error) error

const (
	DefaultDirMode  FileMode = 0750
	DefaultFileMode FileMode = 0640
)

func Base(fsys ReadOnly) path.Absolute {
	return fsys.Abs(path.Relative(""))
}

// MoveFile moves a file.
// If source and target filesystems are the same,
// the mountpoints of the source and target paths are the same
// and the filesystem is supporting the Moveable interface, the optimized Move method of the filesystem is used.
// Otherwise a copy of the file is made and the source file is only deleted, if the copying process was successfull.
// TODO check
func MoveFile(srcfs FS, srcFile path.Relative, trgtfs FS, trgDir path.Relative) error {
	if path.IsDir(srcFile) {
		return ErrExpectedFile.Params(srcFile)
	}

	if !path.IsDir(trgDir) {
		return ErrExpectedDir.Params(trgDir)
	}

	if srcfs == trgtfs {
		if mvFS, isMover := srcfs.(ExtMoveable); isMover {
			mp1, err1 := mvFS.Drive(srcFile)
			mp2, err2 := mvFS.Drive(trgDir)

			if err1 == nil && err2 == nil && mp1 == mp2 {
				return mvFS.Move(srcFile, trgDir)
			}
		}
	}

	err := CopyFileWithCheck(srcfs, srcFile, trgtfs, trgDir.Join(path.Name(srcFile)), Checksum)
	if err != nil {
		return err
	}

	srcfs.Delete(srcFile, false)
	return nil
}

func MkDir(fs ExtWriteable, rel path.Relative) error {
	if !path.IsDir(rel) {
		return ErrExpectedDir.Params(rel.String())
	}
	return fs.Write(rel, nil, false)
}

func MkDirAll(fs ExtWriteable, rel path.Relative) error {
	if !path.IsDir(rel) {
		return ErrExpectedDir.Params(rel.String())
	}
	return fs.Write(rel, nil, true)
}

// IsEmpty returns, if the given dir is empty or does not exist or is not a directory, but a file.
func IsEmpty(fsys ReadOnly, dir path.Relative) bool {
	if !path.IsDir(dir) {
		return true
	}

	if !fsys.Exists(dir) {
		return true
	}

	entries, err := ReadDirNames(fsys, dir)

	if err != nil {
		return true
	}

	return len(entries) == 0
}

// DirSize returns the size of the given directory.
// If the given path is not a directory or any error occurs,
// -1 is returned. If the given dir is empty, 0 is returned.
// TODO test
func DirSize(fsys ReadOnly, dir path.Relative) (size int64) {
	if !path.IsDir(dir) {
		return -1
	}

	if !fsys.Exists(dir) {
		return -1
	}

	entries, err := ReadDirPaths(fsys, dir)

	if err != nil {
		return -1
	}

	if len(entries) == 0 {
		return 0
	}

	for _, entry := range entries {
		if path.IsDir(entry) {
			dsize := DirSize(fsys, entry)
			if dsize == -1 {
				return -1
			}
			size += dsize
			continue
		}

		size += fsys.Size(entry)
	}

	return size
}

func ClearDir(fsys FS, dir path.Relative) error {
	paths, err := ReadDirPaths(fsys, dir)
	if err != nil {
		return err
	}

	var errM = errors.ErrMap{}

	for _, p := range paths {
		err_x := fsys.Delete(p, true)
		if err_x != nil {
			errM[p.String()] = err_x
		}
	}

	if len(errM) == 0 {
		return nil
	}

	return errM
}

// TODO: implement
func MoveDir(srcfs FS, srcDir path.Relative, trgtfs FS, trgDir path.Relative) error {
	return nil
}

// TODO implement
// TODO: first check, if there is enough space left
// CopyFile copies the given file of the given FS to the given target in the given target fs.
// srcfs and trgtfs may be the same. In that case, and if FS also implements the Mover interface and
// if srcFile and trgtFile are on the same Mountpoint the more efficient Mover.Move method is used
func CopyFile(srcfs FS, srcFile path.Relative, trgtfs FS, trgtFile path.Relative) error {
	if !srcfs.Exists(srcFile) {
		return ErrNotFound.Params(srcFile)
	}

	if trgtfs.Exists(trgtFile) {
		return ErrNotFound.Params(trgtFile)
	}

	rd, err := srcfs.Reader(srcFile)

	if err != nil {
		rd.Close()
		return ErrWhileReading.Params(srcFile, err)
	}

	return trgtfs.Write(trgtFile, rd, true)
}

func ReadFile(fs ReadOnly, f path.Relative) ([]byte, error) {
	if path.IsDir(f) || f.IsRoot() {
		return nil, ErrExpectedFile.Params(f)
	}

	if !fs.Exists(f) {
		return nil, ErrNotFound.Params(f)
	}

	rd, err := fs.Reader(f)
	if err != nil {
		if rd != nil {
			rd.Close()
		}
		return nil, err
	}

	bt, err := io.ReadAll(rd)
	if rd != nil {
		rd.Close()
	}
	return bt, err
}

// CreateFileFrom does return an error, if the file already exists or if the parent directory does not exist
func CreateFileFrom(fs ExtWriteable, f path.Relative, rd io.Reader) error {
	if path.IsDir(f) || f.IsRoot() {
		return ErrExpectedFile.Params(f)
	}

	if fs.Exists(f) {
		return ErrAlreadyExists.Params(f)
	}

	return WriteFileFrom(fs, f, rd, false)
}

// CreateFile does return an error, if the file already exists or if the parent directory does not exist
func CreateFile(fs ExtWriteable, f path.Relative, bt []byte) error {
	if path.IsDir(f) || f.IsRoot() {
		return ErrExpectedFile.Params(f)
	}

	if fs.Exists(f) {
		return ErrAlreadyExists.Params(f)
	}

	return WriteFile(fs, f, bt, false)
}

func WriteFileFrom(fs ExtWriteable, f path.Relative, rd io.Reader, recursive bool) error {
	if path.IsDir(f) || f.IsRoot() {
		return ErrExpectedFile.Params(f)
	}

	if rc, ok := rd.(io.ReadCloser); ok {
		return fs.Write(f, rc, recursive)
	}

	return fs.Write(f, ReadCloser(rd), recursive)
}

func WriteFile(fs ExtWriteable, f path.Relative, bt []byte, recursive bool) error {
	if path.IsDir(f) || f.IsRoot() {
		return ErrExpectedFile.Params(f)
	}

	return fs.Write(f, ReadCloser(bytes.NewReader(bt)), recursive)
}

// ReadDirNames returns the names of the dirs and files in a directory and returns them
func ReadDirNames(fs ReadOnly, dir path.Relative) ([]string, error) {
	// fmt.Printf("ReadDirNames called for %q\n", dir.String())
	if dir.IsRoot() {
		dir = path.Relative("")
	}

	dir = path.Relative(path.Clean(dir.String()))
	// fmt.Printf("after cleaning: %q\n", dir.String())
	if !dir.IsRoot() && !path.IsDir(dir) {
		return nil, ErrExpectedDir.Params(dir.String())
	}

	if !dir.IsRoot() && !fs.Exists(dir) {
		// fmt.Printf("not found for dirnames: %q\n", dir.String())
		return nil, ErrNotFound.Params(dir.String())
	}

	rd, err := fs.Reader(dir)
	// fmt.Printf("error while getting reader: %v\n", err)
	if err != nil {
		return nil, ErrWhileReading.Params(dir.String(), err)
	}

	bt, err := io.ReadAll(rd)
	// fmt.Printf("error while reading all: %v\n", err)
	rd.Close()

	if err != nil {
		return nil, ErrWhileReading.Params(dir.String(), err)
	}

	// fmt.Printf("DIRNAMES %q for %q\n", string(bt), dir.String())

	if len(bt) == 0 {
		return []string{}, nil
	}

	names := strings.Split(strings.TrimRight(string(bt), "\n"), "\n")
	sort.Strings(names)
	return names, nil
}

// ReadDirPaths returns the full paths (including dir) of the files and folders inside dir
func ReadDirPaths(fs ReadOnly, dir path.Relative) ([]path.Relative, error) {
	names, err := ReadDirNames(fs, dir)

	if err != nil {
		return nil, err
	}

	relPaths := make([]path.Relative, len(names))

	for i, name := range names {
		relPaths[i] = dir.Join(name)
	}

	return relPaths, nil
}

// CopyFileWithCheck is like CopyFile but doing another read to verify, that the checksum of both
// the src and target file matches. if the checksum does not match, the targetfile is removed
func CopyFileWithCheck(srcfs FS, srcFile path.Relative, trgtfs FS, trgtFile path.Relative, checksumFN func(io.Reader) (string, error)) error {
	// TODO: right now we are reading twice, it would be nicer to
	// read once and calculate the checksum at the same time, see
	// https://stackoverflow.com/questions/60328216/how-to-calculate-sha256-of-a-very-large-file-in-go

	rd1, err := srcfs.Reader(srcFile)
	if err != nil {
		return ErrWhileReading.Params(srcFile, err)
	}
	chk1, err := checksumFN(rd1)
	rd1.Close()
	if err != nil {
		return ErrWhileChecksumming.Params(srcFile, err)
	}

	err = CopyFile(srcfs, srcFile, trgtfs, trgtFile)
	if err != nil {
		return err
	}

	rd2, err := trgtfs.Reader(trgtFile)
	if err != nil {
		trgtfs.Delete(trgtFile, false)
		return ErrWhileReading.Params(trgtFile, err)
	}

	chk2, err := checksumFN(rd2)
	rd2.Close()
	if err != nil {
		return ErrWhileChecksumming.Params(trgtFile, err)
	}

	if chk1 != chk2 {
		trgtfs.Delete(trgtFile, false)
		return ErrChecksumNotMatch.Params(chk1, chk2)
	}

	return nil
}

// TODO implement
func CopyDir(srcfs FS, srcDir path.Relative, trgtfs FS, trgtDir path.Relative) error {
	return nil
}

// see https://stackoverflow.com/questions/60328216/how-to-calculate-sha256-of-a-very-large-file-in-go
func Checksum(data io.Reader) (checksum string, err error) {
	copyBuf := make([]byte, 1024*1024)

	h := sha256.New()
	if _, err := io.CopyBuffer(h, data, copyBuf); err != nil {
		return "", err
	}

	return hex.EncodeToString(h.Sum(nil)), nil
}

/*
	situativ kann geprüft werden, ob
*/

/*
	a new fs.LocalDir may only have one mountpoint (or we need to check in the move operation, or we have a moveable fs,
	that will only be enabled for fses within one mountpoint, maybe that is preferable, because also cloudservices may not allowing moving
	(e.g. S3 cross bucket movement))
*/

/*
// to use it like
func mkdir(mountPoint string, dir string) error {
	fsys := LocalDir(mountPoint)
	local := GetLocal(NewPath(dir))

	meta, err := local.GetMeta()

	_ = meta

	if err == nil {
		// dir already exists and it is a dir, since path ends with /
		return nil
	}

	if wr, ok := local.(Writeable); ok {
		// in the case of writing a dir, the data is ignored
		return wr.Write(nil, 0755)
	}
	return fmt.Errorf("not writeable")
}
*/
