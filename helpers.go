package fs

import (
	"io"
)

type readCloserWrap struct {
	io.Reader
	closed bool
}

func (r *readCloserWrap) Close() error {
	r.closed = true
	if rc, ok := r.Reader.(io.ReadCloser); ok {
		return rc.Close()
	}
	return nil
}

func (r *readCloserWrap) Read(bt []byte) (int, error) {
	if r.closed {
		return 0, io.ErrClosedPipe
	}

	return r.Reader.Read(bt)
}

func ReadCloser(rd io.Reader) io.ReadCloser {
	return &readCloserWrap{rd, false}
}

type readSeekCloserWrap struct {
	io.ReadSeeker
	closed bool
}

func (r *readSeekCloserWrap) Close() error {
	r.closed = true
	if rc, ok := r.ReadSeeker.(io.ReadSeekCloser); ok {
		return rc.Close()
	}
	return nil
}

func (r *readSeekCloserWrap) Read(bt []byte) (int, error) {
	if r.closed {
		return 0, io.ErrClosedPipe
	}

	return r.ReadSeeker.Read(bt)
}

func (r *readSeekCloserWrap) Seek(offset int64) error {
	if r.closed {
		return io.ErrClosedPipe
	}
	_, err := r.ReadSeeker.Seek(offset, 0)
	return err
}

func NewReadSeekCloser(rd io.ReadSeeker) ReadSeekCloser {
	return &readSeekCloserWrap{rd, false}
}
