package wrapfs

import (
	"bytes"
	"embed"
	"errors"
	"fmt"
	"io"
	iofs "io/fs"
	"os"
	"slices"
	"strings"
	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

var _ fs.ReadOnly = &FS{}
var _ fs.ExtGlob = &FS{}

type FS struct {
	FS   iofs.FS
	Base path.Local
}

// NewEmbed returns a wrapfs based on an embed.FS that resides in the given
// relative directory under the current working dir.
func NewEmbed(fsys embed.FS, dir path.Relative) (*FS, error) {
	if !path.IsDir(dir) {
		return nil, fs.ErrExpectedDir.Params(dir.String())
	}

	wd, err := path.ParseWD()

	if err != nil {
		return nil, err
	}

	if wd.Relative() != dir.Dir() {
		return nil, fmt.Errorf("%s is not a directory under the working directory %s", dir.String(), wd.String())
	}

	sfs, err := iofs.Sub(fsys, path.Name(dir))
	if err != nil {
		return nil, err
	}

	return New(sfs, wd.Join(path.Name(dir)))
}

func New(fsys iofs.FS, base path.Absolute) (*FS, error) {

	if !path.IsDir(base) {
		return nil, fs.ErrExpectedDir.Params(base)
	}

	var b path.Local
	b[0] = base.Head()
	b[1] = base.Relative().String()

	f := &FS{
		FS:   fsys,
		Base: b,
	}

	return f, nil
}

func (d *FS) Glob(pattern string) (matches []path.Relative, err error) {
	m, err := globWithLimit(d.FS, pattern, 0)
	if err != nil {
		return nil, err
	}

	slices.Sort(m)
	return m, nil
}

func (w *FS) Abs(r path.Relative) path.Absolute {
	return w.abs(r)
}

func (w *FS) abs(r path.Relative) path.Absolute {

	if r.IsRoot() {
		return w.Base
	}

	return w.Base.Join(r.String())

}

func (w *FS) Reader(p path.Relative) (io.ReadCloser, error) {

	if !w.Exists(p) {
		return nil, fs.ErrNotFound.Params(p.String())
	}

	if p.IsRoot() || path.IsDir(p) {

		dirstr := strings.TrimRight(p.Relative().String(), "/")

		if dirstr == "" {
			dirstr = "."
		}

		fis, err := iofs.ReadDir(w.FS, dirstr)
		if err != nil {
			return nil, fs.ErrWhileReading.Params(p, err)
		}

		var bf bytes.Buffer

		for _, fi := range fis {
			name := fi.Name()
			if fi.IsDir() {
				name += "/"
			}

			bf.WriteString(name + "\n")
		}

		return fs.ReadCloser(&bf), nil
	}

	f, err := w.FS.Open(p.Relative().String())

	if err != nil {
		f.Close()
		return nil, fs.ErrWhileReading.Params(p, err)
	}

	return f, nil
}

func (l *FS) Size(p path.Relative) int64 {
	if p.IsRoot() || path.IsDir(p) {
		return -2
	}

	if !l.Exists(p) {
		return -1
	}

	f, err := l.FS.Open(p.Relative().String())

	if err != nil {
		return -1
	}

	defer f.Close()

	st, err := f.Stat()

	if err != nil {
		return -1
	}

	return st.Size()
}

func (d *FS) GetMode(name path.Relative) (iofs.FileMode, error) {

	if !d.Exists(name) {
		return 0, fs.ErrNotFound.Params(name)
	}

	f, err := d.FS.Open(strings.TrimRight(name.Relative().String(), "/"))

	if f != nil {
		defer f.Close()
	}

	if err != nil {
		return 0, err
	}

	inf, err := f.Stat()

	if err != nil {
		return 0, err
	}

	return inf.Mode(), nil
}

func (l *FS) ModTime(p path.Relative) (t time.Time, err error) {

	if p.IsRoot() || path.IsDir(p) {
		return t, fs.ErrNotSupported.Params("ModTime for directories", l)
	}

	f, err := l.FS.Open(p.Relative().String())

	if err != nil {
		if errors.Is(err, iofs.ErrNotExist) {
			return t, fs.ErrNotFound
		}

		return t, fs.ErrWhileReading.Params(p, err)
	}

	defer f.Close()

	st, err := f.Stat()

	if err != nil {
		return t, err
	}

	return st.ModTime().UTC(), nil
}

var _ fs.ReadOnly = &FS{}

func (l *FS) Exists(name path.Relative) bool {
	if name.IsRoot() {
		return true
	}

	f, err := l.FS.Open(strings.TrimRight(name.Relative().String(), "/"))

	if f != nil {
		defer f.Close()
	}

	if errors.Is(err, os.ErrNotExist) || errors.Is(err, iofs.ErrNotExist) {
		return false
	}

	return err == nil
}
