package wrapfs_test

import (
	iofs "io/fs"
	"io/ioutil"
	"strings"

	"github.com/liamg/memoryfs"
	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

type Option func(*importer)

func OptionOverwrite() Option {
	return func(i *importer) {
		i.overwrite = true
	}
}

func OptionContinueOnError() Option {
	return func(i *importer) {
		i.continueOnError = true
	}
}

type importer struct {
	srcFS           fs.ReadOnly
	targetFS        *memoryfs.FS
	loc             path.Local
	overwrite       bool
	continueOnError bool
	errMap          errors.ErrMap
}

// shouldReturnError tracks the given error, if continueOnError is true and there is not already an error for the given path
// it returns !continueOnError
func (i *importer) shouldReturnError(p path.Relative, e error) (doReturnErr bool) {
	if e == nil {
		return false
	}
	if i.continueOnError {
		if _, has := i.errMap[p.String()]; !has {
			i.errMap[p.String()] = e
		}
	}
	return !i.continueOnError
}

func exists(fsys iofs.FS, name string) bool {
	_, err := iofs.Stat(fsys, name)
	if err == nil {
		return true
	}

	return false
}

func isDir(fsys iofs.FS, name string) bool {
	inf, err := iofs.Stat(fsys, name)
	if err != nil {
		return false
	}

	return inf.IsDir()
}

func (i *importer) iofsPath(p path.Relative) string {
	//res := strings.TrimRight(i.srcFS.Abs(p).Relative().String(), "/")
	res := strings.TrimRight(p.String(), "/")
	//fmt.Printf("cutting %q to %q\n", p.String(), res)
	return res
}

func (i *importer) existsInTarget(p path.Relative) bool {
	return exists(i.targetFS, i.iofsPath(p))
}

func (i *importer) importDir(dir path.Relative) error {

	items, err := fs.ReadDirPaths(i.srcFS, dir)

	if err != nil {
		return err
	}

	for _, it := range items {
		if path.IsDir(it) {
			if !i.existsInTarget(it) {
				//	if !i.targetFS.Exists(it) {
				errMk := i.targetFS.MkdirAll(i.iofsPath(it), 0755)
				//errMk := i.targetFS.Write(it, nil, true)
				if i.shouldReturnError(it, errMk) {
					return errMk
				}
			}
			errDr := i.importDir(it)
			if i.shouldReturnError(it, errDr) {
				return errDr
			}
			continue
		}

		//if i.targetFS.Exists(it) && !i.overwrite {
		if i.existsInTarget(it) && !i.overwrite {
			errEx := fs.ErrAlreadyExists.Params(it.String())
			if i.shouldReturnError(it, errEx) {
				return errEx
			}
		}

		rd, errRd := i.srcFS.Reader(it)
		if i.shouldReturnError(it, errRd) {
			return errRd
		}

		bt, errRd2 := ioutil.ReadAll(rd)
		rd.Close()

		if i.shouldReturnError(it, errRd2) {
			return errRd2
		}

		errWr := i.targetFS.WriteFile(i.iofsPath(it), bt, 0644)

		//errWr := i.targetFS.Write(it, rd, true)
		if i.shouldReturnError(it, errWr) {
			return errWr
		}
	}

	return nil
}

func (i *importer) imp() error {
	if !i.continueOnError {
		return i.importDir(path.Relative(""))
	}

	i.importDir(path.Relative(""))
	if len(i.errMap) == 0 {
		return nil
	}

	return i.errMap
}

/*
// Import imports all files and directories from srcfs to targetfs and stops and returns on the first error.
// On default it stops on the first error and returns it. however by passing OptionContinueOnError, the import continues and
// the collected errors are returned.
// On default it does return an error if a target file already exists, however by passing the option OptionOverwrite this is prevented
func Import(srcfs fs.ReadOnly, targetfs fs.ExtWriteable, opts ...Option) error {
	imp := &importer{
		srcFS:    srcfs,
		targetFS: targetfs,
	}

	for _, opt := range opts {
		opt(imp)
	}

	if imp.continueOnError {
		imp.errMap = errors.ErrMap{}
	}

	return imp.imp()
}
*/

func importFromReadonly(srcfs fs.ReadOnly, targetfs *memoryfs.FS, loc path.Local, opts ...Option) error {
	//memfs := memoryfs.New()

	imp := &importer{
		srcFS:    srcfs,
		targetFS: targetfs,
		loc:      loc,
	}

	for _, opt := range opts {
		opt(imp)
	}

	if imp.continueOnError {
		imp.errMap = errors.ErrMap{}
	}

	return imp.imp()

	/*
	   	if err := memfs.MkdirAll("my/dir", 0o700); err != nil {
	   	    panic(err)
	   	}

	   	if err := memfs.WriteFile("my/dir/file.txt", []byte("hello world"), 0o600); err != nil {
	   	    panic(err)
	   	}

	   data, err := fs.ReadFile(memfs, "my/dir/file.txt")

	   	if err != nil {
	   	    panic(err)
	   	}
	*/
}
