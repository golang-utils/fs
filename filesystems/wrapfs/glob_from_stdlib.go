package wrapfs

import (
	iofs "io/fs"
	gopath "path"
	"path/filepath"

	"gitlab.com/golang-utils/fs/path"
)

// This file is a changed copy of the stdlib of go 1.23, file io/fs/glob.go

// hasMeta reports whether path contains any of the magic characters
// recognized by path.Match.
func hasMeta(path string) bool {
	for i := 0; i < len(path); i++ {
		switch path[i] {
		case '*', '?', '[', '\\':
			return true
		}
	}
	return false
}

func globWithLimit(fsys iofs.FS, pattern string, depth int) (matches []path.Relative, err error) {
	//	fmt.Printf("globWithLimit %s\n", pattern)
	// This limit is added to prevent stack exhaustion issues. See
	// CVE-2022-30630.
	const pathSeparatorsLimit = 10000
	if depth > pathSeparatorsLimit {
		//	fmt.Printf("recursion too high: %v\n", depth)
		return nil, gopath.ErrBadPattern
	}
	if fsys, ok := fsys.(iofs.GlobFS); ok {
		//	fmt.Printf("using internal glob\n")
		rs, err := fsys.Glob(pattern)
		if err != nil {
			return nil, err
		}

		//		fmt.Printf("rs: %v\n", rs)

		// TODO: there is a bug: directories don't end in a slash
		for _, r := range rs {
			f, err := fsys.Open(r)
			if err != nil {
				return nil, err
			}
			st, err := f.Stat()
			f.Close()
			if err != nil {
				return nil, err
			}
			pth := filepath.ToSlash(r)
			if st.IsDir() {
				pth += "/"
			}
			matches = append(matches, path.Relative(pth))
		}
		return matches, nil
	}

	// Check pattern is well-formed.
	if _, err := gopath.Match(pattern, ""); err != nil {
		return nil, err
	}
	if !hasMeta(pattern) {
		st, err := iofs.Stat(fsys, pattern)
		if err != nil {
			return nil, nil
		}

		f := filepath.ToSlash(pattern)
		if st.IsDir() {
			f += "/"
		}

		return []path.Relative{path.Relative(f)}, nil
	}

	dir, file := gopath.Split(pattern)
	dir = cleanGlobPath(dir)

	if !hasMeta(dir) {
		return glob(fsys, dir, file, nil)
	}

	// Prevent infinite recursion. See issue 15879.
	if dir == pattern {
		return nil, gopath.ErrBadPattern
	}

	var m []path.Relative
	m, err = globWithLimit(fsys, dir, depth+1)
	if err != nil {
		return nil, err
	}
	for _, d := range m {
		matches, err = glob(fsys, d.ToGoPath(), file, matches)
		if err != nil {
			return
		}
	}
	return
}

// cleanGlobPath prepares path for glob matching.
func cleanGlobPath(path string) string {
	switch path {
	case "":
		return "."
	default:
		return path[0 : len(path)-1] // chop off trailing separator
	}
}

// glob searches for files matching pattern in the directory dir
// and appends them to matches, returning the updated slice.
// If the directory cannot be opened, glob returns the existing matches.
// New matches are added in lexicographical order.
func glob(fs iofs.FS, dir, pattern string, matches []path.Relative) (m []path.Relative, e error) {
	m = matches
	infos, err := iofs.ReadDir(fs, dir)
	if err != nil {
		return // ignore I/O error
	}

	//	fmt.Printf("infos: %#v\n", infos)

	for _, info := range infos {
		n := info.Name()

		matched, err := gopath.Match(pattern, n)
		if err != nil {
			return m, err
		}
		//	fmt.Printf("matched: %v %q\n", matched, n)
		if matched {
			rs := filepath.ToSlash(gopath.Join(dir, n))
			if info.IsDir() {
				rs += "/"
			}
			m = append(m, path.Relative(rs))
		}
	}

	//	fmt.Printf("matches: %v\n", m)
	return
}
