package wrapfs_test

import (
	"testing"

	"github.com/liamg/memoryfs"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/wrapfs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_readonly"
	"gitlab.com/golang-utils/fs/spec/specs/ext_glob"
)

func mustNew(prepare fs.ReadOnly, p path.Absolute) fs.ReadOnly {

	if _, ok := p.(*path.Remote); ok {
		panic("remote is not supported")
	}

	loc := path.MustLocal(p.String())
	fsys := memoryfs.New()
	err := importFromReadonly(prepare, fsys, loc)

	if err != nil {
		panic(err.Error())
	}

	f, err := wrapfs.New(fsys, loc)

	if err != nil {
		panic(err.Error())
	}

	return f
}

func mustNewGlob(prepare fs.ReadOnly, p path.Absolute) fs.ExtGlob {

	if _, ok := p.(*path.Remote); ok {
		panic("remote is not supported")
	}

	loc := path.MustLocal(p.String())
	fsys := memoryfs.New()
	err := importFromReadonly(prepare, fsys, loc)

	if err != nil {
		panic(err.Error())
	}

	f, err := wrapfs.New(fsys, loc)

	if err != nil {
		panic(err.Error())
	}

	return f
}

func TestSpec(t *testing.T) {
	var c spec.Config
	c.IsLocal = true
	c.Unsupported.DirSize = true
	c.Unsupported.ModTimeDir = true
	s := core_readonly.Spec(c, mustNew)
	s.Run("wrapfs/", t)
}

func TestSpecGlob(t *testing.T) {
	var c spec.Config
	c.IsLocal = true
	c.Unsupported.DirSize = true
	c.Unsupported.ModTimeDir = true
	s := ext_glob.Spec(c, mustNewGlob)
	s.Run("wrapfs/", t)
}
