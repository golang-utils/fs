package tempfs

import (
	"os"

	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/path"
)

/*
tempfs is a dirfs inside a temporary directory
*/

type FS struct {
	root path.Local
	*dirfs.FS
}

// New returns a filesystem that resides inside a temporary folder.
// After usage, you must call the Close() method in order to delete the files.
func New() (*FS, error) {
	d, err := os.MkdirTemp(os.TempDir(), "tempfs*")
	if err != nil {
		return nil, err
	}

	dir, err := path.ParseLocal(d + "/")

	if err != nil {
		os.RemoveAll(d)
		return nil, err
	}

	fsys := &FS{
		root: dir,
	}

	lfs, err := dirfs.New(dir)

	if err != nil {
		os.RemoveAll(d)
		return nil, err
	}

	fsys.FS = lfs
	return fsys, nil
}

func (fsys *FS) Close() error {
	return os.RemoveAll(path.ToSystem(fsys.root))
}
