package dirfs

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/importer"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_local"
)

func mustNew(prepare fs.ReadOnly, loc path.Local) fs.Local {
	f, err := New(loc)

	if err != nil {
		panic(err.Error())
	}

	err = importer.Import(prepare, f)

	if err != nil {
		panic(err.Error())
	}

	return f
}

func TestSpec(t *testing.T) {
	var c spec.Config
	c.Unsupported.DirSize = true
	c.Unsupported.ModTimeDir = true
	s := core_local.Spec(c, mustNew)
	s.Run("dirfs/", t)
}
