package dirfs

import (
	"errors"
	"fmt"
	"io"
	iofs "io/fs"
	"os"
	"strings"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/wrapfs"
	"gitlab.com/golang-utils/fs/path"
)

/*
	TODO: test windows netshare parsing etc
*/

/*
// a filesystem based on the working directory
func WD() (*FS, error) {
	wd, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	p, err := path.ParseLocal(wd + "/")

	if err != nil {
		return nil, err
	}

	return New(p)
}
*/

type Option func(*FS)

func OptMkRoot() Option {
	return func(fsys *FS) {
		fsys.createRootIfMissing = true
	}
}

type FS struct {
	*wrapfs.FS
	createRootIfMissing bool
}

var _ fs.Local = &FS{}

/*
func (d *FS) Glob2(pattern string) (matches []path.Relative, err error) {
	return globWithLimitNew(d.FS.FS, pattern, 0)
}
*/

type wrapReadSeek struct {
	io.ReadSeekCloser
}

func (w *wrapReadSeek) Seek(offset int64) error {
	_, err := w.ReadSeekCloser.Seek(offset, 0)
	return err
}

type wrapWriteSeek struct {
	f *os.File
}

func (w *wrapWriteSeek) Close() error {
	return w.f.Close()
}

func (w *wrapWriteSeek) Write(b []byte) (int, error) {
	return w.f.Write(b)
}

func (w *wrapWriteSeek) Seek(offset int64) error {
	_, err := w.f.Seek(offset, 0)
	return err
}

func (fsys *FS) WriteSeeker(p path.Relative) (fs.WriteSeekCloser, error) {
	if path.IsDir(p) {
		return nil, fs.ErrExpectedFile.Params(p.String())
	}

	fileStr := path.ToSystem(fsys.Abs(p))
	f, err := os.Open(fileStr)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fs.ErrNotFound.Params(p.String())
		}
		f.Close()
		return nil, err
	}
	return &wrapWriteSeek{f}, nil
}

func (fsys *FS) ReadSeeker(p path.Relative) (fs.ReadSeekCloser, error) {
	if path.IsDir(p) {
		return nil, fs.ErrExpectedFile.Params(p.String())
	}

	fileStr := path.ToSystem(fsys.Abs(p))
	f, err := os.Open(fileStr)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fs.ErrNotFound.Params(p.String())
		}
		f.Close()
		return nil, err
	}
	return &wrapReadSeek{f}, nil
}

func (d *FS) SetMode(name path.Relative, m fs.FileMode) error {
	if !d.Exists(name) {
		return fmt.Errorf("%s does not exist", name)
	}

	return os.Chmod(d.Abs(name).String(), m)
}

func (d *FS) FreeSpace(p path.Relative) int64 {
	var freeBytesAvailable uint64

	err := fs.FreeSpace(d.Base.Head(), &freeBytesAvailable)

	if err != nil {
		return -1
	}

	return int64(freeBytesAvailable)
}

func (d *FS) Drive(p path.Relative) (path.Local, error) {
	abs := d.Abs(p)
	loc, err := path.ParseLocal(abs.String())
	if err != nil {
		return path.Local{}, err
	}

	str, err := fs.Drive(loc)
	if err != nil {
		return path.Local{}, err
	}

	return path.ParseLocal(str)
}

// Move should return an error, if
// - the src does not exist
// - src is a directory and moving of directories is not supported by the fs
// - the resulting path trgDir.Join(path.Name(src)) does already exist
// - src and trgdir have different mountpoints
func (d *FS) Move(src path.Relative, trgDir path.Relative) error {
	if !d.Exists(src) {
		return fs.ErrNotFound.Params(src)
	}

	if !d.Exists(trgDir) {
		return fs.ErrNotFound.Params(trgDir)
	}

	mp1, err1 := d.Drive(src)
	mp2, err2 := d.Drive(trgDir)

	if err1 != nil {
		return err1
	}

	if err2 != nil {
		return err2
	}

	if mp1 != mp2 {
		return fmt.Errorf("%s and %s have different mountpoints (%s vs %s)", src, trgDir, mp1, mp2)
	}

	if d.Exists(trgDir.Join(path.Name(src))) {
		return fs.ErrAlreadyExists.Params(trgDir.Join(path.Name(src)))
	}

	oldfull := d.Abs(src).String()
	nufull := d.Abs(trgDir.Join(path.Name(src))).String()
	return os.Rename(oldfull, nufull)
}

func (d *FS) Write(name path.Relative, data io.ReadCloser, recursive bool) error {
	var perm fs.FileMode = fs.DefaultFileMode

	if path.IsDir(name) {
		perm = fs.DefaultDirMode
	}
	return d.WriteWithMode(name, data, perm, recursive)
}

// WriteWithMode reads from the given reader and writes all the content
// to the file of the given path. If the file already exists, if will be overwritten.
// Otherwise the file is newly created
// If name is a directory, data is ignored and the directory is created.
// If the directory is already there, nothing happens
func (d *FS) WriteWithMode(p path.Relative, rd io.ReadCloser, perm fs.FileMode, recursive bool) (err error) {
	if rd != nil {
		defer rd.Close()
	}

	if path.IsDir(p) && d.Exists(p) {
		return nil
	}

	full := d.Abs(p) //.String()

	if path.IsDir(p) {
		if recursive {
			err = os.MkdirAll(strings.TrimRight(full.String(), "/"), perm)
		} else {
			err = os.Mkdir(strings.TrimRight(full.String(), "/"), perm)
			if errors.Is(err, os.ErrNotExist) || errors.Is(err, iofs.ErrNotExist) {
				return fs.ErrNotFound.Params(p.Dir().String())
			}
		}
		if errors.Is(err, os.ErrExist) || errors.Is(err, iofs.ErrExist) {
			return nil
		}

		return err
	}

	if recursive {
		dir := d.Abs(p.Dir())
		os.MkdirAll(strings.TrimRight(dir.String(), "/"), 0755)
	} else {
		if !d.Exists(p.Dir()) {
			return fs.ErrNotFound.Params(p.Dir().String())
		}
	}

	// TODO read and write as you go (io.Copy)
	bt, err := io.ReadAll(rd)
	if err != nil {
		return err
	}

	return os.WriteFile(full.String(), bt, perm)
}

func (d *FS) Delete(p path.Relative, recursive bool) (err error) {
	if p.IsRoot() {
		return fs.ErrNotSupported.Params(d, "Deleting root dir")
	}

	if !d.Exists(p) {
		return nil
	}

	full := d.Abs(p).String()

	if recursive {
		err = os.RemoveAll(full)
	} else {
		err = os.Remove(full)
	}

	if errors.Is(err, os.ErrNotExist) || errors.Is(err, iofs.ErrNotExist) {
		return nil
	}

	//fmt.Printf("Delete called for %q recursive: %v err: %v\n", p, recursive, err)

	if !recursive && err != nil {
		//fmt.Println("XXXXX")
		return fs.ErrNotEmpty.Params(p.String())
	}

	return err
}

// Rename renames a file
// RenameFile renames a file. It is not allowed for directories
// and will also not move files, if the target is a directory.
// Renaming will fail, if there exists already a file under this name.
// Renaming will also fail, if the new file path is in a different directory
// should fail when the new name is not a name but a path
func (d *FS) Rename(old path.Relative, newName string) error {
	if strings.Contains(newName, "/") {
		return path.ErrInvalidName.Params(newName)
	}

	if path.IsDir(old) {
		return fs.ErrExpectedFile.Params(old)
	}

	if !d.Exists(old) {
		return fs.ErrNotFound.Params(old)
	}

	oldfull := d.Abs(old).String()
	dir := old.Dir()
	nu := dir.Join(newName)
	if d.Exists(nu) {
		return fs.ErrAlreadyExists.Params(nu)
	}

	nufull := d.Abs(nu).String()
	return os.Rename(oldfull, nufull)
}
func NewFromSystem(dir string) (ofs *FS, err error) {
	loc, err := path.ParseDirFromSystem(dir)
	if err != nil {
		return nil, err
	}

	return New(loc)
}

func New(dir path.Local, opts ...Option) (ofs *FS, err error) {
	if !path.IsDir(dir) {
		return nil, fs.ErrExpectedDir.Params(dir)
	}

	outer := &FS{}

	for _, opt := range opts {
		opt(outer)
	}

	st, err := os.Stat(path.ToSystem(dir))
	if err != nil {
		if os.IsNotExist(err) && outer.createRootIfMissing {
			err2 := os.MkdirAll(path.ToSystem(dir), 0755)
			if err2 != nil {
				return nil, err2
			}
		}
		return nil, err
	}

	if !st.IsDir() {
		return nil, fs.ErrExpectedDir.Params(dir)
	}

	/*
		from the os.DirFS documentation

				// Note that DirFS("/prefix") only guarantees that the Open calls it makes to the
				// operating system will begin with "/prefix": DirFS("/prefix").Open("file") is the
				// same as os.Open("/prefix/file"). So if /prefix/file is a symbolic link pointing outside
				// the /prefix tree, then using DirFS does not stop the access any more than using
				// os.Open does. Additionally, the root of the fs.FS returned for a relative path,
				// DirFS("prefix"), will be affected by later calls to Chdir. DirFS is therefore not
				// a general substitute for a chroot-style security mechanism when the directory tree
				// contains arbitrary content.
				//
				// The directory dir must not be "".
			   implements [io/fs.StatFS], [io/fs.ReadFileFS] and [io/fs.ReadDirFS]
	*/

	fsys := os.DirFS(path.ToSystem(dir))
	if fsys == nil {
		return nil, fmt.Errorf("could not make filesystem: invalid path: %q", dir)
	}

	wr, err := wrapfs.New(fsys, dir)

	if err != nil {
		return nil, err
	}

	outer.FS = wr

	return outer, nil
}
