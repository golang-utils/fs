package mapfs

import (
	gopath "path"

	"gitlab.com/golang-utils/fs"

	exportedIOFS "gitlab.com/golang-utils/fs/internal/original/iofs"
	"gitlab.com/golang-utils/fs/path"
)

// this is a modified copy from globWithLimit inside gitlab.com/golang-utils/fs/localfs/internal/original/iofs
// which is a copy of the functions with the same name of the go 1.22.2 standard lib io/fs
func globWithLimitNew(fsys *FS, pattern string, depth int) (matches []path.Relative, err error) {
	// This limit is added to prevent stack exhaustion issues. See
	// CVE-2022-30630.
	const pathSeparatorsLimit = 10000
	if depth > pathSeparatorsLimit {
		return nil, gopath.ErrBadPattern
	}

	// there is no implementation, this is the implementation
	/*
		if fsys, ok := fsys.(iofs.GlobFS); ok {
			return fsys.Glob(pattern)
		}
	*/

	// Check pattern is well-formed.
	if _, err := gopath.Match(pattern, ""); err != nil {
		return nil, err
	}
	if !exportedIOFS.HasMeta(pattern) {
		var fPath path.Relative
		switch {
		case fsys.Exists(path.Relative(pattern)):
			fPath = path.Relative(pattern)
		case fsys.Exists(path.Relative(pattern + "/")):
			fPath = path.Relative(pattern + "/")
		default:
			return nil, nil
		}

		return []path.Relative{fPath}, nil
	}

	dir, file := gopath.Split(pattern)
	dir = exportedIOFS.CleanGlobPath(dir)

	if !exportedIOFS.HasMeta(dir) {
		return globNew(fsys, path.Relative(dir+"/"), file, nil)
	}

	// Prevent infinite recursion. See issue 15879.
	if dir == pattern || dir+"/" == pattern {
		return nil, gopath.ErrBadPattern
	}

	var m []path.Relative
	m, err = globWithLimitNew(fsys, dir, depth+1)
	if err != nil {
		return nil, err
	}
	for _, d := range m {
		matches, err = globNew(fsys, d, file, matches)
		if err != nil {
			return
		}
	}
	return
}

// this is a modified copy from glob inside gitlab.com/golang-utils/fs/localfs/internal/original/iofs
// which is a copy of the functions with the same name of the go 1.22.2 standard lib io/fs
func globNew(fsys *FS, dir path.Relative, pattern string, matches []path.Relative) (m []path.Relative, e error) {
	m = matches

	infos, err := fs.ReadDirPaths(fsys, dir)
	if err != nil {
		return // ignore I/O error
	}

	for _, info := range infos {
		n := path.Name(info)
		matched, err := gopath.Match(pattern, n)
		if err != nil {
			return m, err
		}
		if matched {
			//m = append(m, gopath.Join(dir, n))
			m = append(m, dir.Join(path.Base(info)))
		}
	}
	return
}
