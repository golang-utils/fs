package mapfs

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

func mustNewMapFS(absPath string) *FS {
	loc, err := path.ParseLocal(absPath)

	if err != nil {
		panic(err.Error())
	}

	fs, err := New(loc)

	if err != nil {
		panic(err.Error())
	}

	return fs
}

func TestMapHasAllDirs(t *testing.T) {

	tests := []struct {
		create   string
		check    string
		expected bool
	}{
		{"a/b/", "a/b/", true},
		{"a/b/", "a/b/c/", false},
		{"a/", "a/b/c/", false},
		{"a/b/c/", "a/b/", true},
	}

	for i, test := range tests {
		m := mustNewMapFS("/map/")
		err := fs.MkDirAll(m, path.Relative(test.create))

		if err != nil {
			t.Fatalf("[%v] could not create %s: %v", i, test.create, err)
		}

		//got := m.Exists(path.Relative(test.check))
		got := m.HasAllDirs(path.Relative(test.check))

		if got != test.expected {
			t.Errorf("[%v] Create(%q) hasAllDirs(%q) returns %v // expected %v", i, test.create, test.check, got, test.expected)
		}
	}
}

func TestMapMkdirall(t *testing.T) {
	tests := []struct {
		create string
		check  []string
	}{
		{"a/b/", []string{"a/", "a/b/"}},
		{"a/b/c/d/e/f/", []string{"a/", "a/b/", "a/b/c/", "a/b/c/d/", "a/b/c/d/e/", "a/b/c/d/e/f/"}},
	}

	for i, test := range tests {
		m := mustNewMapFS("/map/")
		err := m.Mkdirall(path.Relative(test.create))

		if err != nil {
			t.Fatalf("[%v] could not create %s: %v", i, test.create, err)
		}

		for _, sub := range test.check {
			_, has := m.Files[path.Relative(sub)]

			if !has {
				t.Errorf("[%v] mkdirall %q did not create %q", i, test.create, sub)
			}
		}

	}
}
