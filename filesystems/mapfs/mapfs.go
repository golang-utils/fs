package mapfs

import (
	"bytes"
	"fmt"
	"io"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	//	"gitlab.com/golang-utils/fs/path"
)

/*
	Package mapfs implements a filesystem that is based on a map
	and can be used for all applications of the fs package as well as for testing.
*/

var _ fs.TestFS = &FS{}

type File struct {
	FileMode fs.FileMode
	Meta     map[string][]byte
	ModTime  time.Time
	Data     []byte
}

// das könnte man zum kopieren bzw. synchronisieren nehmen
type FS struct {
	Base      path.Local
	URL       *url.URL
	Files     map[path.Relative]*File
	MaxSize   int64
	UsedBytes int64
}

func New(abs path.Absolute) (*FS, error) {
	//fmt.Printf("creating mapfs for %s\n", abs.String())
	if !path.IsDir(abs) {
		return nil, fs.ErrExpectedDir.Params(abs)
	}

	m := &FS{
		Files: map[path.Relative]*File{},
	}

	if rem, isRem := abs.(*path.Remote); isRem {
		m.URL = rem.URL
	} else {
		var a path.Local
		a[0] = abs.Head()
		a[1] = abs.Relative().String()
		m.Base = a
	}

	return m, nil
}

func (fsys *FS) Glob(pattern string) (matches []path.Relative, err error) {
	return globWithLimitNew(fsys, pattern, 0)
}

func (f *FS) Scheme() string {
	if f.URL == nil {
		return ""
	}

	return f.URL.Scheme
}

func (f *FS) HostName() string {
	if f.URL == nil {
		return ""
	}

	return f.URL.Hostname()
}

func (f *FS) Port() int {
	if f.URL == nil {
		return 0
	}

	port := f.URL.Port()

	if f.URL.Port() == "" {
		return 0
	}

	po, err := strconv.Atoi(port)

	if err != nil {
		return 0
	}

	return po
}

func (f *FS) UserName() string {
	if f.URL == nil {
		return ""
	}

	if f.URL.User == nil {
		return ""
	}

	return f.URL.User.Username()
}

func (f *FS) Password() string {
	if f.URL == nil {
		return ""
	}

	if f.URL.User == nil {
		return ""
	}

	pw, ok := f.URL.User.Password()

	if !ok {
		return ""
	}

	return pw
}

func (d *FS) add(name path.Relative, data []byte, perm fs.FileMode, meta map[string][]byte) {
	d.Files[name] = &File{
		ModTime:  time.Now(),
		Meta:     meta,
		Data:     data,
		FileMode: perm,
	}
	d.UsedBytes += int64(len(data))
}

func (d *FS) FreeSpace(p path.Relative) int64 {
	if d.MaxSize == 0 {
		return 1024 * 1024 * 1024
	}

	if d.UsedBytes >= d.MaxSize {
		return 0
	}
	return d.UsedBytes - d.MaxSize
}

func (d *FS) GetMode(name path.Relative) (fs.FileMode, error) {
	f, has := d.Files[name]
	if !has {
		return 0, fs.ErrNotFound.Params(name)
	}

	return f.FileMode, nil
}

func (d *FS) Size(name path.Relative) int64 {
	if name.IsRoot() || path.IsDir(name) {
		return -2
	}
	f, has := d.Files[name]
	if !has {
		return -1
	}

	return int64(len(f.Data))
}

func (d *FS) SetMode(name path.Relative, m fs.FileMode) error {
	f, has := d.Files[name]
	if !has {
		return fs.ErrNotFound.Params(name)
	}

	f.FileMode = m
	d.Files[name] = f
	return nil
}

func (m *FS) GetMeta(name path.Relative) (map[string][]byte, error) {
	f, has := m.Files[name]
	if !has {
		return nil, fs.ErrNotFound
	}
	return f.Meta, nil
}

func (m *FS) SetMeta(name path.Relative, meta map[string][]byte) error {
	f, has := m.Files[name]
	if !has {
		return fs.ErrNotFound
	}
	f.Meta = meta
	return nil
}

func (m *FS) Drive(path.Relative) (path.Local, error) {
	return m.Base, nil
}

// TODO test
func (m *FS) Move(src path.Relative, targetFolder path.Relative) error {
	trg := targetFolder.Join(path.Name(src))
	if _, has := m.Files[trg]; has {
		return fs.ErrAlreadyExists.Params(trg)
	}

	bt, has := m.Files[src]
	if !has {
		return fs.ErrNotFound.Params(src)
	}

	delete(m.Files, src)
	m.Files[trg] = bt
	return nil
}

func (fsys *FS) ReadSeeker(p path.Relative) (fs.ReadSeekCloser, error) {
	if path.IsDir(p) {
		return nil, fs.ErrExpectedFile.Params(p.String())
	}

	f, has := fsys.Files[p]
	if !has {
		return nil, fs.ErrNotFound
	}

	rs := bytes.NewReader(f.Data)
	return fs.NewReadSeekCloser(rs), nil
}

type wrapWriteSeek struct {
	f      []byte
	offset int64
	fsys   *FS
	p      path.Relative
}

func (w *wrapWriteSeek) Close() error {
	return fs.WriteFile(w.fsys, w.p, w.f, false)
}

func (w *wrapWriteSeek) Write(b []byte) (int, error) {
	var bf = bytes.NewBuffer(w.f[:w.offset])
	written, err := io.Copy(bf, bytes.NewReader(b))
	var err2 error
	if w.offset+written < int64(len(w.f)) {
		_, err2 = io.Copy(bf, bytes.NewReader(w.f[w.offset+written:]))
	}

	if err != nil {
		return 0, err
	}

	if err2 != nil {
		return 0, err2
	}

	w.f = bf.Bytes()

	return int(written), nil
}

func (w *wrapWriteSeek) Seek(offset int64) error {
	if offset < 0 {
		return fmt.Errorf("invalid offset: %v", offset)
	}

	if offset > int64(len(w.f)) {
		return fmt.Errorf("invalid offset: %v larger than file size %v", offset, len(w.f))
	}
	w.offset = offset
	return nil
}

func (fsys *FS) WriteSeeker(p path.Relative) (fs.WriteSeekCloser, error) {
	if path.IsDir(p) {
		return nil, fs.ErrExpectedFile.Params(p.String())
	}

	f, has := fsys.Files[p]
	if !has {
		return nil, fs.ErrNotFound
	}

	ws := &wrapWriteSeek{
		f:      f.Data,
		offset: 0,
		fsys:   fsys,
		p:      p,
	}

	return ws, nil
}

func (m *FS) Reader(p path.Relative) (io.ReadCloser, error) {
	if !m.Exists(p) {
		return nil, fs.ErrNotFound.Params(p.String())
	}
	var bf bytes.Buffer

	if p.IsRoot() || path.IsDir(p) {
		// find all subdirs and files
		pstr := p.String()
		for k := range m.Files {
			//kstr := k.String()

			if k.String() == "/" {
				continue
			}

			if k.Dir().String() == pstr {
				bf.WriteString(path.Base(k) + "\n")
			}

			/*
				if kstr == pstr {
					continue
				}

				if strings.HasPrefix(kstr, pstr) {
					rel, err := filepath.Rel(pstr, kstr)
					if err == nil {
						if strings.Contains(rel, "/") {
							continue
						}
						bf.WriteString(rel + "\n")
					}
					continue
				}
			*/
		}

		return fs.ReadCloser(strings.NewReader(bf.String())), nil
	}

	f, has := m.Files[p]
	if !has {
		return nil, fs.ErrNotFound
	}

	return fs.ReadCloser(bytes.NewReader(f.Data)), nil
}

func (m *FS) Write(p path.Relative, rd io.ReadCloser, recursive bool) (err error) {
	var perm = fs.DefaultFileMode
	if path.IsDir(p) {
		perm = fs.DefaultDirMode
	}

	return m.WriteWithMode(p, rd, perm, recursive)
}

func (m *FS) WriteWithMeta(p path.Relative, rd io.ReadCloser, meta map[string][]byte, recursive bool) error {
	return m.write(p, rd, 0, meta, recursive)
}

func (m *FS) WriteWithMode(p path.Relative, rd io.ReadCloser, perm fs.FileMode, recursive bool) (err error) {
	return m.write(p, rd, perm, nil, recursive)
}

// we don't need to write all directories, since they are just implicit
// func (m *MapFS) Write(p path.Relative, rd io.ReadCloser, recursive bool) (err error) {
func (m *FS) write(p path.Relative, rd io.ReadCloser, perm fs.FileMode, meta map[string][]byte, recursive bool) (err error) {
	/*
		if p.IsRoot() {
			return fs.ErrNotSupported.Params("writing root of fs")
		}
	*/
	if path.IsDir(p) && recursive {
		return m.Mkdirall(p)
	}

	if recursive {
		err := m.Mkdirall(p.Dir())

		if err != nil {
			return err
		}
	}

	if !p.Dir().IsRoot() && !m.HasAllDirs(p.Dir()) {
		//return fmt.Errorf("could not write %s directory %s does not exist", p, p.Dir())
		return fs.ErrNotFound.Params(p.Dir().String())
	}

	var bt []byte

	if rd != nil {
		bt, err = io.ReadAll(rd)
		rd.Close()
		if err != nil {
			return err
		}
	}

	m.add(p, bt, perm, meta)
	return nil
}

func (m *FS) HasAllDirs(p path.Relative) bool {
	if !path.IsDir(p) {
		p = p.Dir()
	}

	parts := strings.Split(p.String(), "/")

	//for i, pa := range parts {
	for i := range parts {
		compl := strings.Join(parts[:i], "/") + "/"

		_, has := m.Files[path.Relative(compl)]
		if !has {
			return false
		}
	}

	return true
}

func (m *FS) Mkdirall(p path.Relative) error {
	/*
		if p.IsRoot() {
			return fs.ErrNotSupported.Params("writing root of fs", m)
		}
	*/
	if !path.IsDir(p) {
		return fs.ErrExpectedDir.Params(p)
	}

	parts := strings.Split(p.String(), "/")

	//for i, pa := range parts {
	for i := range parts {
		compl := strings.Join(parts[:i], "/") + "/"

		_, has := m.Files[path.Relative(compl)]
		if !has {
			m.add(path.Relative(compl), nil, fs.DefaultDirMode, nil)
			//m.Files[path.Relative(compl)] = nil
		}
	}

	return nil
}

// recursive means, that the content (files and folders) with be also deleted
func (m *FS) Delete(p path.Relative, recursive bool) error {
	if p.IsRoot() {
		return fs.ErrNotSupported.Params("Deleting root directory", m)
	}

	if path.IsDir(p) {
		pstr := p.String()

		if recursive {
			for k := range m.Files {
				kstr := k.String()

				if kstr == pstr {
					delete(m.Files, k)
					continue
				}

				if strings.HasPrefix(kstr, pstr) {
					m.UsedBytes -= int64(len(m.Files[k].Data))
					delete(m.Files, k)
					continue
				}
			}
		} else {

			for k := range m.Files {
				kstr := k.String()

				if kstr != pstr && strings.HasPrefix(kstr, pstr) {
					return fs.ErrNotEmpty.Params(pstr)
				}
			}

			delete(m.Files, p)
		}
		return nil
	}

	if f, has := m.Files[p]; has {
		m.UsedBytes -= int64(len(f.Data))
	}

	delete(m.Files, p)
	return nil
}

// should fail when the new name is not a name but a path
func (m *FS) Rename(p path.Relative, name string) error {
	if path.IsDir(p) {
		return fs.ErrNotSupported.Params("renaming of directories", m)
		//return fmt.Errorf("renaming of directories not supported (would require to change all the paths of the files beneath)")
	}

	if strings.Contains(name, "/") {
		return path.ErrInvalidName.Params(name)
	}

	data, has := m.Files[p]
	if !has {
		return fs.ErrNotFound.Params(p)
	}

	newp := p.Dir().Join(name)

	delete(m.Files, p)
	m.Files[newp] = data
	return nil
}

func (m *FS) ModTime(p path.Relative) (t time.Time, err error) {
	f, has := m.Files[p]
	if !has {
		return t, fs.ErrNotFound.Params(p)
	}
	return f.ModTime, nil
}

func (m *FS) Abs(r path.Relative) path.Absolute {
	if m.URL != nil {
		if r.IsRoot() {
			return path.MustRemote(m.URL.String())
		}
		return path.MustRemote(m.URL.JoinPath(r.String()).String())
	}

	if r.IsRoot() {
		return m.Base
	}
	return m.Base.Join(r.String())
}

func (t *FS) Exists(p path.Relative) bool {
	if p.IsRoot() {
		return true
	}
	_, has := t.Files[p]
	return has
}

// TODO collect the errors instead of directly aborting
func (t *FS) WriteTo(fsys fs.FS) error {
	for k, f := range t.Files {
		if !path.IsDir(k) {
			d := k.Dir()
			fsys.Write(d, nil, true)
			err := fsys.Write(k, fs.ReadCloser(bytes.NewReader(f.Data)), false)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// TODO collect the errors instead of directly aborting
func (t *FS) ReadFrom(fsys fs.FS, p path.Relative) error {
	if !path.IsDir(p) {
		return fmt.Errorf("%s is no dir", p)
	}

	entries, err := fs.ReadDirPaths(fsys, p)

	if err != nil {
		return fs.ErrWhileReading.Params(p, err)
	}

	for _, entry := range entries {
		if !path.IsDir(p) {
			rd, err2 := fsys.Reader(entry)
			if err2 != nil {
				return err2
			}

			bt, err3 := io.ReadAll(rd)

			if err3 != nil {
				return err3
			}
			t.add(entry, bt, fs.DefaultFileMode, nil)
		} else {
			t.add(entry, nil, fs.DefaultDirMode, nil)
			err2 := t.ReadFrom(fsys, entry)
			if err2 != nil {
				return err2
			}
		}
	}
	return nil
}
