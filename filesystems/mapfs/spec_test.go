package mapfs_test

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mapfs"
	"gitlab.com/golang-utils/fs/importer"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_testfs"
)

func mustNewTestfs(prepare fs.ReadOnly, p path.Absolute) fs.TestFS {
	f, err := mapfs.New(p)

	if err != nil {
		panic(err.Error())
	}

	err = importer.Import(prepare, f)

	if err != nil {
		panic(err.Error())
	}

	return f
}

func mustNewGlob(prepare fs.ReadOnly, p path.Absolute) fs.ExtGlob {
	f, err := mapfs.New(p)

	if err != nil {
		panic(err.Error())
	}

	err = importer.Import(prepare, f)

	if err != nil {
		panic(err.Error())
	}

	return f
}

func TestSpecTestFS(t *testing.T) {
	var c spec.Config
	c.Unsupported.DirSize = true
	s := core_testfs.Spec(c, mustNewTestfs)
	s.Run("mapfs/", t)
}
