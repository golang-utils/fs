package rootfs

import (
	"gitlab.com/golang-utils/fs/path"
)

/*
IDEA

have a special flavor of a local filesystem, that is behaving like the root filesystem tree in unix,
but also works on windows. The idea is to be able to use a single filesystem also for all windows drives to programm and test against.

In order to make that happen, we need 3 things (on windows):

- mixfs where we have one mounted fs for every drive letter
- have a mapping between mountpoints and drive letters on the first level,
  e.g.
	  c/...  goes to C:\
	  d/...  goes to D:\
		etc.
- have support to convert a local path to this pseudo-relative path, we call it root-path.
  this support must go into the path library

on unix, we can just use a normal local fs

Since we want it to work seemingless for the user, we'll compile flags.
We want the rootfs always be of the same type. it is more or less a copy of mixfs, only for the localfs methods, instead of plain FS.
Also we want to call the same functions on paths on every os.

first implementation will use mixfs to make it easier-

So full circle would be (windows):

str1 := `c:\Windows\Temp`
dir1 := path.MustLocal(str1 + "/")
rel1 := dir1.RootRelative() // now a relative path that can be used with any filesystem and everywhere, so you can now also replace the fs with a mockfs
str2 := `d:\Pictures`
dir2 := path.MustLocal(str2 + "/")
rel2 := dir2.RootRelative() // now a relative path yada yada  "d/Pictures/"

// from here on (with the relative paths) you could use any fs, e.g. for testing
// you only need the rootfs, if you actually want to work on the real local fs in rooted manner
fsys, _ := New()

// any of this relative paths you can bring back to the system

// and with this root fs you can also get the original paths back
str1a := fs.ToSystem(fsys,rel1) // `c:\Windows\Temp`
str2a := fs.ToSystem(fsys,rel2) // `d:\Pictures`


*/

type FS struct {
	// *mixfs.FS
	*mixFS
}

func (fsys *FS) Abs(p path.Relative) path.Absolute {
	// if p.IsRoot() && !OnWindows() {
	if p.IsRoot() {
		return path.MustLocal("/")
	}
	return fsys.mixFS.Abs(p)
}

// ParsePath parses an existing local path and returns a normalized path
// that is relative to rootfs
// the path might be absolute or relative (than it is relative to the current working directory)
// the given path must not end in a slash.
// if p must not be an empty string, or .
func ParsePath(p string) (r path.Relative, err error) {
	loc, err := path.ParseDirFromSystem(p)
	if err != nil {
		return r, err
	}
	return loc.RootRelative(), nil
}

// MustPath is like ParsePath, only that it panics, if the given path is not valid
func MustPath(p string) (r path.Relative) {
	r, err := ParsePath(p)
	if err != nil {
		panic(err.Error())
	}
	return r
}

// ParseWD parses the working dir and returns a normalized path that is relative to rootfs
func ParseWD() (r path.Relative, err error) {
	wd, err := path.ParseWD()
	if err != nil {
		return r, err
	}

	return wd.RootRelative(), nil
}

func matchesContains(mt []path.Relative, what path.Relative) bool {
	for _, m := range mt {
		if m == what {
			return true
		}
	}
	return false
}

// MustWD is like ParseWD, but panics on error
func MustWD() (r path.Relative) {
	r, err := ParseWD()
	if err != nil {
		panic(err.Error())
	}
	return r
}

func mustNew() *FS {
	fsys, err := New()
	if err != nil {
		panic(err.Error())
	}
	return fsys
}

func New() (*FS, error) {
	ffs, err := newRealFS()
	if err != nil {
		return nil, err
	}
	fsys := &FS{}
	fsys.mixFS = ffs

	return fsys, nil
}

func NewMock() (*FS, error) {
	ffs, err := newMockFS()
	if err != nil {
		return nil, err
	}
	fsys := &FS{}
	fsys.mixFS = ffs

	return fsys, nil
}
