//go:build windows

package rootfs

import (
	"fmt"
	"strings"
	"syscall"

	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

func newRealFS() (*mixFS, error) {
	var opts []mixfsOption

	drives, err := getLogicalDrives()

	if err != nil {
		return nil, err
	}

	if len(drives) == 0 {
		return nil, fmt.Errorf("no drives found on windows")
	}

	for _, dr := range drives {
		lfs, err := dirfs.New(path.MustLocal(dr + `:\`))
		if err != nil {
			//return nil, err
			// be tolerant about drive-letters that are there, but are not accessible, like e.g. cdrom drives
			continue
		}
		opts = append(opts, mixfsOptAddFS(path.Relative(strings.ToLower(dr)+"/"), lfs))
	}

	return newMixfs(opts...)
}

func newMockFS() (*mixFS, error) {
	var opts []mixfsOption

	drives, err := getLogicalDrives()

	if err != nil {
		return nil, err
	}

	if len(drives) == 0 {
		return nil, fmt.Errorf("no drives found on windows")
	}

	for _, dr := range drives {
		lfs, err := mockfs.New(path.MustLocal(dr + `:\`))
		if err != nil {
			return nil, err
		}
		opts = append(opts, mixfsOptAddFS(path.Relative(strings.ToLower(dr)+"/"), lfs))
	}

	return newMixfs(opts...)
}

// stolen from here: https://stackoverflow.com/questions/23128148/how-can-i-get-a-listing-of-all-drives-on-windows-using-golang
func getLogicalDrives() ([]string, error) {
	kernel32, _ := syscall.LoadLibrary("kernel32.dll")
	getLogicalDrivesHandle, _ := syscall.GetProcAddress(kernel32, "GetLogicalDrives")

	var drives []string

	if ret, _, callErr := syscall.Syscall(uintptr(getLogicalDrivesHandle), 0, 0, 0, 0); callErr != 0 {
		// handle error
		return nil, callErr
	} else {
		drives = bitsToDrives(uint32(ret))
	}

	return drives, nil

}

func bitsToDrives(bitMap uint32) (drives []string) {
	availableDrives := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

	for i := range availableDrives {
		if bitMap&1 == 1 {
			drives = append(drives, availableDrives[i])
		}
		bitMap >>= 1
	}

	return
}
