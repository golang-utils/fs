//go:build !windows

package rootfs

import (
	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

func newRealFS() (*mixFS, error) {
	lfs, err := dirfs.New(path.MustLocal(`/`))
	if err != nil {
		return nil, err
	}
	return newMixfs(mixfsOptAddFS("", lfs))
}

func newMockFS() (*mixFS, error) {
	lfs, err := mockfs.New(path.MustLocal(`/`))
	if err != nil {
		return nil, err
	}
	return newMixfs(mixfsOptAddFS("", lfs))
}
