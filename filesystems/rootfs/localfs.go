package rootfs

import (
	"io"
	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

var local = mustNew()

func Delete(p path.Local, recursive bool) error {
	return local.Delete(p.RootRelative(), recursive)
}

func Drive(p path.Local, recursive bool) (path.Local, error) {
	return local.Drive(p.RootRelative())
}

func Exists(p path.Local) bool {
	return local.Exists(p.RootRelative())
}

func FreeSpace(p path.Local) int64 {
	return local.FreeSpace(p.RootRelative())
}

func GetMode(p path.Local) (fs.FileMode, error) {
	return local.GetMode(p.RootRelative())
}

func Glob(pattern string) (matches []path.Local, err error) {
	m, err := local.Glob(pattern)
	if err != nil {
		return nil, err
	}

	matches = make([]path.Local, len(m))

	for i := range m {
		matches[i] = path.ToLocal(local.Abs(m[i]))
	}

	return matches, nil
}

func MkDirTmp(pattern string) (dir path.Local, err error) {
	reldir, err := local.MkDirTmp("", pattern)
	if err != nil {
		return dir, err
	}
	return path.ToLocal(local.Abs(reldir)), nil
}

func ModTime(p path.Local) (time.Time, error) {
	return local.ModTime(p.RootRelative())
}

func Move(src path.Local, targetDir path.Local) error {
	return local.Move(src.RootRelative(), targetDir.RootRelative())
}

func ReadSeeker(p path.Local) (fs.ReadSeekCloser, error) {
	return local.ReadSeeker(p.RootRelative())
}

func Reader(p path.Local) (io.ReadCloser, error) {
	return local.Reader(p.RootRelative())
}

func Rename(p path.Local, name string) error {
	return local.Rename(p.RootRelative(), name)
}

func SetMode(p path.Local, m fs.FileMode) error {
	return local.SetMode(p.RootRelative(), m)
}

func Size(file path.Local) int64 {
	return local.Size(file.RootRelative())
}

func Write(p path.Local, rd io.ReadCloser, inbetween bool) error {
	return local.Write(p.RootRelative(), rd, inbetween)
}

func WriteTmpFile(parentDir path.Local, pattern string, data []byte) (path.Local, error) {
	tfile, err := local.WriteTmpFile(parentDir.RootRelative(), pattern, data)
	if err != nil {
		return path.Local{}, err
	}
	return path.ToLocal(local.Abs(tfile)), nil
}

func WriteWithMode(p path.Local, data io.ReadCloser, m fs.FileMode, recursive bool) error {
	return local.WriteWithMode(p.RootRelative(), data, m, recursive)
}

/*
shortcuts from fs
*/

func MkDir(p path.Local) error {
	return fs.MkDir(local, p.RootRelative())
}

func MkDirAll(p path.Local) error {
	return fs.MkDirAll(local, p.RootRelative())
}

// IsEmpty returns, if the given dir is empty or does not exist or is not a directory, but a file.
func IsEmpty(dir path.Local) bool {
	return fs.IsEmpty(local, dir.RootRelative())
}

// DirSize returns the size of the given directory.
// If the given path is not a directory or any error occurs,
// -1 is returned. If the given dir is empty, 0 is returned.
// TODO test
func DirSize(dir path.Local) (size int64) {
	return fs.DirSize(local, dir.RootRelative())
}

func ClearDir(dir path.Local) error {
	return fs.ClearDir(local, dir.RootRelative())
}

func ReadFile(f path.Local) ([]byte, error) {
	return fs.ReadFile(local, f.RootRelative())
}

func CreateFileFrom(f path.Local, rd io.Reader) error {
	return fs.CreateFileFrom(local, f.RootRelative(), rd)
}

// CreateFile does return an error, if the file already exists or if the parent directory does not exist
func CreateFile(f path.Local, bt []byte) error {
	return fs.CreateFile(local, f.RootRelative(), bt)
}

func WriteFileFrom(f path.Local, rd io.Reader, recursive bool) error {
	return fs.WriteFileFrom(local, f.RootRelative(), rd, recursive)
}

func WriteFile(f path.Local, bt []byte, recursive bool) error {
	return fs.WriteFile(local, f.RootRelative(), bt, recursive)
}

// ReadDirNames returns the names of the dirs and files in a directory and returns them
func ReadDirNames(dir path.Local) ([]string, error) {
	return fs.ReadDirNames(local, dir.RootRelative())
}

// ReadDirPaths returns the full paths (including dir) of the files and folders inside dir
func ReadDirPaths(dir path.Local) ([]path.Local, error) {
	rels, err := fs.ReadDirPaths(local, dir.RootRelative())
	if err != nil {
		return nil, err
	}

	locs := make([]path.Local, len(rels))

	for i := range rels {
		locs[i] = path.ToLocal(local.Abs(rels[i]))
	}

	return locs, nil
}
