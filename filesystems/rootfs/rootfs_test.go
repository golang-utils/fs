package rootfs

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

func TestGlobWindows(t *testing.T) {
	if !fs.OnWindows() {
		t.Skip()
		return
	}

	//	fsys, err := NewMock()
	fsys, err := New()

	if err != nil {
		t.Fatal(err.Error())
	}

	matches, err := fsys.Glob("c/Windows/*")

	if err != nil {
		t.Errorf("err while trying to find entries on c/Windows: %s", err.Error())
		return
	}

	if len(matches) == 0 {
		t.Errorf("could not find entries on c/Windows")
	}

	if !matchesContains(matches, path.Relative("c/Windows/System32/")) {
		t.Errorf("c/Windows/System32/ folder is missing")
	}

	//	fmt.Printf("%#v", matches)

}

func TestMkDirTmp(t *testing.T) {
	root, err := New()

	if err != nil {
		t.Fatal(err.Error())
	}

	res, err := root.MkDirTmp("", "test-dir-*")

	if err != nil {
		t.Fatal(err.Error())
	}

	tdir := path.MustDirFromSystem(os.TempDir()).RootRelative()

	if !strings.HasPrefix(res.String(), tdir.String()) {
		t.Errorf("%s is not inside os.TempDir (%s)", res, tdir)
	}

	if !strings.HasPrefix(res.String(), tdir.Join("test-dir-").String()) {
		t.Errorf("%s does not start with pattern %s", res, "test-dir")
	}

	if !path.IsDir(res) {
		t.Errorf("%s  is no directory", res)
	}

}

func TestWriteTmpFile(t *testing.T) {
	root, err := New()

	if err != nil {
		t.Fatal(err.Error())
	}

	res, err := root.WriteTmpFile("", "test-file-*.txt", []byte("content"))

	if err != nil {
		t.Fatal(err.Error())
	}

	tdir := path.MustDirFromSystem(os.TempDir()).RootRelative()

	if !strings.HasPrefix(res.String(), tdir.String()) {
		t.Errorf("%s is not inside os.TempDir (%s)", res, tdir)
	}

	if !strings.HasPrefix(res.String(), tdir.Join("test-file-").String()) {
		t.Errorf("%s does not start with pattern %s", res, "test-file-")
	}

	if !strings.HasSuffix(res.String(), ".txt") {
		t.Errorf("%s does not end with  %s", res, ".txt")
	}

	if path.IsDir(res) {
		t.Errorf("%s  is a directory", res)
	}

}

func TestParsePath(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		// 0
		{"/", ""},
		{"/home", "home"},
		{"/home/", "home/"},
		{"C:/", "c/"},
		{"C:/home", "c/home"},
		{"C:/home/", "c/home/"},
		{"D:/", "d/"},
	}

	for i, test := range tests {

		loc := path.MustLocal(test.input)
		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := loc.RootRelative().String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] MustLocal(%q).Head() returned %q // expected %q", i, test.input, got, expected)
		}

	}

	_ = tests
}

func TestFullCircle(t *testing.T) {
	if !fs.OnWindows() {
		t.Skip()
	}

	str1 := `c:\Windows\Temp`
	//dir1 := path.MustLocal(str1 + "/")
	rel1 := MustPath(str1) // now a relative path that can be used with any filesystem and everywhere, so you can now also replace the fs with a mockfs
	str2 := `i:\Pictures`
	//dir2 := path.MustLocal(str2 + "/")
	rel2 := MustPath(str2 + "/") // now a relative path yada yada  "d/Pictures/"

	fsys, _ := New()

	// any of this relative paths you can bring back to the system

	str1a := path.ToSystem(fsys.Abs(rel1)) // `c:\Windows\Temp`
	str2a := path.ToSystem(fsys.Abs(rel2)) // `d:\Pictures`

	if str1 != str1a {
		t.Errorf("before: %q, after: %q", str1, str1a)
	}

	if str2 != str2a {
		t.Errorf("before: %q, after: %q", str2, str2a)
	}
}

func TestAbsDir(t *testing.T) {
	base := path.MustWD().Join("tests/testAbsDir/")

	os.RemoveAll(path.ToSystem(base.Dir()))
	os.MkdirAll(path.ToSystem(base), 0755)

	prefix := base.RootRelative()

	_ = prefix

	tests := []struct {
		path     string
		expected string
	}{
		{prefix.String() + "testdir_b/g/f/", base.Join("testdir_b/g/f/").String()},
		//	{"testdir_b/g/f/", base.Join("testdir_b/g/f/").String()},
	}

	for i, test := range tests {
		fsys, err := New()

		if err != nil {
			t.Fatalf("ERROR: %v", err)
		}

		/*
			err = fs.MkDirAll(fsys, path.Relative(test.path))

			if err != nil {
				t.Fatalf("ERROR: %v", err)
			}
		*/

		got := fsys.Abs(path.Relative(test.path))
		expected := test.expected

		if got == nil {
			t.Errorf("[%v] %T().Abs(%q) = nil // expected: %q", i, fsys, test.path, expected)
			continue
		}

		if got.String() != expected {
			t.Errorf("[%v] %T().Abs(%q) = %q // expected: %q", i, fsys, test.path, got.String(), expected)
		}
	}
}

func TestAbsFile(t *testing.T) {
	base := path.MustWD().Join("tests/testAbsFile/")

	os.RemoveAll(path.ToSystem(base.Dir()))
	os.MkdirAll(path.ToSystem(base), 0755)

	prefix := base.RootRelative()

	_ = prefix

	tests := []struct {
		path     string
		expected string
	}{
		{prefix.String() + "testdir_b/g/f", base.Join("testdir_b/g/f").String()},
		//{true, base, "testdir_b/g/f/", prepareFS.Abs(path.Relative("testdir_b/g/f/")).String()},
		//{prefix.String() + "testdir_b/g/f", "http://localhost:6000/tests/testAbsFile/testdir_b/g/f"},
		//{false, base, "testdir_b/g/f/", "http://localhost:6000/tests/testdir_b/g/f/"},
		//	{prefix.String() + "testdir_b/g/f", prepareFS.Abs(path.Relative("testdir_b/g/f")).String()},
		//{false, base, "testdir_b/g/f/", prepareFS.Abs(path.Relative("testdir_b/g/f/")).String()},
	}

	for i, test := range tests {
		fsys, err := New()

		if err != nil {
			t.Fatalf("ERROR: %v", err)
		}

		/*
			err = fs.WriteFile(fsys, path.Relative(test.path), []byte("content"), true)

			if err != nil {
				t.Fatalf("ERROR: %v", err)
			}
		*/

		got := fsys.Abs(path.Relative(test.path))

		expected := test.expected
		if got.String() != expected {
			t.Errorf("[%v] %T().Abs(%q) = %q // expected: %q", i, fsys, test.path, got.String(), expected)
		}
	}
}

func TestAbsRoot(t *testing.T) {

	base := path.MustWD().Join("tests/testAbsFile/")

	os.RemoveAll(path.ToSystem(base.Dir()))
	os.MkdirAll(path.ToSystem(base), 0755)

	prefix := base.RootRelative()

	_ = prefix

	//fmt.Println("testAbs did run")
	//prepareFS, base := spec.PrepareTest(fstests.conf, "testAbsRoot")

	tests := []struct {
		path     string
		expected string
	}{
		//{"/", prefix.String()},
		{"/", "/"},
		{"", "/"},
		{".", "/"},
		{prefix.String(), base.String()},
	}

	for i, test := range tests {
		fsys, err := New()

		if err != nil {
			t.Fatalf("ERROR: %v", err)
		}

		got := fsys.Abs(path.Relative(test.path))
		expected := test.expected

		if got == nil {
			t.Errorf("[%v] %T.Abs(%q) = nil // expected: %q", i, fsys, test.path, expected)
			continue
		}

		if got.String() != expected {
			t.Errorf("[%v] %T.Abs(%q) = %q // expected: %q", i, fsys, test.path, got.String(), expected)
		}
	}
}
