package rootfs

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

/*
IDEA:

the idea is, to have a fs, based on a mix of other fs that has "mount points", so that it knows
which is which

this could be the base of a "rootfs" for windows that has the drive letters as mount points

fs can't be added after the initialization (maybe that changes at a later point in time)

*/

type mixfsOption func(*mixFS)

// to add a basefs (for "everything else"), add an fs with the mountpoint ""
// there must not be two fs with the same mountpoint
// mountpoints must always be directories
func mixfsOptAddFS(mountpoint path.Relative, mfsys fs.Local) mixfsOption {
	return func(fsys *mixFS) {
		fsys.addFS(mountpoint, mfsys)
	}
}

var _ fs.Local = &mixFS{}

type mixFS struct {
	fsmap             map[path.Relative]fs.Local
	fallback          *wrapFallbackFS
	sortedMountpoints []string // for finding the longer matches first
}

func (fsys *mixFS) addFS(mountpoint path.Relative, mfsys fs.Local) {
	if mountpoint.IsRoot() {
		if fsys.fallback.FS != nil {
			panic("a fallback FS  already exists")
		}

		fsys.fallback.FS = mfsys
		return
	}

	if _, has := fsys.fsmap[mountpoint]; has {
		panic("a FS for mountpoint " + mountpoint + " already exists")
	}

	if !path.IsDir(mountpoint) {
		panic(fs.ErrExpectedDir.Params(mountpoint.String()))
	}
	fsys.fsmap[mountpoint] = mfsys
	fsys.sortedMountpoints = append(fsys.sortedMountpoints, mountpoint.String())
	sort.Strings(fsys.sortedMountpoints)
}

// to add a basefs (for "everything else"), add an fs with the mountpoint ""
func newMixfs(opts ...mixfsOption) (*mixFS, error) {
	fsys := &mixFS{
		fsmap: map[path.Relative]fs.Local{},
	}

	fsys.fallback = &wrapFallbackFS{fsys: fsys}

	for _, opt := range opts {
		opt(fsys)
	}

	err := fsys.checkWrongMounts()

	if err != nil {
		return nil, err
	}

	return fsys, nil
}

func (fsys *mixFS) checkWrongMounts() error {
	for k, fs1 := range fsys.fsmap {
		for y, fs2 := range fsys.fsmap {
			if y.String() == k.String() {
				continue
			}
			if strings.HasPrefix(y.String(), k.String()) {
				return fmt.Errorf("fs %T mounted on %q is mounted inside fs %T that is mounted on %q", fs2, y, fs1, k)
			}
		}
	}
	return nil
}

func (fsys *mixFS) getFallBackFS(p path.Relative) (found fs.Local, stripped path.Relative, err error) {
	if fsys.fallback.FS == nil {
		return nil, "", fs.ErrNotFound.Params(p)
	}

	return fsys.fallback, p, nil
}

func (fsys *mixFS) isMountPoint(p path.Relative) bool {
	if p.IsRoot() {
		return fsys.fallback.FS != nil
	}

	if !path.IsDir(p) {
		return false
	}

	for mp := range fsys.fsmap {
		if p == mp {
			return true
		}
	}

	return false
}

// MkDirTmp creates a temporary directory inside the given parentDir
// if the parentDir is root, os.TempDir() is used as a parent, in each case the resulting dir must exist
func (fsys *mixFS) MkDirTmp(parentDir path.Relative, pattern string) (dir path.Relative, err error) {
	var loc path.Local

	if parentDir.IsRoot() {
		loc, err = path.ParseDirFromSystem(os.TempDir())
		if err != nil {
			return dir, err
		}
	} else {
		if !path.IsDir(parentDir) {
			return dir, fs.ErrExpectedDir.Params(parentDir.String())
		}

		loc = fsys.Abs(parentDir).(path.Local)
	}

	if !fsys.Exists(loc.RootRelative()) {
		return dir, fs.ErrNotFound.Params(loc.RootRelative().String())
	}

	d, err := os.MkdirTemp(path.ToSystem(loc), pattern)

	if err != nil {
		return dir, err
	}

	l, err := path.ParseDirFromSystem(d)

	if err != nil {
		os.RemoveAll(d)
		return dir, err
	}

	return l.RootRelative(), nil
}

// WriteTmpFile writes to a temporary file inside the given parentDir
// if the parentDir is root, os.TempDir() is used as a parent, in each case the resulting dir must exist
func (fsys *mixFS) WriteTmpFile(parentDir path.Relative, pattern string, data []byte) (file path.Relative, err error) {
	var loc path.Local

	if parentDir.IsRoot() {
		loc, err = path.ParseDirFromSystem(os.TempDir())
		if err != nil {
			return file, err
		}
	} else {
		if !path.IsDir(parentDir) {
			return file, fs.ErrExpectedDir.Params(parentDir.String())
		}

		loc = fsys.Abs(parentDir).(path.Local)
	}

	if !fsys.Exists(loc.RootRelative()) {
		return file, fs.ErrNotFound.Params(loc.RootRelative().String())
	}

	f, err := os.CreateTemp(path.ToSystem(loc), pattern)
	defer f.Close()
	if err != nil {
		return file, err
	}

	_, err = f.Write(data)

	if err != nil {
		os.Remove(f.Name())
		return file, err
	}

	l, err := path.ParseFileFromSystem(f.Name())

	if err != nil {
		os.Remove(f.Name())
		return file, err
	}

	return l.RootRelative(), nil
}

// finds:
// 1. exact match
// 2. match on longes mountpoint
// 3. fallback fs if exists
// stripped is the path relative to the found filesystem (the mountpoint stripped away)
func (fsys *mixFS) findFSOfPath(p path.Relative) (found fs.Local, stripped path.Relative, err error) {
	if p.IsRoot() {
		if fs.OnWindows() {
			return nil, path.Relative(""), fmt.Errorf("no fallback fs on windows")
		}
		return fsys.fallback, path.Relative(""), nil
	}

	if path.IsDir(p) {
		fss, has := fsys.fsmap[p]
		// exact match
		if has {
			return fss, path.Relative(""), nil
		}
	}

	for i := len(fsys.sortedMountpoints) - 1; i >= 0; i-- {
		pref := fsys.sortedMountpoints[i]

		switch {
		// p is inside a mountpoint
		case strings.HasPrefix(p.String(), pref):
			if strings.HasPrefix(p.String(), pref) {
				stripped = path.Relative(strings.TrimPrefix(p.String(), pref))
				return fsys.fsmap[path.Relative(pref)], stripped, nil
			}

		// p is outside a mountpoint, but there is a subdir inside p that is a mountpoint
		case path.IsDir(p) && strings.HasPrefix(pref, p.String()):
			return fsys.fallback, p, nil
		}

	}

	return fsys.getFallBackFS(p)
}

func (fsys *mixFS) Drive(p path.Relative) (l path.Local, err error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return l, err
	}

	return ffs.Drive(stripped)
}

func (fsys *mixFS) SetMode(p path.Relative, m fs.FileMode) (err error) {
	if p.IsRoot() || fsys.isMountPoint(p) {
		return fs.ErrNotSupported.Params("SetMode not supported for mountpoints", p.String())
	}
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return err
	}

	return ffs.SetMode(stripped, m)
}

// should not return mode for a mountpoint (error (not implemented)), should return error (not implemented), if there is no fallback
func (fsys *mixFS) GetMode(p path.Relative) (m fs.FileMode, err error) {
	if p.IsRoot() || fsys.isMountPoint(p) {
		return m, fs.ErrNotSupported.Params("GetMode not supported for mountpoints", p.String())
	}
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return m, err
	}

	return ffs.GetMode(stripped)
}

func (fsys *mixFS) FreeSpace(r path.Relative) int64 {
	ffs, stripped, err := fsys.findFSOfPath(r)
	if err != nil {
		return -1
	}

	return ffs.FreeSpace(stripped)
}

func (fsys *mixFS) Rename(p path.Relative, name string) (err error) {
	if p.IsRoot() || fsys.isMountPoint(p) {
		return fs.ErrNotSupported.Params("Rename not supported for mountpoints", p.String())
	}

	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return err
	}

	return ffs.Rename(stripped, name)
}

// TODO test extensively, if its doing, what we want on windows, e.g. would we move across
// drive letters / mountpoints?

/*
i would suggest the following:

on windows we forbid the action, if src and target are not on the same drive
on unix we just pass it to the system
*/

func (fsys *mixFS) Move(src, trgtDir path.Relative) (err error) {
	if src.IsRoot() || fsys.isMountPoint(src) {
		return fs.ErrNotSupported.Params("Move not supported for mountpoints", src.String())
	}

	if trgtDir.IsRoot() || fsys.isMountPoint(trgtDir) {
		return fs.ErrNotSupported.Params("Move not supported for mountpoints", trgtDir.String())
	}

	if fs.OnWindows() {
		srcDr, err := fsys.Drive(src)
		if err != nil {
			return err
		}

		trgtDr, err := fsys.Drive(trgtDir)
		if err != nil {
			return err
		}

		if srcDr != trgtDr {
			return fs.ErrNotSupported.Params("source and target directories must not be on different drives on windows")
		}

		ffs, strippedSrc, err := fsys.findFSOfPath(src)
		if err != nil {
			return err
		}

		_, strippedTarget, err := fsys.findFSOfPath(src)
		if err != nil {
			return err
		}

		return ffs.Move(strippedSrc, strippedTarget)
	}

	return fsys.fallback.FS.Move(src, trgtDir)

	//panic("find out how to do it (should be allow cross drive moving?)")
	/*
		ffs, stripped, err := fsys.findFSOfPath(r)
		if err != nil {
			return err
		}

		return ffs.Move(src, trgtDir)
	*/
	//return fmt.Errorf("not implemented yet")
}

// TODO test extensively, if it doing, what we want on windows, e.g. should the drive letters
// be part of the pattern or not, and if, how should they be noted:
// fs lib style, or os system style?
/*
we could define it this way:
- on windows the pattern must start with the drive letter in the root-relative style, e.g.
  d/
	c/
	and so on. the rest of the pattern is not checked and passed down to the underlying system
- on all other systems, the pattern is just passed, as is

on windows the drive-letter up to the slash is stripped away from the pattern before passing it down the chain
*/

var driveRegExp = regexp.MustCompile("^([a-z]/)(.*)$")

func (fsys *mixFS) Glob(pattern string) (matches []path.Relative, err error) {
	if fs.OnWindows() {
		//fmt.Printf("searching for %q\n", pattern)
		res := driveRegExp.FindStringSubmatch(pattern)

		if len(res) < 3 {
			return nil, fmt.Errorf("on windows, the pattern must start with a lowercase drive letter, followed by a slash")
		}

		pre := path.Relative(res[1])
		stripped := res[2]

		//fmt.Printf("stripped: %q\n", stripped)

		mfs, has := fsys.fsmap[pre]

		if !has {
			return nil, fmt.Errorf("can't find drive %s", strings.Replace(strings.ToUpper(pre.String()), "/", `:\`, 1))
		}

		mt, err := mfs.Glob(stripped)

		if err != nil {
			return nil, err
		}

		prefMatches := make([]path.Relative, len(mt))

		for i := range mt {
			//	fmt.Println(pre.Join(mt[i].String()))
			prefMatches[i] = pre.Join(mt[i].String())
		}

		return prefMatches, nil

	}

	return fsys.fallback.FS.Glob(pattern)

	//panic("find out how to do it (should be allow cross drive moving?)")
	/*
		ffs, stripped, err := fsys.findFSOfPath(r)
		if err != nil {
			return nil, err
		}

		return ffs.Glob(pattern)
	*/
}

func (fsys *mixFS) Delete(p path.Relative, recursive bool) (err error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		if path.IsDir(p) && errors.Is(err, fs.ErrNotFound) {
			return nil
		}
		return err
	}

	return ffs.Delete(stripped, recursive)
}

func (fsys *mixFS) Write(p path.Relative, rd io.ReadCloser, inbetween bool) (err error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return err
	}

	return ffs.Write(stripped, rd, inbetween)
}

func (fsys *mixFS) WriteWithMode(p path.Relative, data io.ReadCloser, m fs.FileMode, recursive bool) (err error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return err
	}

	return ffs.WriteWithMode(stripped, data, m, recursive)
}

func (fsys *mixFS) ModTime(p path.Relative) (t time.Time, err error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return t, err
	}

	return ffs.ModTime(stripped)
}

func (m *mixFS) Abs(r path.Relative) path.Absolute {
	ffs, stripped, err := m.findFSOfPath(r)
	if err != nil {
		panic(err.Error())
	}

	return ffs.Abs(stripped)
}

func (fsys *mixFS) Exists(p path.Relative) bool {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return false
	}

	return ffs.Exists(stripped)
}

func (fsys *mixFS) Size(file path.Relative) int64 {
	if path.IsDir(file) {
		return -2
	}
	ffs, stripped, err := fsys.findFSOfPath(file)
	if err != nil {
		return -1
	}

	return ffs.Size(stripped)
}

func (fsys *mixFS) Reader(p path.Relative) (io.ReadCloser, error) {
	ffs, stripped, err := fsys.findFSOfPath(p)
	if err != nil {
		return nil, err
	}

	return ffs.Reader(stripped)
}

func (fsys *mixFS) ReadSeeker(r path.Relative) (fs.ReadSeekCloser, error) {
	ffs, stripped, err := fsys.findFSOfPath(r)
	if err != nil {
		return nil, err
	}

	return ffs.ReadSeeker(stripped)
}

func (fsys *mixFS) WriteSeeker(r path.Relative) (fs.WriteSeekCloser, error) {
	ffs, stripped, err := fsys.findFSOfPath(r)
	if err != nil {
		return nil, err
	}

	return ffs.WriteSeeker(stripped)
}

func (fsys *mixFS) getMountpointMap(prefix string) map[string]bool {
	var m = map[string]bool{}

	if prefix == "" {
		for k := range fsys.fsmap {
			arr := strings.Split(k.String(), "/")
			name := arr[0]
			if name != "" {
				m[name+"/"] = true
			}
		}

		return m
	}

	for k := range fsys.fsmap {
		if strings.HasPrefix(k.String(), prefix) {
			n := strings.TrimPrefix(k.String(), prefix)
			arr := strings.Split(n, "/")
			name := arr[0]
			//		fmt.Printf("k: %q, n: %q prefix: %q name: %q\n", k.String(), n, prefix, name+"/")
			if name != "" {
				m[name+"/"] = true
			}
		}
	}

	return m
}

func (fsys *mixFS) getMountpointSlice(prefix string) (mp []string) {
	var m = fsys.getMountpointMap(prefix)

	for k := range m {
		mp = append(mp, k)
	}

	sort.Strings(mp)
	return mp
}

type wrapFallbackFS struct {
	fsys *mixFS
	FS   fs.Local
}

// should not delete a mountpoint, should return error (not implemented), if there is no fallback
func (f *wrapFallbackFS) Delete(p path.Relative, recursive bool) (err error) {
	if p.IsRoot() {
		return fs.ErrNotSupported.Params(f.fsys, "deleting root")
	}

	if path.IsDir(p) {
		if _, isMp := f.fsys.fsmap[p]; isMp {
			return fs.ErrNotSupported.Params(f.fsys, "deleting mountpoints")
		}

		mpts := f.fsys.getMountpointMap(p.String())
		if len(mpts) != 0 {
			return fs.ErrNotSupported.Params(f.fsys, "deleting directories that include mountpoints")
		}
	}

	if f.FS == nil {
		//return fs.ErrNotFound.Params(p.String())
		return nil
	}

	return f.FS.Delete(p, recursive)
}

// creating a dir that is root, or a mountpoint or contains a mountpoint should just be ignored
func (f *wrapFallbackFS) Write(p path.Relative, rd io.ReadCloser, inbetween bool) (err error) {
	if p.IsRoot() {
		return nil
	}

	if path.IsDir(p) {
		if _, isMp := f.fsys.fsmap[p]; isMp {
			return nil
		}

		mpts := f.fsys.getMountpointMap(p.String())
		if len(mpts) != 0 {
			return nil
		}
	}

	if f.FS == nil {
		return fs.ErrNotSupported.Params(f.FS, "no fallback fs defined")
	}

	return f.FS.Write(p, rd, inbetween)
}

// TODO compare agains Write
func (f *wrapFallbackFS) WriteWithMode(p path.Relative, data io.ReadCloser, m fs.FileMode, recursive bool) (err error) {
	if f.FS == nil {
		return fs.ErrNotSupported.Params(f.FS, "WriteWithMode (no fallback fs defined)")
	}

	return f.FS.WriteWithMode(p, data, m, recursive)
}

func (f *wrapFallbackFS) SetMode(p path.Relative, m fs.FileMode) (err error) {
	if f.FS == nil {
		return fs.ErrNotSupported.Params(f.FS, "SetMode (no fallback fs defined)")
	}

	return f.FS.SetMode(p, m)
}

// should not return a Modtime for a mountpoint (error (not implemented)), should return error (not implemented), if there is no fallback
func (f *wrapFallbackFS) GetMode(p path.Relative) (m fs.FileMode, err error) {
	if f.FS == nil {
		return m, fs.ErrNotSupported.Params(f.FS, "GetMode (no fallback fs defined)")
	}

	return f.FS.GetMode(p)
}

// should not return a Modtime for a mountpoint (error (not implemented)), should return error (not implemented), if there is no fallback
func (f *wrapFallbackFS) ModTime(p path.Relative) (t time.Time, err error) {
	if p.IsRoot() {
		return t, fs.ErrNotSupported.Params(f.fsys, "ModTime on root")
	}

	if path.IsDir(p) {
		return t, fs.ErrNotSupported.Params(f.fsys, "ModTime on dir")
	}

	if f.FS == nil {
		return t, fs.ErrNotSupported.Params(f.FS, "ModTime (no fallback fs defined)")
	}

	return f.FS.ModTime(p)
}

func (f *wrapFallbackFS) FreeSpace(r path.Relative) int64 {
	d, err := fs.Drive(path.ToLocal(f.Abs(r)))
	if err != nil {
		return -1
	}
	var avaible uint64
	err = fs.FreeSpace(d, &avaible)
	if err != nil {
		return -1
	}
	return int64(avaible)
}

// TODO: check agains Reader
func (f *wrapFallbackFS) ReadSeeker(r path.Relative) (fs.ReadSeekCloser, error) {
	if f.FS == nil {
		return nil, fs.ErrNotSupported.Params(f.FS, "ReadSeeker (no fallback fs defined)")
	}
	return f.FS.ReadSeeker(r)
}

func (f *wrapFallbackFS) WriteSeeker(r path.Relative) (fs.WriteSeekCloser, error) {
	if f.FS == nil {
		return nil, fs.ErrNotSupported.Params(f.FS, "ReadSeeker (no fallback fs defined)")
	}
	return f.FS.WriteSeeker(r)
}

func (f *wrapFallbackFS) Rename(p path.Relative, name string) (err error) {
	if f.FS == nil {
		return fs.ErrNotSupported.Params(f.FS, "Rename (no fallback fs defined)")
	}
	return f.FS.Rename(p, name)
}

// TODO test extensively, if it doing, what we want on windows, e.g. would we move across
// drive letters / mountpoints?
func (f *wrapFallbackFS) Move(src, trgtDir path.Relative) (err error) {
	if f.FS == nil {
		return fs.ErrNotSupported.Params(f.FS, "Move (no fallback fs defined)")
	}
	return f.FS.Move(src, trgtDir)
}

// TODO test extensively, if it doing, what we want on windows, e.g. should the drive letters
// be part of the pattern or not, and if, how should they be noted:
// fs lib style, or os system style?
func (f *wrapFallbackFS) Glob(pattern string) (matches []path.Relative, err error) {
	//panic("TODO ")

	if f.FS == nil {
		return nil, fs.ErrNotSupported.Params(f.FS, "Drive (no fallback fs defined)")
	}
	return f.FS.Glob(pattern)
}

func (f *wrapFallbackFS) Drive(p path.Relative) (l path.Local, err error) {
	str, err := fs.Drive(path.ToLocal(f.Abs(p)))
	if err != nil {
		return l, err
	}

	return path.ParseDirFromSystem(str)
}

// should return the root for a mountpoint, should return nil, if there is no fallback
func (f *wrapFallbackFS) Abs(r path.Relative) path.Absolute {
	if r.IsRoot() {
		if f.FS == nil {
			return nil
		}
		return f.FS.Abs(r)
	}

	if path.IsDir(r) {
		if mfs, isMp := f.fsys.fsmap[r]; isMp {
			return mfs.Abs(path.Relative(""))
		}
	}

	if f.FS == nil {
		return nil
	}
	return f.FS.Abs(r)
}

// should return true for a mountpoint, should return false, if there is no fallback
func (f *wrapFallbackFS) Exists(p path.Relative) bool {
	if p.IsRoot() {
		return true
	}

	if path.IsDir(p) {
		if _, isMp := f.fsys.fsmap[p]; isMp {
			return true
		}

		mpts := f.fsys.getMountpointMap(p.String())
		if len(mpts) != 0 {
			return true
		}
	}

	if f.FS == nil {
		return false
	}

	return f.FS.Exists(p)

}

// should return not implemented -2 for a mountpoint or dir, should return -2, if there is no fallback
func (f *wrapFallbackFS) Size(file path.Relative) int64 {
	if file.IsRoot() || path.IsDir(file) {
		return -2
	}

	if f.FS == nil {
		return -2
	}

	return f.FS.Size(file)
}

// if p is a dir and there is a fallback, it should add the mountpoints that are prefixed with p to the content returned by the fallback
// if p is a dir and there is no fallback, it should return the mountpoints that are prefixed with p
// if p is not a dir and there is a fallback, it should return whatever the fallback returns
// if p is not a dir and there is no fallback, it should return not found
func (f *wrapFallbackFS) Reader(p path.Relative) (io.ReadCloser, error) {
	if !p.IsRoot() && !path.IsDir(p) {
		if f.FS == nil {
			return nil, fs.ErrNotFound.Params(p.String())
		}
		return f.FS.Reader(p)
	}

	// p is root or a dir
	mps := f.fsys.getMountpointSlice(p.String())

	//fmt.Printf("mps: %v\n", mps)

	var bf bytes.Buffer

	for _, mp := range mps {
		bf.WriteString(path.Base(path.Relative(mp)) + "\n")
	}

	if f.FS == nil {
		return fs.ReadCloser(bytes.NewReader(bf.Bytes())), nil
	}

	names, err := fs.ReadDirNames(f.FS, p)

	if err != nil {
		if bf.Len() > 0 {
			return fs.ReadCloser(bytes.NewReader(bf.Bytes())), nil
		}
		return nil, err
	}

	for _, nm := range names {
		bf.WriteString(nm + "\n")
	}
	return fs.ReadCloser(bytes.NewReader(bf.Bytes())), nil
}
