package rootfs

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/importer"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_local"
)

func mustNewTest(prepare fs.ReadOnly, base path.Local) fs.Local {

	f, err := NewMock()

	if err != nil {
		panic(err.Error())
	}

	err = importer.Import(prepare, f)

	if err != nil {
		panic(err.Error())
	}

	return f

}

func TestSpec(t *testing.T) {
	var c spec.Config
	c.IsLocal = true
	c.Unsupported.DirSize = true
	c.Unsupported.ModTimeDir = true
	c.Unsupported.Abs = true
	c.Unsupported.ReadRootDir = true
	c.Unsupported.WriteRootDir = true
	c.Unsupported.DeleteRootDir = true
	c.PathPrefix = path.MustWD().RootRelative().Join("testfs/").String()
	s := core_local.Spec(c, mustNewTest)
	s.Run("rootfs/", t)
}
