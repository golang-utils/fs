package mockfs_test

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/importer"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_testfs"
)

func mustNew(prepareFS fs.ReadOnly, loc path.Absolute) fs.TestFS {
	f, err := mockfs.New(loc)

	if err != nil {
		panic(err.Error())
	}

	err = importer.Import(prepareFS, f, importer.OptionContinueOnError())

	if err != nil {
		panic(err.Error())
	}

	return f
}

func TestSpec(t *testing.T) {
	var c spec.Config
	c.Unsupported.DirSize = true
	s := core_testfs.Spec(c, mustNew)
	s.Run("mockfs/", t)
}
