package mockfs

import (
	"fmt"
	"io"
	"time"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mapfs"
	"gitlab.com/golang-utils/fs/path"
)

/*
	Package mockfs implements a filesystem that is used to replace a real filesystem
	for testing purposes. It is based on mapfs but adds the ability to return user defined errors.
*/

var _ fs.TestFS = &FS{}

type FS struct {
	Error  error
	ErrMap errors.ErrMap
	*mapfs.FS
}

func (t *FS) Clear() {
	mfs, err := mapfs.New(path.MustLocal("/test/"))
	if err != nil {
		panic(err.Error())
	}
	t.FS = mfs
	t.Error = nil
	t.ErrMap = errors.ErrMap{}
}

func New(loc path.Absolute) (*FS, error) {
	mfs, err := mapfs.New(loc)

	if err != nil {
		return nil, err
	}

	return &FS{
		FS:     mfs,
		ErrMap: errors.ErrMap{},
	}, nil
}

func (d *FS) Glob(pattern string) (matches []path.Relative, err error) {
	if d.Error != nil {
		return nil, d.Error
	}

	matches, err = d.FS.Glob(pattern)

	if err != nil {
		d.ErrMap[fmt.Sprintf("Glob(%q)", pattern)] = err
	}

	return
}

func (d *FS) FreeSpace(p path.Relative) int64 {
	if d.Error != nil {
		return -1
	}
	return d.FS.FreeSpace(p)
}

func (d *FS) ReadSeeker(p path.Relative) (fs.ReadSeekCloser, error) {
	if d.Error != nil {
		return nil, d.Error
	}

	rsc, err := d.FS.ReadSeeker(p)

	if err != nil {
		d.ErrMap[fmt.Sprintf("ReadSeeker(%q)", p.String())] = err
	}

	return rsc, err
}

func (d *FS) GetMode(name path.Relative) (fs.FileMode, error) {
	if d.Error != nil {
		return 0, d.Error
	}
	m, err := d.FS.GetMode(name)
	if err != nil {
		d.ErrMap[fmt.Sprintf("GetMode(%q)", name.String())] = err
	}

	return m, err
}

func (d *FS) Size(name path.Relative) int64 {
	if d.Error != nil {
		return -1
	}
	return d.FS.Size(name)
}

func (d *FS) SetMode(name path.Relative, m fs.FileMode) error {
	if d.Error != nil {
		return d.Error
	}
	err := d.FS.SetMode(name, m)
	if err != nil {
		d.ErrMap[fmt.Sprintf("SetMode(%q)", name.String())] = err
	}
	return err
}

func (m *FS) Drive(p path.Relative) (l path.Local, err error) {
	if m.Error != nil {
		return l, m.Error
	}
	l, err = m.FS.Drive(p)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Drive(%q)", p.String())] = err
	}
	return l, err
}

func (m *FS) Move(src path.Relative, targetFolder path.Relative) error {
	if m.Error != nil {
		return m.Error
	}
	err := m.FS.Move(src, targetFolder)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Move(%q,%q)", src.String(), targetFolder.String())] = err
	}
	return err
}

func (m *FS) Reader(p path.Relative) (io.ReadCloser, error) {
	if m.Error != nil {
		return nil, m.Error
	}

	rd, err := m.FS.Reader(p)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Reader(%q)", p.String())] = err
	}
	return rd, err
}

func (m *FS) Write(p path.Relative, rd io.ReadCloser, recursive bool) (err error) {
	if m.Error != nil {
		return m.Error
	}

	err = m.FS.Write(p, rd, recursive)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Write(%q,%v)", p.String(), recursive)] = err
	}
	return err
}

func (m *FS) WriteWithMeta(p path.Relative, rd io.ReadCloser, meta map[string][]byte, recursive bool) error {
	if m.Error != nil {
		return m.Error
	}

	err := m.FS.WriteWithMeta(p, rd, meta, recursive)
	if err != nil {
		m.ErrMap[fmt.Sprintf("WriteWithMeta(%q,%v)", p.String(), recursive)] = err
	}
	return err
}

func (m *FS) WriteWithMode(p path.Relative, rd io.ReadCloser, perm fs.FileMode, recursive bool) (err error) {
	if m.Error != nil {
		return m.Error
	}

	err = m.FS.WriteWithMode(p, rd, perm, recursive)
	if err != nil {
		m.ErrMap[fmt.Sprintf("WriteWithMode(%q,%v)", p.String(), recursive)] = err
	}
	return err
}

func (m *FS) Delete(p path.Relative, recursive bool) error {
	if m.Error != nil {
		return m.Error
	}
	err := m.FS.Delete(p, recursive)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Delete(%q,%v)", p.String(), recursive)] = err
	}
	return err
}

// should fail when the new name is not a name but a path
func (m *FS) Rename(p path.Relative, name string) error {
	if m.Error != nil {
		return m.Error
	}
	err := m.FS.Rename(p, name)
	if err != nil {
		m.ErrMap[fmt.Sprintf("Rename(%q,%q)", p.String(), name)] = err
	}
	return err
}

func (m *FS) ModTime(p path.Relative) (t time.Time, err error) {
	if m.Error != nil {
		return t, m.Error
	}
	t, err = m.FS.ModTime(p)
	if err != nil {
		m.ErrMap[fmt.Sprintf("ModTime(%q)", p.String())] = err
	}
	return t, err
}

func (t *FS) Exists(p path.Relative) bool {
	if t.Error != nil {
		return false
	}

	return t.FS.Exists(p)
}

func (t *FS) WriteTo(fsys fs.FS) error {
	if t.Error != nil {
		return t.Error
	}
	err := t.FS.WriteTo(fsys)
	if err != nil {
		t.ErrMap[fmt.Sprintf("WriteTo(%T)", fsys)] = err
	}
	return err
}

func (t *FS) ReadFrom(fsys fs.FS, p path.Relative) error {
	if t.Error != nil {
		return t.Error
	}
	err := t.FS.ReadFrom(fsys, p)
	if err != nil {
		t.ErrMap[fmt.Sprintf("ReadFrom(%T, %q)", fsys, p.String())] = err
	}
	return err
}

func (t *FS) GetMeta(name path.Relative) (map[string][]byte, error) {
	if t.Error != nil {
		return nil, t.Error
	}

	m, err := t.FS.GetMeta(name)
	if err != nil {
		t.ErrMap[fmt.Sprintf("GetMeta(%q)", name.String())] = err
	}
	return m, err
}

func (t *FS) SetMeta(name path.Relative, meta map[string][]byte) error {
	if t.Error != nil {
		return t.Error
	}
	err := t.FS.SetMeta(name, meta)
	if err != nil {
		t.ErrMap[fmt.Sprintf("SetMeta(%q, %v)", name.String(), meta)] = err
	}
	return err
}
