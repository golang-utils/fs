package timeout

import (
	"context"
	"io"
	"time"
)

var _ io.ReadSeekCloser = &readSeekCloser{}

type readSeekCloser struct {
	rc       io.ReadSeekCloser
	canceled bool
	finished chan bool
}

func (r *readSeekCloser) Close() error {
	r.finished <- true
	return r.rc.Close()
}

func (r *readSeekCloser) Read(bt []byte) (int, error) {
	if r.canceled {
		r.rc.Close()
		return 0, context.Canceled
	}

	return r.rc.Read(bt)
}

func (r *readSeekCloser) Seek(offset int64, whence int) (int64, error) {
	if r.canceled {
		r.rc.Close()
		return 0, context.Canceled
	}
	return r.rc.Seek(offset, whence)
}

func ReadSeekCloser(r io.ReadSeekCloser, timeout time.Duration, callback func(rc io.ReadSeekCloser)) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	rc := &readSeekCloser{rc: r}
	rc.finished = make(chan bool, 1)
	go callback(rc)

	for {
		select {
		case <-ctx.Done():
			rc.canceled = true
			return
		case <-rc.finished:
			return
		default:
		}
	}
}
