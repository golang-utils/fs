package timeout

import (
	"context"
	"io"
	"time"
)

type readCloser struct {
	rc       io.ReadCloser
	canceled bool
	finished chan bool
}

func (r *readCloser) Close() error {
	r.finished <- true
	return r.rc.Close()
}

func (r *readCloser) Read(bt []byte) (int, error) {
	if r.canceled {
		r.rc.Close()
		return 0, context.Canceled
	}

	return r.rc.Read(bt)
}

func ReadCloser(r io.ReadCloser, timeout time.Duration, callback func(rc io.ReadCloser)) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	rc := &readCloser{rc: r}
	rc.finished = make(chan bool, 1)
	go callback(rc)

	for {
		select {
		case <-ctx.Done():
			rc.canceled = true
			return
		case <-rc.finished:
			return
		default:
		}
	}
}

var _ io.ReadCloser = &readCloser{}
