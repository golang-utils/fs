package timeout

import (
	"io"
	"strings"
	"testing"
	"time"
)

type waitingReadSeekCloser struct {
	rd       io.ReadSeeker
	sleep    time.Duration
	finished bool
}

func (r *waitingReadSeekCloser) Close() error {
	r.finished = true
	return nil
}

func (r *waitingReadSeekCloser) Read(bt []byte) (int, error) {
	time.Sleep(r.sleep)
	return r.rd.Read(bt)
}

func (r *waitingReadSeekCloser) Seek(offset int64, whence int) (int64, error) {
	return r.rd.Seek(offset, whence)
}

func newWaitingReadSeekCloser(s string, d time.Duration) *waitingReadSeekCloser {
	return &waitingReadSeekCloser{
		rd:    strings.NewReader(s),
		sleep: d,
	}
}

func TestReadSeekCloser(t *testing.T) {

	tests := []struct {
		sleeping time.Duration
		timeout  time.Duration
		finished bool
	}{
		{time.Millisecond * 30, time.Millisecond * 10, false},
		{time.Millisecond * 10, time.Millisecond * 30, true},
	}

	for i, test := range tests {

		rc := newWaitingReadSeekCloser("huhu", test.sleeping)
		fn := func(r io.ReadSeekCloser) { io.ReadAll(r); r.Close() }

		ReadSeekCloser(rc, test.timeout, fn)

		if rc.finished != test.finished {
			t.Errorf("[%v] expected finished: %v, but got %v", i, test.finished, rc.finished)
		}
	}

}
