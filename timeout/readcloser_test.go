package timeout

import (
	"io"
	"strings"
	"testing"
	"time"
)

type waitingReadCloser struct {
	rd       io.Reader
	sleep    time.Duration
	finished bool
}

func (r *waitingReadCloser) Close() error {
	r.finished = true
	return nil
}

func (r *waitingReadCloser) Read(bt []byte) (int, error) {
	time.Sleep(r.sleep)
	return r.rd.Read(bt)
}

func newWaitingReadCloser(s string, d time.Duration) *waitingReadCloser {
	return &waitingReadCloser{
		rd:    strings.NewReader(s),
		sleep: d,
	}
}

func TestReadCloser(t *testing.T) {

	tests := []struct {
		sleeping time.Duration
		timeout  time.Duration
		finished bool
	}{
		{time.Millisecond * 30, time.Millisecond * 10, false},
		{time.Millisecond * 10, time.Millisecond * 30, true},
	}

	for i, test := range tests {

		rc := newWaitingReadCloser("huhu", test.sleeping)
		fn := func(r io.ReadCloser) { io.ReadAll(r); r.Close() }

		ReadCloser(rc, test.timeout, fn)

		if rc.finished != test.finished {
			t.Errorf("[%v] expected finished: %v, but got %v", i, test.finished, rc.finished)
		}
	}

}
