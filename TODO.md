
# TODO

- implement localfs.Glob()
- implement Walk function
- implement validation for relative paths
- check paths with spaces (or maybe also other characters that are escaped with \\) on windows and unix, but also for remote urls!!

## Rules for paths

- there are valid and invalid paths
- there are valid and invalid filesystem paths (in general but also only for certain OSes, like windows)
- every valid filesystem path is a valid path
- there are valid and invalid URLs
- every valid url should be a valid path
- we could not cover all possible ways to have paths being build, but we could establish
  an internal general abstraction that than can be translated to local paths, where they can be Local for a certain os, but also local for the current running os.

so why not just have a general schema, which could be:

there are 3 types of paths:

1. "headless relative paths" (every relative path is headless and vice versa)
2. "absolute paths that are not URLs"
3. "absolute paths that are URLs"

then a path would be 

type Path [2]string

the "head would be in p[0] and the tail in p[1]

in general, paths with heads would be absolute Paths.
there could be a mapper, mapping absolute paths to the corresponding FS
(they could be registered before and then mapped back)
there would be some general mapper and entries in the registry like:

```sh
unix-path             
windows-path          
windows-network-path
http-path
https-path
db-connection-path (maybe)
git-path (maybe)
```

for a full path there would be the following "heads"

```sh
/                             unix-path
/[somedirs]/                  unix-subdir-path
[drive-letter]:/              windows-path
[drive-letter]:/[somedirs]/   windows-path-subdir-path
\\[networkshare]/             windows-networkshare
\\[networkshare]/[somedirs]/  windows-networkshare-subdir-path
http://www.domain.com/        http url
https://www.domain.com/       https url
ftp://user:pw@ftp.domain.com/ ftp url
```

and so on...

for the tail of the path it would be a string that is made up of folders and/or filenames,
where a foldername ends in a / and a filename does not end in a /
there may only be one filename and it must be the last part. in case of an url
there might be an achor after the filename (a # followed by other characters)
there might be any number of folders before the filename. the filename might also be left out completely.
if there is no filename and no foldername, the tail must be empty
