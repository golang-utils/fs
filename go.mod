module gitlab.com/golang-utils/fs

go 1.23.0

require (
	github.com/moby/sys/mountinfo v0.7.1
	gitlab.com/golang-utils/errors v0.0.2
	gitlab.com/golang-utils/fmtdate v1.0.2
	gitlab.com/golang-utils/spectest v0.0.5
	golang.org/x/sys v0.16.0
)

require (
	github.com/liamg/memoryfs v1.6.0
	gitlab.com/golang-utils/updatechecker v0.0.6
)

require github.com/davecgh/go-spew v1.1.1 // indirect
