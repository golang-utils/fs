package fs

import (
	"io"

	"gitlab.com/golang-utils/fs/path"
)

type ReadSeekCloser interface {
	io.Reader
	//in contrast to io.Seeker, here Seek always Seeks from the beginning of the file (whence=0)
	// other behaviors can be simply implemented by tracking (whence=1, starting from last position)
	// or via calculation by file size( whence=2, from the end of the file)
	Seek(offset int64) error
	io.Closer
}

type WriteSeekCloser interface {
	io.Writer
	//in contrast to io.Seeker, here Seek always Seeks from the beginning of the file (whence=0)
	// other behaviors can be simply implemented by tracking (whence=1, starting from last position)
	// or via calculation by file size( whence=2, from the end of the file)
	Seek(offset int64) error

	// when writing is finished Close must be called
	io.Closer
}

/*
	In this file are extension interfaces that can optionally be implemented (and checked for) in addition to the one of
	the basic interfaces (ReadOnly, FS, Local and Remote).
*/

// ExtReadSeekable is an extention interface for a filesystem that adds the ability to seek in files.
type ExtReadSeekable interface {
	ReadOnly

	// ReadSeeker returns an io.ReadSeekCloser for the content of a file.
	// The io.ReadSeekCloser should be closed after reading all.
	// An error is only returned, if p is a directory or if p does not exist.
	ReadSeeker(p path.Relative) (ReadSeekCloser, error)
}

/*
TODO:

a wrapper could be made in order to allow ReadSeeker for a filesystem that only implements ReadOnly
this wrapper could read parts of the file into a cache (bytearray) and just starts copying when the starting point is reached.
is slower, but can help (for small files) when filesystems can't deliver a seeker but the receiver (e.g. fuse) needs one
*/

// ExtWriteable is an extention interface for a filesystem that adds the ability to write files and folders.
type ExtWriteable interface {
	ReadOnly

	// Write writes either a file or a folder to the given path with default permissions.
	// If p is a directory, data is ignored.
	// If recursive is set to true, all intermediate directories will be created if necessary.
	// Write always overwrites an existing file. If you need to make sure that a file at the given
	// path should not be overwritten, check with Exists(p) first.
	// If p is a directory that already exists, nothing will be done and no error will be returned.
	Write(p path.Relative, data io.ReadCloser, recursive bool) error
}

// ExtWriteSeekable is an extention interface for a filesystem that adds the ability to seek in files.
type ExtWriteSeekable interface {
	ExtWriteable

	// WriteSeeker returns a WriteSeekCloser where Write and Seek can be called. Close must be called when the writing is finished
	WriteSeeker(p path.Relative) (WriteSeekCloser, error)
}

/*
TODO:

a wrapper could be made in order to allow WriteSeek for a filesystem that only implements ExtWriteable
this wrapper could read the full file into a cache (bytearray) and just change that part the should be overwritten
when the close method is used, the whole file is then written.
is slower, but can help (for small files) when filesystems can't deliver a seeker but the receiver (e.g. fuse) needs one
*/

// ExtDeleteable is an extension interface for a filesystem that adds the ability to delete files and folders.
// The deletion of folders may not be supported. In this case an error has to be returned, if the given path
// is a folder.
type ExtDeleteable interface {
	ReadOnly

	// Delete deletes either a file or a folder.
	// The deletion of folders may not be supported. In this case an error has to be returned, if the given path
	// is a folder.
	// If recursive is set to true, directories will be deleted, even if they are not empty.
	// If the given path does not exist, no error is returned.
	// If the given path can't be deleted, either because it is not empty and recursive was not set, or
	// because of missing permissions, an error is returned.
	Delete(p path.Relative, recursive bool) error
}

// ExtRenameable is an interface for a filesystem, that adds the ability to rename files and folders.
// The renaming of folders may not be supported. In this case an error has to be returned, if the given path
// is a folder.
type ExtRenameable interface {
	ReadOnly

	// Rename deletes either a file or a folder.
	// The given name must not be a path, so it must not contain path seperators.
	// If the given path does not exist, an error is returned.
	// If the renaming procedure was not successfull, an error is returned.
	// Please note that Rename behaves differently from the traditional unix operation with the same name.
	// Rename can't be used to move a file or folder to a different folder, but only to change the name of the
	// file or folder while staying in the same folder.
	// To move files, you need the Move function which would be more efficient, if the filesystem
	// supports the Moveable interface.
	Rename(p path.Relative, name string) error
}

// ExtModeable is an interface for a filesystem, that adds the ability to read and set FileMode (permissions) for files and folders.
// To allow the creation of files with restricted permissions, this interface includes
// a method to write with a given mode, which is not supported by the FS interface.
// ExtModeable should be implemented mainly by local filesystems.
type ExtModeable interface {
	ReadOnly

	// WriteWithMode behaves like Writeable.Write and only differs that the given filemod is used within the creation
	// (instead of the default filemods).
	WriteWithMode(p path.Relative, data io.ReadCloser, mode FileMode, recursive bool) error

	// GetMode returns the FileMode of the given path, or an error if the path does not exist.
	GetMode(p path.Relative) (FileMode, error)

	// SetMode set the given FileMode for the given path. If the path does not exist, an error is returned.
	SetMode(p path.Relative, m FileMode) error
}

// ExtMoveable is an interface for a filesystem, that adds the native / optimized (e.g. zero copy) ability to move files and folders.
type ExtMoveable interface {
	ReadOnly

	// Drive returns the drive (drive letter or UNC on windows, mountpoint of unix) of the given relative path
	// for moving between mountpoints or filesystem, just copy all files via CopyFile or CopyDir and
	// remove afterwards (better to check the copy by comparing checksums or the original and the new file)
	Drive(p path.Relative) (path.Local, error)

	// Move should return an error, if
	// - the src does not exist
	// - src is a directory and moving of directories is not supported by the fs
	// - the resulting path trgDir.Join(path.Name(src)) does already exist
	// - src and trgdir have different mountpoints
	Move(src path.Relative, trgDir path.Relative) error
}

type ExtSpaceReporter interface {
	ReadOnly

	// FreeSpace returns how many bytes of space are left on the mountpoint of the given path
	FreeSpace(p path.Relative) int64
}

// ExtMeta is an interface for a filesystem, that adds ability to set and get meta data
type ExtMeta interface {
	ReadOnly

	WriteWithMeta(p path.Relative, data io.ReadCloser, meta map[string][]byte, recursive bool) error
	SetMeta(p path.Relative, meta map[string][]byte) error
	GetMeta(p path.Relative) (map[string][]byte, error)
}

// ExtURL is an interface for a filesystem, that adds the ability to get url based information
type ExtURL interface {
	ReadOnly

	HostName() string
	Port() int
	UserName() string
	Password() string
	Scheme() string
}

type ExtGlob interface {
	ReadOnly

	Glob(pattern string) (matches []path.Relative, err error)
}

type ExtLock interface {
	ReadOnly

	Lock(p path.Relative) error
	Unlock(p path.Relative) error
}

type ExtClose interface {
	ReadOnly

	Close() error
}
