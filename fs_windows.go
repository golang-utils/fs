//go:build windows

package fs

import (
	"gitlab.com/golang-utils/fs/path"
	"golang.org/x/sys/windows"
)

func OnUnix() bool {
	return false
}

func OnWindows() bool {
	return true
}

func Drive(loc path.Local) (string, error) {
	dr := path.DriveLetter(loc)
	if dr == "" {
		return "", ErrExpectedWinDrive.Params("")
	}

	return dr, nil
}

// see https://stackoverflow.com/questions/20108520/get-amount-of-free-disk-space-using-go
func FreeSpace(drive string, freeBytesAvailable *uint64) error {
	if drive == "" {
		drive = "C:"
	}

	var totalNumberOfBytes, totalNumberOfFreeBytes uint64

	return windows.GetDiskFreeSpaceEx(
		windows.StringToUTF16Ptr(drive),
		freeBytesAvailable,
		&totalNumberOfBytes,
		&totalNumberOfFreeBytes,
	)
}
