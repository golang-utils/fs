package spec

import (
	"os"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/mockfs"
	"gitlab.com/golang-utils/fs/path"
)

func PrepareTest(conf Config, prefix string) (*mockfs.FS, path.Absolute) {

	last := "tests/"

	if prefix != "" {
		last = last + prefix + "/"
	}

	if conf.IsLocal {
		loc := path.MustWD().Join(last)
		fsys, err := mockfs.New(loc)
		if err != nil {
			panic(err.Error())
		}

		return fsys, loc
	}

	rem := path.MustRemote("http://localhost:6000/" + last)
	fsys, err := mockfs.New(rem)
	if err != nil {
		panic(err.Error())
	}
	return fsys, rem
}

func ClearFS(mfs *mockfs.FS, base path.Absolute, conf Config) {
	if conf.IsLocal {
		loc := base.(path.Local)
		//err := os.RemoveAll(loc.ToSystem())
		err := os.RemoveAll(path.ToSystem(loc.Dir()))
		/*
			if err != nil {
				panic(err.Error())
			}
		*/
		//time.Sleep(10 * time.Millisecond)
		err = os.MkdirAll(path.ToSystem(loc), 0755)
		/*
			if err != nil {
				panic(err.Error())
			}
		*/

		_ = err
	}

	err := fs.ClearDir(mfs, path.Relative(""))
	if err != nil {
		panic(err.Error())
	}

}
