package spec

type Config struct {
	PathPrefix  string
	IsLocal     bool
	Unsupported struct {
		ReadingFile   bool
		WritingFile   bool
		RenameFile    bool
		FileSize      bool
		DirSize       bool
		ModTimeDir    bool
		Abs           bool
		ReadRootDir   bool
		WriteRootDir  bool
		DeleteRootDir bool
	}
}

//func getSpec[T any]

/*
func Suite[T any](fn func(path.Absolute) T, c *Config) *spectest.Suite {
	suite := spectest.NewSuite("", "test suite for implementations of a fs")

	suite.AddSpec(Minimal(fn, c))

  fn(path.Absolute{})


	return suite
}
*/

/*
type Config struct {
	GetDeleteableFS    func() fs.Deleteable
	GetFS              func() fs.FS
	GetLocalFS         func() fs.Local
	GetMinimalFS       func() fs.Minimal
	GetModeableFS      func() fs.Modeable
	GetMoveableFS      func() fs.Moveable
	GetRemoteFS        func() fs.Remote
	GetRenameableFS    func() fs.Renameable
	GetSpaceReporterFS func() fs.SpaceReporter
	GetWriteableFS     func() fs.Writeable
}

func Suite(c *Config) *spectest.Suite {
	suite := spectest.NewSuite("", "test suite for implementations of a fs")

	if c.GetDeleteableFS != nil {
		suite.AddSpec(Deleteable(c.GetDeleteableFS))
	}

	if c.GetFS != nil {
		suite.AddSpec(FS(c.GetFS))
	}

	if c.GetLocalFS != nil {
		suite.AddSpec(Local(c.GetLocalFS))
	}

	if c.GetMinimalFS != nil {
		suite.AddSpec(Minimal(c.GetMinimalFS))
	}

	if c.GetModeableFS != nil {
		suite.AddSpec(Modeable(c.GetModeableFS))
	}

	if c.GetMoveableFS != nil {
		suite.AddSpec(Moveable(c.GetMoveableFS))
	}

	if c.GetRemoteFS != nil {
		suite.AddSpec(Remote(c.GetRemoteFS))
	}

	if c.GetRenameableFS != nil {
		suite.AddSpec(Renameable(c.GetRenameableFS))
	}

	if c.GetSpaceReporterFS != nil {
		suite.AddSpec(SpaceReporter(c.GetSpaceReporterFS))
	}

	if c.GetWriteableFS != nil {
		suite.AddSpec(Writeable(c.GetWriteableFS))
	}

	return suite
}
*/
