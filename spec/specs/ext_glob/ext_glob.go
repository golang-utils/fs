package ext_glob

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

type spectests struct {
	conf spec.Config
	fn   func(prepare fs.ReadOnly, base path.Absolute) fs.ExtGlob
}

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtGlob) *spectest.Spec {
	spec := spectest.NewSpec("ExtGlob", "test spec for the ExtGlob interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ExtGlob {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}
	spec.AddTest("find via glob", tests.testGlob)
	return spec
}
