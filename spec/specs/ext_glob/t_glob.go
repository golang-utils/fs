package ext_glob

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testGlob(t *testing.T) {
	if !fstest.conf.IsLocal {
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testGlob")

	tests := []struct {
		createFiles []string
		pattern     string
		result      []path.Relative
	}{
		{
			[]string{fstest.conf.PathPrefix + "tester_dfd/dfdf/file.txt", fstest.conf.PathPrefix + "tester_dfd/dfddsfdsf/fiererle.txt"},
			fstest.conf.PathPrefix + "tester_dfd/*/*.txt",
			[]path.Relative{path.Relative(fstest.conf.PathPrefix + "tester_dfd/dfddsfdsf/fiererle.txt"), path.Relative(fstest.conf.PathPrefix + "tester_dfd/dfdf/file.txt")},
		},

		{
			[]string{fstest.conf.PathPrefix + "tester_dfd/dfdf/file.txt", fstest.conf.PathPrefix + "tester_dfd/dfddsfdsf/fiererle.txt"},
			fstest.conf.PathPrefix + "tester_dfd/*",
			[]path.Relative{path.Relative(fstest.conf.PathPrefix + "tester_dfd/dfddsfdsf/"), path.Relative(fstest.conf.PathPrefix + "tester_dfd/dfdf/")},
		},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		for _, file := range test.createFiles {
			err := prepareFS.Write(path.Relative(file), fs.ReadCloser(strings.NewReader("blah")), true)

			if err != nil {
				t.Errorf("could not prepare file system: %v\n", err)
				continue
			}
		}

		fsys := fstest.fn(prepareFS, base)

		//err := test.fs.Mkdir(path.Relative(test.dir), true, 0755)
		for _, file := range test.createFiles {
			if !fsys.Exists(path.Relative(file)) {
				t.Errorf("[%v] %T: file %q not created", i, fsys, file)
				continue
			}
		}

		matches, err := fsys.Glob(test.pattern)

		if err != nil {
			t.Errorf("[%v] %T: could not Glob with pattern %q: %v", i, fsys, test.pattern, err)
			continue
		}

		if !reflect.DeepEqual(matches, test.result) {
			t.Errorf("[%v] %T: Glob(%q) = %v // expected %v", i, fsys, test.pattern, matches, test.result)
			continue
		}

	}

}
