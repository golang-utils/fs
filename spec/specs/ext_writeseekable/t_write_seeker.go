package ext_writeseekable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteSeeker(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteSeeker")

	tests := []struct {
		file     string
		content  string
		offset   int64
		write    string
		expected string
		err      bool
	}{
		{"a2/b/c.txt", "huhoha-heissasa", 0, "hoha", "hohaha-heissasa", false},
		{"a1/b.txt", "huhoha-heissasa", 0, "hoha", "hohaha-heissasa", false},
		{"a.txt", "huhoha-heissasa", 6, "XX", "huhohaXXeissasa", false},
		{"a.txt", "huhoha-heissasa", -1, "XX", "huhohaXXeissasa", true},
		{"a2/b/c.txt", "huhoha-heissasa", 15, "hoha", "huhoha-heissasahoha", false},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		err := fs.WriteFile(prepareFS, path.Relative(test.file), []byte(test.content), true)

		if err != nil {
			t.Fatalf("[%v] could not write %s: %v", i, test.file, err)
		}

		ws, err := prepareFS.WriteSeeker(path.Relative(test.file))

		if err != nil {
			t.Fatalf("[%v] could not get writeSeeker %s: %v", i, test.file, err)
		}

		err = ws.Seek(test.offset)

		if err != nil && !test.err {
			t.Fatalf("[%v] could not seek offset %v in %s: %v", i, test.offset, test.file, err)
		}

		if err == nil && test.err {
			t.Fatalf("[%v] should not be able to seek offset %v in %s, but got no errors", i, test.offset, test.file)
		}

		if test.err {
			continue
		}

		_, err = ws.Write([]byte(test.write))

		if err != nil {
			t.Fatalf("[%v] could not write to %s (offset %v): %v", i, test.file, test.offset, err)
		}

		err = ws.Close()

		if err != nil {
			t.Fatalf("[%v] could not close %s (offset %v): %v", i, test.file, test.offset, err)
		}

		res, err := fs.ReadFile(prepareFS, path.Relative(test.file))

		if err != nil {
			t.Fatalf("[%v] could not read %s: %v", i, test.file, err)
		}

		got := string(res)
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] %s got %q // expected %q", i, test.file, got, expected)
		}
	}

	///////////////////////////////////

	/*
		tests := []struct {
			dir string
		}{
			//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
			//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
			{fstest.conf.PathPrefix + "testdir_b/g/f/"},
			{fstest.conf.PathPrefix + "testdir_b/"},
			//	{true, subdir, "testdir_b/g/f/"},
			//	{true, subdir, "testdir b/a b/f/"},
			//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
			//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
		}

		for i, test := range tests {

			spec.ClearFS(prepareFS, base, fstest.conf)

			rel := path.Relative(test.dir)

			err := prepareFS.Write(rel.Dir(), nil, true)

			if err != nil {
				t.Errorf("[%v] could not create parent dir %q: %v", i, rel.Dir().String(), err)
				continue
			}

			fsys := fstest.fn(prepareFS, base)

			err = fs.MkDir(fsys, rel)

			if err != nil {
				t.Errorf("[%v] could not create dir %q: %v", i, test.dir, err)
			}

			if !fsys.Exists(rel) {
				t.Errorf("[%v] dir %q not created", i, test.dir)
			}

		}
	*/
}
