package ext_writeseekable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

// DONE

type spectests struct {
	conf spec.Config
	fn   func(prepareFS fs.ReadOnly, base path.Absolute) fs.ExtWriteSeekable
}

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prepareFS fs.ReadOnly, base path.Absolute) fs.ExtWriteSeekable) *spectest.Spec {
	spec := spectest.NewSpec("ExtWriteSeekable", "test spec for the ExtWriteSeekable interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ExtWriteSeekable {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}

	write := spectest.NewSpec("WriteSeeker", "WriteSeeker returns a WriteSeeker that allows for seeking and writing")

	write.AddTest("write seeker test", tests.testWriteSeeker)
	spec.AddSubSpec(write)

	return spec
}
