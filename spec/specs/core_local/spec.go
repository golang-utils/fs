package core_local

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_fs"
	"gitlab.com/golang-utils/fs/spec/specs/ext_glob"
	"gitlab.com/golang-utils/fs/spec/specs/ext_modeable"
	"gitlab.com/golang-utils/fs/spec/specs/ext_moveable"
	"gitlab.com/golang-utils/fs/spec/specs/ext_renameable"
	"gitlab.com/golang-utils/fs/spec/specs/ext_seekable"
	"gitlab.com/golang-utils/fs/spec/specs/ext_spacereporter"
	"gitlab.com/golang-utils/fs/spec/specs/ext_writeseekable"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prepareFS fs.ReadOnly, base path.Local) fs.Local) *spectest.Spec {
	c.IsLocal = true
	spec := spectest.NewSpec("Local", "test spec for the Local interface")

	fnMod := func(prepare fs.ReadOnly, p path.Absolute) fs.Local {
		loc, ok := p.(path.Local)

		if ok {
			return fn(prepare, loc)
		}

		panic("not  a local fs")
	}

	fnFS := func(prepare fs.ReadOnly, p path.Absolute) fs.FS {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(core_fs.Spec(c, fnFS))

	fnMv := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtMoveable {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_moveable.Spec(c, fnMv))

	fnMd := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtModeable {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_modeable.Spec(c, fnMd))

	fnSr := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtSpaceReporter {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_spacereporter.Spec(c, fnSr))

	fnRn := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtRenameable {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_renameable.Spec(c, fnRn))

	fnSk := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtReadSeekable {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_seekable.Spec(c, fnSk))

	fnGl := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtGlob {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_glob.Spec(c, fnGl))

	fnws := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtWriteSeekable {
		return fnMod(prepare, p)
	}
	spec.AddSubSpec(ext_writeseekable.Spec(c, fnws))

	return spec
}
