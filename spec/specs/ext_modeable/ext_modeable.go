package ext_modeable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtModeable) *spectest.Spec {
	spec := spectest.NewSpec("ExtModeable", "test spec for the ExtModeable interface")

	/*
		write := spectest.NewSpec("WriteWithMode", "WriteWithMode behaves like Writeable.Write and only differs that the given filemod is used within the creation.")

		write.AddTest("write file", func(t *testing.T) {

		})

		write.AddTest("overwrite file", func(t *testing.T) {

		})

		write.AddTest("write file recursive", func(t *testing.T) {

		})

		write.AddTest("write dir", func(t *testing.T) {

		})

		write.AddTest("overwrite dir", func(t *testing.T) {

		})

		write.AddTest("write dir recursive", func(t *testing.T) {

		})

		spec.AddSubSpec(write)
	*/

	/*
		get := spectest.NewSpec("GetMode", "Rename renames either a file or a folder.")
		get.AddTest("get mode file", func(t *testing.T) {})
		get.AddTest("get mode dir", func(t *testing.T) {})
		spec.AddSubSpec(get)
	*/

	/*
		set := spectest.NewSpec("SetMode", "Rename renames either a file or a folder.")
		set.AddTest("set mode file", func(t *testing.T) {})
		set.AddTest("set mode dir", func(t *testing.T) {})
		spec.AddSubSpec(set)
	*/
	return spec
}
