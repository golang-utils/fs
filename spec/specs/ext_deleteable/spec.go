package ext_deleteable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

// DONE

type spectests struct {
	conf spec.Config
	fn   func(prepare fs.ReadOnly, base path.Absolute) fs.ExtDeleteable
}

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prepare fs.ReadOnly, base path.Absolute) fs.ExtDeleteable) *spectest.Spec {
	s := spectest.NewSpec("ExtDeleteable", "test spec for the ExtDeleteable interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ExtDeleteable {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}

	del := spectest.NewSpec("Delete", "// Delete deletes either a file or a folder.")
	del.AddTest("delete file", tests.testRmFile)

	del.AddTest("delete directory recursively", tests.testRmDirAll)
	del.AddTest("delete empty directory", tests.testRmDirEmpty)
	del.AddTest("delete directory with files", tests.testRmDirWithFiles)
	del.AddTest("delete directory with dirs", tests.testRmDirWithDirs)
	del.AddTest("delete non existing file", tests.testRmFileNotFound)
	del.AddTest("delete non existing dir", tests.testRmDirNotFound)
	del.AddTest("delete not root dir", tests.testRmDirRootDir)

	s.AddSubSpec(del)
	return s
}
