package ext_deleteable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirEmpty(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirEmpty")

	tests := []struct {
		dir string
	}{
		{fstest.conf.PathPrefix + "testdir_f/p/y/"},
		{fstest.conf.PathPrefix + "testdir_g/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		err := fs.MkDirAll(prepareFS, path.Relative(test.dir))

		if err != nil {
			t.Fatalf("[%v] %T: could not create dir %q: %v", i, prepareFS, test.dir, err)
		}

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q not created", i, fsys, test.dir)
		}

		err = fsys.Delete(path.Relative(test.dir), false)

		if err != nil {
			t.Fatalf("[%v] %T: could not remove dir %q: %v", i, fsys, test.dir, err)
		}

		if fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q is not removed", i, fsys, test.dir)
		}
	}

}
