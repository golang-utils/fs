package ext_deleteable

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirWithFiles(t *testing.T) {

	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirWithFiles")

	tests := []struct {
		file string
		dir  string
	}{
		{fstest.conf.PathPrefix + "testdir_f/p/y/k", fstest.conf.PathPrefix + "testdir_f/p/y/"},
		{fstest.conf.PathPrefix + "testdir_g/a", fstest.conf.PathPrefix + "testdir_g/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader("content")), true)

		if err != nil {
			t.Fatalf("[%v] %T: could not create file %q: %v", i, prepareFS, test.file, err)
		}

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(path.Relative(test.file)) {
			t.Errorf("[%v] %T: file %q not created", i, fsys, test.file)
		}

		if !fsys.Exists(path.Relative(test.file).Dir()) {
			t.Errorf("[%v] %T: dir %q not created", i, fsys, path.Relative(test.file).Dir().String())
		}

		err = fsys.Delete(path.Relative(test.dir), false)

		if err == nil {
			t.Errorf("[%v] %T: expected error when removing dir %q: %v", i, fsys, test.dir, err)
			continue
		}

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q was deleted although it should not have been", i, fsys, test.dir)
		}

		if !fsys.Exists(path.Relative(test.file)) {
			t.Errorf("[%v] %T: file %q was deleted although it should not have been", i, fsys, test.file)
		}

		if !errors.Is(err, fs.ErrNotEmpty) {
			t.Errorf("[%v] %T: when removing dir %q expected error %v but got error: %v", i, fsys, test.dir, fs.ErrNotEmpty.Params(test.dir), err)
		}

		//fmt.Printf("%s\n", err.Error())

	}

}
