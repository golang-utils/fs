package ext_deleteable

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirAllRemote(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirAllRemote")

	spec.ClearFS(prepareFS, base, fstest.conf)
	err := prepareFS.Write(path.Relative(fstest.conf.PathPrefix+"a/b/c/d/"), fs.ReadCloser(strings.NewReader("hiho")), true)

	fsys := fstest.fn(prepareFS, base)

	if !fsys.Exists(path.Relative(fstest.conf.PathPrefix + "a/b/c/d/")) {
		t.Errorf(fstest.conf.PathPrefix + "a/b/c/d/ should exist")
	}

	err = fsys.Delete(path.Relative(fstest.conf.PathPrefix+"a/b/c/d/"), true)

	if err != nil {
		t.Fatalf("error: %s", err.Error())
	}

	if fsys.Exists(path.Relative(fstest.conf.PathPrefix + "a/b/c/d/")) {
		t.Errorf("a/b/c/d/ should not exist anymore")
	}
}

func (fstest *spectests) testRmDirAll(t *testing.T) {
	//	t.Skip()
	if !fstest.conf.IsLocal {
		fstest.testRmDirAllRemote(t)
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirAll")

	tests := []struct {
		forlocal bool
		dir      string
		rmdir    string
	}{
		//{"Memory", Memory(), "testdir_a"},
		//{"Map", Map(), "testdir_c"},
		//{false, path.MustLocal("/map/"), "testdir_c/", "testdir_c/"},
		{true, fstest.conf.PathPrefix + "testdir_f/p/y/", fstest.conf.PathPrefix + "testdir_f/"},
		//{false, path.MustLocal("C:/tests/"), "testdir_b/p/y/", "testdir_b/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		if fstest.conf.IsLocal != test.forlocal {
			continue
		}

		err := fs.MkDirAll(prepareFS, path.Relative(test.dir))

		if err != nil {
			t.Fatalf("[%v] %T: could not create dir %q: %v", i, prepareFS, test.dir, err)
		}

		fsys := fstest.fn(prepareFS, base)

		//err := test.fs.Mkdir(path.Relative(test.dir), true, 0755)

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q not created", i, fsys, test.dir)
		}

		if !path.IsDir(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: %q is not a dir", i, fsys, test.dir)
		}

		err = fsys.Delete(path.Relative(test.rmdir), true)

		if err != nil {
			t.Fatalf("[%v] %T: could not remove dir %q: %v", i, fsys, test.rmdir, err)
		}

		if fsys.Exists(path.Relative(test.rmdir)) {
			t.Errorf("[%v] %T: dir %q is not removed", i, fsys, test.rmdir)
		}

		if fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q is not removed", i, fsys, test.dir)
		}
	}

}
