package ext_deleteable

import (
	"testing"

	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirRootDir(t *testing.T) {
	if fstest.conf.Unsupported.DeleteRootDir {
		t.Skip()
		return
	}

	/*
		if fstest.conf.Unsupported.WritingFile {
			t.Skip()
			return
		}
	*/

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirRootDir")

	tests := []struct {
		dir string
	}{
		{fstest.conf.PathPrefix + ""},
		//		{fstest.conf.PathPrefix + "/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: root dir %q does not exist", i, fsys, test.dir)
		}

		err := fsys.Delete(path.Relative(test.dir), false)

		if err == nil {
			t.Errorf("[%v] %T: expected error when removing root dir %q: %v", i, fsys, test.dir, err)
			continue
		}

		err = fsys.Delete(path.Relative(test.dir), true)

		if err == nil {
			t.Errorf("[%v] %T: expected error when removing root dir %q recursively: %v", i, fsys, test.dir, err)
			continue
		}

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: root dir %q was deleted although it should not have been", i, fsys, test.dir)
		}

	}

}
