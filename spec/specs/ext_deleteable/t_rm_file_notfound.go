package ext_deleteable

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmFileNotFound(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmFileNotFound")

	tests := []struct {
		file string
		data string
	}{
		//{"Memory", Memory(), "testdir_a"},
		//{"Map", Map(), "testdir_c"},
		//{false, path.MustLocal("/map/"), "testdir_c/", "testdir_c/"},
		{fstest.conf.PathPrefix + "testdir_f/p/y", "content"},
		{fstest.conf.PathPrefix + "testdir_f/pq", "contentXX"},
		//{false, path.MustLocal("C:/tests/"), "testdir_b/p/y/", "testdir_b/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Errorf("[%v] %T: could not create file %q: %v", i, prepareFS, test.file, err)
			continue
		}

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(path.Relative(test.file)) {
			t.Errorf("[%v] %T: file %q not created", i, fsys, test.file)
			continue
		}

		queryFile := test.file + "X"

		err = fsys.Delete(path.Relative(queryFile), false)

		if err != nil {
			t.Errorf("[%v] %T: could not remove file %q: %v", i, fsys, queryFile, err)
			continue
		}

		if fsys.Exists(path.Relative(queryFile)) {
			t.Errorf("[%v] %T: file %q is not removed", i, fsys, queryFile)
		}
	}

}
