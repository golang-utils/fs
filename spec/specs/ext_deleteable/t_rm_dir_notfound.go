package ext_deleteable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirNotFound(t *testing.T) {

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirNotFound")

	tests := []struct {
		forlocal bool
		dir      string
		rmdir    string
	}{
		//{"Memory", Memory(), "testdir_a"},
		//{"Map", Map(), "testdir_c"},
		//{false, path.MustLocal("/map/"), "testdir_c/", "testdir_c/"},
		{true, fstest.conf.PathPrefix + "testdir_f/p/y/", fstest.conf.PathPrefix + "testdir_f/"},
		//{false, path.MustLocal("C:/tests/"), "testdir_b/p/y/", "testdir_b/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		if fstest.conf.IsLocal != test.forlocal {
			continue
		}

		err := fs.MkDirAll(prepareFS, path.Relative(test.dir))

		if err != nil {
			t.Fatalf("[%v] %T: could not create dir %q: %v", i, prepareFS, test.dir, err)
		}

		fsys := fstest.fn(prepareFS, base)

		//err := test.fs.Mkdir(path.Relative(test.dir), true, 0755)

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q not created", i, fsys, test.dir)
		}

		queryDir := "X" + test.rmdir

		err = fsys.Delete(path.Relative(queryDir), false)

		if err != nil {
			t.Fatalf("[%v] %T: could not remove dir %q: %v", i, fsys, queryDir, err)
		}

		if fsys.Exists(path.Relative(queryDir)) {
			t.Errorf("[%v] %T: dir %q is not removed", i, fsys, queryDir)
		}
	}

}
