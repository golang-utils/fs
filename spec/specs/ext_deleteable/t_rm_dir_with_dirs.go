package ext_deleteable

import (
	"errors"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRmDirWithDirs(t *testing.T) {

	/*
		if fstest.conf.Unsupported.WritingFile {
			t.Skip()
			return
		}
	*/

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRmDirWithDirs")

	tests := []struct {
		innerdir string
		dir      string
	}{
		{fstest.conf.PathPrefix + "testdir_f/p/y/k/", fstest.conf.PathPrefix + "testdir_f/p/y/"},
		{fstest.conf.PathPrefix + "testdir_g/a/", fstest.conf.PathPrefix + "testdir_g/"},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstest.conf)

		err := prepareFS.Write(path.Relative(test.innerdir), nil, true)

		if err != nil {
			t.Fatalf("[%v] %T: could not create innerdir %q: %v", i, prepareFS, test.innerdir, err)
		}

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(path.Relative(test.innerdir)) {
			t.Errorf("[%v] %T: innerdir %q not created", i, fsys, test.innerdir)
		}

		if !fsys.Exists(path.Relative(test.innerdir).Dir()) {
			t.Errorf("[%v] %T: dir %q not created", i, fsys, path.Relative(test.innerdir).Dir().String())
		}

		err = fsys.Delete(path.Relative(test.dir), false)

		if err == nil {
			t.Errorf("[%v] %T: expected error when removing dir %q: %v", i, fsys, test.dir, err)
			continue
		}

		if !fsys.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %T: dir %q was deleted although it should not have been", i, fsys, test.dir)
		}

		if !fsys.Exists(path.Relative(test.innerdir)) {
			t.Errorf("[%v] %T: innerdir %q was deleted although it should not have been", i, fsys, test.innerdir)
		}

		if !errors.Is(err, fs.ErrNotEmpty) {
			t.Errorf("[%v] %T: when removing dir %q expected error %v but got error: %v", i, fsys, test.dir, fs.ErrNotEmpty.Params(test.dir), err)
		}

		//fmt.Printf("%s\n", err.Error())

	}

}
