package core_readonly

import (
	"testing"

	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testAbsFile(t *testing.T) {
	if ro.conf.Unsupported.Abs {
		t.Skip()
		return
	}
	//fmt.Println("testAbs did run")
	prepareFS, base := spec.PrepareTest(ro.conf, "testAbsFile")

	tests := []struct {
		forLocal bool
		fsroot   path.Absolute
		path     string
		expected string
	}{
		{true, base, ro.conf.PathPrefix + "testdir_b/g/f", prepareFS.Abs(path.Relative("testdir_b/g/f")).String()},
		//{true, base, "testdir_b/g/f/", prepareFS.Abs(path.Relative("testdir_b/g/f/")).String()},
		{false, base, ro.conf.PathPrefix + "testdir_b/g/f", "http://localhost:6000/tests/testAbsFile/testdir_b/g/f"},
		//{false, base, "testdir_b/g/f/", "http://localhost:6000/tests/testdir_b/g/f/"},
		{false, base, ro.conf.PathPrefix + "testdir_b/g/f", prepareFS.Abs(path.Relative("testdir_b/g/f")).String()},
		//{false, base, "testdir_b/g/f/", prepareFS.Abs(path.Relative("testdir_b/g/f/")).String()},
	}

	for i, test := range tests {
		if test.forLocal != ro.conf.IsLocal {
			continue
		}

		spec.ClearFS(prepareFS, base, ro.conf)

		fsys := ro.fn(prepareFS, base)

		got := fsys.Abs(path.Relative(test.path))

		expected := test.expected
		if got.String() != expected {
			t.Errorf("[%v] %T(%s).Abs(%q) = %q // expected: %q", i, fsys, test.fsroot, test.path, got.String(), expected)
		}
	}
}
