package core_readonly

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testExistsFile(t *testing.T) {
	if ro.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(ro.conf, "testExistsFile")

	tests := []struct {
		descr    string
		writing  []string
		expected []string
	}{
		{"A",
			[]string{ro.conf.PathPrefix + "m/b/f", ro.conf.PathPrefix + "m/b/g", ro.conf.PathPrefix + "m/b/h"},
			[]string{ro.conf.PathPrefix + "m/b/g", ro.conf.PathPrefix + "m/b/h", ro.conf.PathPrefix + "m/b/f"},
		},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, ro.conf)

		for _, filewrite := range test.writing {
			file := path.Relative(filewrite)
			err := prepareFS.Write(file, fs.ReadCloser(strings.NewReader("content")), true)
			if err != nil {
				t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, filewrite, err)
				continue
			}

			if !prepareFS.Exists(file) {
				if err != nil {
					t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, filewrite, err)
					continue
				}
			}
		}

		fsys := ro.fn(prepareFS, base)

		for _, expFile := range test.expected {
			if !fsys.Exists(path.Relative(expFile)) {
				t.Errorf("[%v] %s: file %q not there", i, test.descr, expFile)
			}
		}
	}
}
