package core_readonly

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testLastModFile(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testLastModFile")

	now := time.Now()

	fmtstr := "YYYY/MM/DD hh"
	nowdate := fmtdate.Format(fmtstr, now.UTC())

	tests := []struct {
		file string
		data string
	}{
		{fstest.conf.PathPrefix + "fztutu", "content"},
		{fstest.conf.PathPrefix + "testdir_b/g/fttztz", "contentXYZ"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		mt, err := fsys.ModTime(path.Relative(test.file))

		if err != nil {
			t.Errorf("[%v] modtime of %q returned error %v", i, test.file, err)
			continue
		}

		got := fmtdate.Format(fmtstr, mt.UTC())

		if got != nowdate {
			t.Errorf("[%v] %T.ModTime(%q) returned %v // expected %v", i, fsys, test.file, got, nowdate)
		}
	}

}
