package core_readonly

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testReaderNonExistingFile(t *testing.T) {
	if ro.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(ro.conf, "testReaderNonExistingFile")

	tests := []struct {
		descr    string
		writing  []string
		expected []string
	}{
		{"A",
			[]string{ro.conf.PathPrefix + "m/b/f", ro.conf.PathPrefix + "m/b/g", ro.conf.PathPrefix + "m/b/h"},
			[]string{ro.conf.PathPrefix + "m/b/g2", ro.conf.PathPrefix + "m/b/h2", ro.conf.PathPrefix + "m/b/f1"},
		},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, ro.conf)

		for _, filewrite := range test.writing {
			file := path.Relative(filewrite)
			err := prepareFS.Write(file, fs.ReadCloser(strings.NewReader("content")), true)
			if err != nil {
				t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, filewrite, err)
				continue
			}

			if !prepareFS.Exists(file) {
				if err != nil {
					t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, filewrite, err)
					continue
				}
			}
		}

		//	fmt.Println(base.String())
		fsys := ro.fn(prepareFS, base)

		for _, expFile := range test.expected {
			_, err := fsys.Reader(path.Relative(expFile))

			if err == nil {
				t.Errorf("[%v] %s: file %q should return an error, but does not", i, test.descr, expFile)
			} else {
				if !errors.Is(err, fs.ErrNotFound) {
					t.Errorf("[%v] %s: returned error must be fs.ErrNotFound, but is %T", i, test.descr, err)
				}
			}
		}
	}
}
