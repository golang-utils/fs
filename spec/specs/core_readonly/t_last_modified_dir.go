package core_readonly

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/golang-utils/fmtdate"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testLastModDir(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	if fstest.conf.Unsupported.ModTimeDir {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testLastModDir")

	now := time.Now()

	fmtstr := "YYYY/MM/DD hh"
	nowdate := fmtdate.Format(fmtstr, now.UTC())

	tests := []struct {
		file string
		data string
		dir  string
	}{
		{fstest.conf.PathPrefix + "testdir_b/f", "content", fstest.conf.PathPrefix + "testdir_b/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f", "contentXYZ", fstest.conf.PathPrefix + "testdir_b/g/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		mt, err := fsys.ModTime(path.Relative(test.dir))

		if err != nil {
			t.Errorf("[%v] modtime of %q returned error %v", i, test.dir, err)
			continue
		}

		got := fmtdate.Format(fmtstr, mt.UTC())

		if got != nowdate {
			t.Errorf("[%v] %T.ModTime(%q) returned %v // expected %v", i, fsys, test.dir, got, nowdate)
		}
	}

}
