package core_readonly

import (
	"testing"

	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testLastModRootDir(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	if fstest.conf.Unsupported.ModTimeDir {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testLastModRootDir")

	tests := []struct {
		dir string
	}{
		//	{fstest.conf.PathPrefix + "testdir_b/f", "content", fstest.conf.PathPrefix + "testdir_b/"},
		//	{fstest.conf.PathPrefix + "testdir_b/g/f", "contentXYZ", fstest.conf.PathPrefix + "testdir_b/g/"},
		{fstest.conf.PathPrefix + ""},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		_, err := fsys.ModTime(path.Relative(test.dir))

		if err == nil {
			t.Errorf("[%v] modtime of root dir %q should return error, but did not", i, test.dir)
			continue
		}
	}

}
