package core_readonly

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testReadRootDir(t *testing.T) {
	if ro.conf.Unsupported.ReadRootDir {
		t.Skip()
		return
	}

	prepareFS, base := spec.PrepareTest(ro.conf, "testReadRootDir")

	tests := []struct {
		descr      string
		filesWrite []string
		dirRead    string
		expected   []string
		hasErr     bool
	}{

		{"Y2",
			[]string{},
			"",
			[]string{},
			false},
		{"Y3",
			[]string{},
			".",
			[]string{},
			false},
		{"Y4",
			[]string{},
			"/",
			[]string{},
			false},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, ro.conf)

		for _, filewrite := range test.filesWrite {
			file := path.Relative(filewrite)
			dir := file.Dir()

			if !prepareFS.Exists(dir) {
				err := fs.MkDirAll(prepareFS, dir)
				if err != nil {
					t.Errorf("[%v] %s: could not create dir in prepare fs %q: %v", i, test.descr, dir, err)
					continue
				}
			}

			rd := strings.NewReader("content")
			err := prepareFS.Write(file, fs.ReadCloser(rd), true)

			if err != nil {
				t.Errorf("[%v] %s: could not write file in prepare fs %q: %T %v", i, test.descr, filewrite, err, err)
				continue
			}

			if !prepareFS.Exists(file) {
				t.Errorf("[%v] %s: file %q not created in prepare fs", i, test.descr, filewrite)
			}
		}

		fsys := ro.fn(prepareFS, base)

		dir := path.Relative(test.dirRead)

		if !fsys.Exists(dir) {
			t.Errorf("[%v] %s: dir %q not there", i, test.descr, test.dirRead)
		}

		if !path.IsDir(dir) {
			t.Errorf("[%v] %s: %q is not a dir", i, test.descr, test.dirRead)
		}

		results, err := fs.ReadDirNames(fsys, dir)

		if err != nil {
			t.Errorf("[%v] %s: could not read dir %q: %v", i, test.descr, test.dirRead, err)
			continue
		}

		if ro.conf.PathPrefix == "" {
			if got, want := results, test.expected; !reflect.DeepEqual(got, want) {
				t.Errorf("[%v] %s: content of directory %q = %#v // expected %#v", i, test.descr, test.dirRead, got, want)
			}
		} else {
			exp := []string{ro.conf.PathPrefix}
			if got, want := results, exp; !reflect.DeepEqual(got, want) {
				t.Errorf("[%v] %s: content of directory %q = %#v // expected %#v", i, test.descr, test.dirRead, got, want)
			}
		}

	}
}
