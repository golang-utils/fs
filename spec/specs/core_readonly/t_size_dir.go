package core_readonly

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testSizeDir(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testSizeDir")

	type file struct {
		path string
		data string
	}

	tests := []struct {
		files []file
		dir   string
		size  int64
	}{
		{
			[]file{
				{fstest.conf.PathPrefix + "testdir_b/g/k", "content"},
				{fstest.conf.PathPrefix + "testdir_b/g/34f", "contentXYZ"},
			},
			fstest.conf.PathPrefix + "testdir_b/g/",
			17,
		},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		for _, file := range test.files {
			err := prepareFS.Write(path.Relative(file.path), fs.ReadCloser(strings.NewReader(file.data)), true)

			if err != nil {
				t.Fatalf("could not prepare file system: %v\n", err)
			}

		}

		fsys := fstest.fn(prepareFS, base)

		expected := test.size

		if fstest.conf.Unsupported.DirSize {
			expected = -2
		}

		got := fsys.Size(path.Relative(test.dir))

		if got != int64(expected) {
			t.Errorf("[%v] %T.Size(%q) returned %v // expected %v", i, fsys, test.dir, got, expected)
		}
	}

}
