package core_readonly

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testSizeFileNotFound(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testSizeFileNotFound")

	tests := []struct {
		file     string
		data     string
		fileRead string
	}{
		{fstest.conf.PathPrefix + "fgg", "content", fstest.conf.PathPrefix + "f1"},
		{fstest.conf.PathPrefix + "testdir_b/g/fdfgfd", "contentXYZ", fstest.conf.PathPrefix + "testdir_b/g/f1"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		expected := -1

		if fstest.conf.Unsupported.FileSize {
			expected = -2
		}

		got := fsys.Size(path.Relative(test.fileRead))

		if got != int64(expected) {
			t.Errorf("[%v] %T.Size(%q) returned %v // expected %v", i, fsys, test.data, got, expected)
		}
	}

}
