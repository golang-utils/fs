package core_readonly

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testLastModFileNotFound(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testLastModFileNotFound")

	tests := []struct {
		file string
		data string
	}{
		{fstest.conf.PathPrefix + "fff", "content"},
		{fstest.conf.PathPrefix + "testdir_b/g/fff", "contentXYZ"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		queryFile := test.file + "X"

		_, err = fsys.ModTime(path.Relative(queryFile))

		if err == nil {
			t.Errorf("[%v] modtime of %q should return error, but did not", i, queryFile)
			continue
		}

		if !errors.Is(err, fs.ErrNotFound) {
			t.Errorf("[%v] %T.ModTime(%q) returned error %v // expected %v", i, fsys, queryFile, err, fs.ErrNotFound.Params(queryFile))
		}
	}

}
