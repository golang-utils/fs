package core_readonly

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testReaderNonExistingDir(t *testing.T) {
	prepareFS, base := spec.PrepareTest(ro.conf, "testReaderNonExistingDir")

	tests := []struct {
		descr    string
		writing  []string
		expected []string
	}{
		{"A",
			[]string{ro.conf.PathPrefix + "m/b/f/", ro.conf.PathPrefix + "m/b/g/", ro.conf.PathPrefix + "m/b/h/"},
			[]string{ro.conf.PathPrefix + "m/b/g2/", ro.conf.PathPrefix + "m/b/h2/", ro.conf.PathPrefix + "m/b/f1/"},
		},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, ro.conf)

		for _, dirwrite := range test.writing {
			dir := path.Relative(dirwrite)
			err := prepareFS.Write(dir, fs.ReadCloser(strings.NewReader("content")), true)
			if err != nil {
				t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, dirwrite, err)
				continue
			}

			if !prepareFS.Exists(dir) {
				if err != nil {
					t.Errorf("[%v] %s: could not create file in prepare fs %q: %v", i, test.descr, dirwrite, err)
					continue
				}
			}
		}

		//fmt.Println(base.String())
		fsys := ro.fn(prepareFS, base)

		for _, expDir := range test.expected {
			_, err := fsys.Reader(path.Relative(expDir))

			if err == nil {
				t.Errorf("[%v] %s: dir %q should return an error, but does not", i, test.descr, expDir)
			} else {
				if !errors.Is(err, fs.ErrNotFound) {
					t.Errorf("[%v] %s: returned error must be fs.ErrNotFound, but is %T", i, test.descr, err)
				}
			}
		}
	}
}
