package core_readonly

import (
	"io"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testReaderFile(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testReaderFile")

	tests := []struct {
		file string
		data string
	}{
		{fstest.conf.PathPrefix + "f", "content"},
		{fstest.conf.PathPrefix + "testdir_b/g/f", "content"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		rd2, err := fsys.Reader(path.Relative(test.file))

		if err != nil {
			t.Fatalf("[%v] %T: could not read from file %q: %v", i, fsys, test.file, err)
		}

		bt, err := io.ReadAll(rd2)

		if err != nil {
			t.Fatalf("[%v] %T: could not readall from file %q: %v", i, fsys, test.file, err)
		}

		if got, want := string(bt), test.data; got != want {
			t.Errorf("[%v] %T content of %q = %q // expected %q", i, fsys, test.file, got, want)
		}
	}

}
