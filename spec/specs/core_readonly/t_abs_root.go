package core_readonly

import (
	"testing"

	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstests *spectests) testAbsRootRemote(t *testing.T) {
	//fmt.Println("testAbs did run")
	prepareFS, base := spec.PrepareTest(fstests.conf, "testAbsRootRemote")

	tests := []struct {
		path     string
		expected string
	}{
		{fstests.conf.PathPrefix + "/", prepareFS.URL.String()},
		{fstests.conf.PathPrefix + "", prepareFS.URL.String()},
		{fstests.conf.PathPrefix + ".", prepareFS.URL.String()},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstests.conf)
		fsys := fstests.fn(prepareFS, base)

		got := fsys.Abs(path.Relative(test.path))

		expected := test.expected
		if got.String() != expected {
			t.Errorf("[%v] %T.Abs(%q) = %q // expected: %q", i, fsys, test.path, got.String(), expected)
		}
	}
}

func (fstests *spectests) testAbsRoot(t *testing.T) {
	if fstests.conf.Unsupported.Abs {
		t.Skip()
		return
	}
	if !fstests.conf.IsLocal {
		fstests.testAbsRootRemote(t)
		return
	}
	//fmt.Println("testAbs did run")
	prepareFS, base := spec.PrepareTest(fstests.conf, "testAbsRoot")

	tests := []struct {
		path     string
		expected string
	}{
		{"/", prepareFS.Base.String()},
		{"", prepareFS.Base.String()},
		{".", prepareFS.Base.String()},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, fstests.conf)
		fsys := fstests.fn(prepareFS, base)

		got := fsys.Abs(path.Relative(test.path))

		expected := test.expected
		if got.String() != expected {
			t.Errorf("[%v] %T.Abs(%q) = %q // expected: %q", i, fsys, test.path, got.String(), expected)
		}
	}
}
