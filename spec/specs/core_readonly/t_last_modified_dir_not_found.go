package core_readonly

import (
	"errors"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testLastModDirNotFound(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	if fstest.conf.Unsupported.ModTimeDir {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testLastModDirNotFound")

	tests := []struct {
		file string
		data string
		dir  string
	}{
		{fstest.conf.PathPrefix + "testdir_b/f", "content", fstest.conf.PathPrefix + "testdir_b/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f", "contentXYZ", fstest.conf.PathPrefix + "testdir_b/g/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		dirquery := "x" + test.dir
		_, err = fsys.ModTime(path.Relative(dirquery))

		if err == nil {
			t.Errorf("[%v] modtime of %q should return error, but did not", i, dirquery)
			continue
		}

		if !errors.Is(err, fs.ErrNotFound) {
			t.Errorf("[%v] %T.ModTime(%q) returned error %v // expected %v", i, fsys, dirquery, err, fs.ErrNotFound.Params(dirquery))
		}
	}

}
