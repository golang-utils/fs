package core_readonly

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testSizeFile(t *testing.T) {
	if fstest.conf.Unsupported.ReadingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testSizeFile")

	tests := []struct {
		file string
		data string
	}{
		{fstest.conf.PathPrefix + "f45654", "content"},
		{fstest.conf.PathPrefix + "testdir_b/g/fff", "contentXYZ"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)
		err := prepareFS.Write(path.Relative(test.file), fs.ReadCloser(strings.NewReader(test.data)), true)

		if err != nil {
			t.Fatalf("could not prepare file system: %v\n", err)
		}

		fsys := fstest.fn(prepareFS, base)

		expected := len(test.data)

		if !fsys.Exists(path.Relative(test.file)) {
			t.Errorf("[%v] %q is not created", i, test.file)
			continue
		}

		bt, err := fs.ReadFile(fsys, path.Relative(test.file))

		if err != nil {
			t.Errorf("[%v] can't read file %q: %s", i, test.file, err.Error())
			continue
		}

		if test.data != string(bt) {
			t.Errorf("[%v] content of file %q is: %q // expected %q", i, test.file, string(bt), test.data)
			continue
		}

		if fstest.conf.Unsupported.FileSize {
			expected = -2
		}

		got := fsys.Size(path.Relative(test.file))

		if got != int64(expected) {
			t.Errorf("[%v] %T.Size(%q) returned %v // expected %v", i, fsys, test.data, got, expected)
		}

	}

}
