package core_readonly

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (ro *spectests) testExistsDir(t *testing.T) {
	prepareFS, base := spec.PrepareTest(ro.conf, "testExistsDir")

	tests := []struct {
		descr    string
		writing  []string
		expected []string
	}{
		{"A",
			[]string{ro.conf.PathPrefix + "m/b/f/", ro.conf.PathPrefix + "m/b/g/", ro.conf.PathPrefix + "m/b/h/"},
			[]string{ro.conf.PathPrefix + "m/", ro.conf.PathPrefix + "m/b/", ro.conf.PathPrefix + "m/b/g/", ro.conf.PathPrefix + "m/b/h/", ro.conf.PathPrefix + "m/b/f/"},
		},
	}

	for i, test := range tests {
		spec.ClearFS(prepareFS, base, ro.conf)

		for _, dirwrite := range test.writing {
			dir := path.Relative(dirwrite)
			err := fs.MkDirAll(prepareFS, dir)
			if err != nil {
				t.Errorf("[%v] %s: could not create dir in prepare fs %q: %v", i, test.descr, dirwrite, err)
				continue
			}

			if !prepareFS.Exists(dir) {
				if err != nil {
					t.Errorf("[%v] %s: could not create dir in prepare fs %q: %v", i, test.descr, dirwrite, err)
					continue
				}
			}
		}

		fsys := ro.fn(prepareFS, base)

		for _, expDir := range test.expected {
			if !fsys.Exists(path.Relative(expDir)) {
				t.Errorf("[%v] %s: dir %q not there", i, test.descr, expDir)
			}
		}
	}
}
