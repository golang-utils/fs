package core_readonly

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

type spectests struct {
	conf spec.Config
	fn   func(prefillFS fs.ReadOnly, base path.Absolute) fs.ReadOnly
}

// DONE

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ReadOnly) *spectest.Spec {
	spec := spectest.NewSpec("ReadOnly", "test spec for the ReadOnly interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ReadOnly {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}

	rd := spectest.NewSpec("Reader", `
	Reader returns an io.ReadCloser that reads in the whole file or in case of a directory 
	the relative paths related to the directory path, separated by a unix linefeed (\n).
	The io.ReadCloser will be closed after reading all.
	An error is only returned, if p does not exist.
	`)

	rd.AddTest(`read file`, tests.testReaderFile)
	rd.AddTest(`read non existing file`, tests.testReaderNonExistingFile)
	rd.AddTest(`read non existing dir`, tests.testReaderNonExistingDir)
	rd.AddTest(`read directory with dirs`, tests.testReadDirWithDirs)
	rd.AddTest(`read directory with files`, tests.testReadDirWithFiles)
	rd.AddTest(`read root directory`, tests.testReadRootDir)

	spec.AddSubSpec(rd)

	exists := spectest.NewSpec("Exists", `
		Exists does return, if a file or directory with the given path exists.`)

	exists.AddTest(`file exists`, tests.testExistsFile)
	exists.AddTest(`dir exists`, tests.testExistsDir)
	exists.AddTest(`file not found`, tests.testExistsFileNotFound)
	exists.AddTest(`dir not found`, tests.testExistsDirNotFound)

	spec.AddSubSpec(exists)

	modTime := spectest.NewSpec("ModTime", `
		ModTime return the time of the last modification of p.
		It returns an error, if p does not exist.
		`)

	modTime.AddTest("last modification dir not found", tests.testLastModDirNotFound)
	modTime.AddTest("last modification file not found", tests.testLastModFileNotFound)
	modTime.AddTest("last modification file", tests.testLastModFile)
	modTime.AddTest("last modification dir", tests.testLastModDir)
	modTime.AddTest("last modification root dir", tests.testLastModRootDir)

	spec.AddSubSpec(modTime)

	abs := spectest.NewSpec("Abs", `
		   	Abs converts the given relative path to an absolute path, based on the base of the filesystem.
		   `)

	abs.AddTest("root to abs", tests.testAbsRoot)
	abs.AddTest("abs file", tests.testAbsFile)
	abs.AddTest("abs dir", tests.testAbsDir)

	spec.AddSubSpec(abs)

	size := spectest.NewSpec("Size", `
	   	Size returns the size of the given p in bytes
	   `)

	size.AddTest("dir not found", tests.testSizeDirNotFound)
	size.AddTest("file not found", tests.testSizeFileNotFound)
	size.AddTest("dir", tests.testSizeDir)
	size.AddTest("file", tests.testSizeFile)

	spec.AddSubSpec(size)

	return spec
}
