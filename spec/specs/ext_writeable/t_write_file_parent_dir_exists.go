package ext_writeable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteFileParentDirExists(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteFileParentDirExists")

	tests := []struct {
		file string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/g"},
		{fstest.conf.PathPrefix + "testdir_b/d"},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		rel := path.Relative(test.file)

		err := fs.MkDirAll(prepareFS, rel.Dir())

		if err != nil {
			t.Errorf("[%v] could not prepare dir %q: %v", i, rel.Dir().String(), err)
			continue
		}

		fsys := fstest.fn(prepareFS, base)

		err = fs.MkDirAll(fsys, rel.Dir())

		if err != nil {
			t.Errorf("[%v] could not create dir %q: %v", i, rel.Dir().String(), err)
			continue
		}

		err = fs.WriteFile(fsys, rel, []byte("content"), false)

		if err != nil {
			t.Errorf("[%v] could not create file %q: %v", i, test.file, err)
			continue
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] file %q not created", i, test.file)
		}

		bt, err := fs.ReadFile(fsys, rel)

		if err != nil {
			t.Errorf("[%v] could not read file %q: %v", i, test.file, err)
			continue
		}

		if string(bt) != "content" {
			t.Errorf("[%v] content of file %q does not match", i, test.file)
		}

	}

}
