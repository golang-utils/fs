package ext_writeable

import (
	"errors"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteDirParentDirNotExists(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteDirParentDirNotExists")

	tests := []struct {
		dir string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/"},
		{fstest.conf.PathPrefix + "testdir/b/"},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		rel := path.Relative(test.dir)

		fsys := fstest.fn(prepareFS, base)

		err := fs.MkDir(fsys, rel)

		if err == nil {
			t.Errorf("[%v] could create dir %q, but should throw error", i, test.dir)
			continue
		}

		if fsys.Exists(rel) {
			t.Errorf("[%v] dir %q created, but should not", i, test.dir)
			continue
		}

		if !errors.Is(err, fs.ErrNotFound) {
			t.Errorf("[%v] while trying to create dir %q got error: %v // expected error %v", i, test.dir, err, fs.ErrNotFound.Params(rel.Dir().String()))
		}

	}

}
