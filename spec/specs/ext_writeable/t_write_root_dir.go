package ext_writeable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteRootDir(t *testing.T) {
	if fstest.conf.Unsupported.WriteRootDir {
		t.Skip()
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteRootDir")

	tests := []struct {
		dir string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + ""},
		//{fstest.conf.PathPrefix + "testdir_b/"},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		rel := path.Relative(test.dir)

		fsys := fstest.fn(prepareFS, base)

		if !fsys.Exists(rel) {
			t.Errorf("[%v] root dir %q is not there", i, test.dir)
		}

		err := fs.MkDir(fsys, rel)

		if err != nil {
			t.Errorf("[%v] should not return an error but just ignore, trying to create the root dir %q, but got error %v", i, test.dir, err)
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] root dir %q is not there", i, test.dir)
		}

	}

}
