package ext_writeable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteFileRecursive(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteFileRecursive")

	tests := []struct {
		file        string
		shouldExist []string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/g", []string{fstest.conf.PathPrefix + "testdir_b/", fstest.conf.PathPrefix + "testdir_b/g/"}},
		{fstest.conf.PathPrefix + "testdir_b/d", []string{}},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		rel := path.Relative(test.file)

		err := fs.WriteFile(fsys, rel, []byte("content"), true)

		if err != nil {
			t.Errorf("[%v] could not create file %q: %v", i, test.file, err)
			continue
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] file %q not created", i, test.file)
		}

		bt, err := fs.ReadFile(fsys, rel)

		if err != nil {
			t.Errorf("[%v] could not read file %q: %v", i, test.file, err)
			continue
		}

		if string(bt) != "content" {
			t.Errorf("[%v] content of file %q does not match", i, test.file)
		}

		for _, d := range test.shouldExist {
			dd := path.Relative(d)

			if !fsys.Exists(dd) {
				t.Errorf("[%v] dir %q not created", i, dd)
			}
		}

	}

}
