package ext_writeable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteDirParentDirExists(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteDirParentDirExists")

	tests := []struct {
		dir string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/"},
		{fstest.conf.PathPrefix + "testdir_b/"},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		rel := path.Relative(test.dir)

		err := prepareFS.Write(rel.Dir(), nil, true)

		if err != nil {
			t.Errorf("[%v] could not create parent dir %q: %v", i, rel.Dir().String(), err)
			continue
		}

		fsys := fstest.fn(prepareFS, base)

		err = fs.MkDir(fsys, rel)

		if err != nil {
			t.Errorf("[%v] could not create dir %q: %v", i, test.dir, err)
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] dir %q not created", i, test.dir)
		}

	}

}
