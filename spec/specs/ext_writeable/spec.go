package ext_writeable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

// DONE

type spectests struct {
	conf spec.Config
	fn   func(prepareFS fs.ReadOnly, base path.Absolute) fs.ExtWriteable
}

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prepareFS fs.ReadOnly, base path.Absolute) fs.ExtWriteable) *spectest.Spec {
	spec := spectest.NewSpec("ExtWriteable", "test spec for the ExtWriteable interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ExtWriteable {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}

	write := spectest.NewSpec("Write", "Write writes either a file or a folder to the given path with default permissions.")

	write.AddTest("write file parent dir exists", tests.testWriteFileParentDirExists)
	write.AddTest("write file parent dir not exists", tests.testWriteFileParentDirNotExists)
	write.AddTest("write file recursive", tests.testWriteFileRecursive)
	write.AddTest("write file exists", tests.testWriteFileExists)
	write.AddTest("write file close", tests.testWriteFileClose)

	write.AddTest("write dir recursive", tests.testWriteDirRecursive)
	write.AddTest("write dir parent dir exists", tests.testWriteDirParentDirExists)
	write.AddTest("write dir exists", tests.testWriteDirExists)
	write.AddTest("write dir parent dir not exists", tests.testWriteDirParentDirNotExists)
	write.AddTest("write root dir - should ignore", tests.testWriteRootDir)

	spec.AddSubSpec(write)

	return spec
}
