package ext_writeable

import (
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteDirRecursive(t *testing.T) {
	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteDirRecursive")

	tests := []struct {
		dir         string
		shouldExist []string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/", []string{fstest.conf.PathPrefix + "testdir_b/", fstest.conf.PathPrefix + "testdir_b/g/"}},
		{fstest.conf.PathPrefix + "testdir_b/", []string{}},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		rel := path.Relative(test.dir)

		err := fs.MkDirAll(fsys, rel)

		if err != nil {
			t.Fatalf("[%v] could not create dir %q: %v", i, test.dir, err)
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] dir %q not created", i, test.dir)
		}

		for _, d := range test.shouldExist {
			dd := path.Relative(d)

			if !fsys.Exists(dd) {
				t.Errorf("[%v] dir %q not created", i, dd)
			}
		}
	}

}
