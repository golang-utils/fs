package ext_writeable

import (
	"errors"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testWriteFileParentDirNotExists(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteFileParentDirNotExists")

	tests := []struct {
		file string
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/g"},
		{fstest.conf.PathPrefix + "testdir_b/d"},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		rel := path.Relative(test.file)

		fsys := fstest.fn(prepareFS, base)

		err := fs.WriteFile(fsys, rel, []byte("content"), false)

		if err == nil {
			t.Errorf("[%v] could create file %q, but should throw error", i, test.file)
			continue
		}

		if fsys.Exists(rel) {
			t.Errorf("[%v] file %q created, but should not", i, test.file)
			continue
		}

		if !errors.Is(err, fs.ErrNotFound) {
			t.Errorf("[%v] while trying to create file %q got error: %v // expected error %v", i, test.file, err, fs.ErrNotFound.Params(rel.Dir().String()))
		}
	}

}
