package ext_writeable

import (
	"io"
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

type closeTester struct {
	io.Reader
	wasClosed bool
}

func (c *closeTester) Close() error {
	c.wasClosed = true
	return nil
}

func (fstest *spectests) testWriteFileClose(t *testing.T) {
	if fstest.conf.Unsupported.WritingFile {
		t.Skip()
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testWriteFileClose")

	tests := []struct {
		file      string
		recursive bool
	}{
		//{false, path.MustLocal("C:"), "testdir_c/d/e/"},
		//{false, path.MustLocal("C:"), "testdir_a/b/c/"},
		{fstest.conf.PathPrefix + "testdir_b/g/f/tertg", true},
		{fstest.conf.PathPrefix + "d", true},
		//	{true, subdir, "testdir_b/g/f/"},
		//	{true, subdir, "testdir b/a b/f/"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/"},
		//{false, path.MustLocal("C:/fh h/"), "testdir c/g a/f/"},
	}

	for i, test := range tests {

		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		rel := path.Relative(test.file)

		ct := &closeTester{strings.NewReader("content"), true}
		err := fsys.Write(rel, ct, test.recursive)

		if err != nil {
			t.Errorf("[%v] could not create file %q: %v", i, test.file, err)
			continue
		}

		if !fsys.Exists(rel) {
			t.Errorf("[%v] file %q not created", i, test.file)
		}

		bt, err := fs.ReadFile(fsys, rel)

		if err != nil {
			t.Errorf("[%v] could not read file %q: %v", i, test.file, err)
			continue
		}

		if string(bt) != "content" {
			t.Errorf("[%v] content of file %q does not match", i, test.file)
			continue
		}

		if !ct.wasClosed {
			t.Errorf("[%v] reader of %q was not closed", i, test.file)
		}

	}

}
