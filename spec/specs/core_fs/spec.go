package core_fs

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_readonly"
	"gitlab.com/golang-utils/fs/spec/specs/ext_deleteable"
	"gitlab.com/golang-utils/fs/spec/specs/ext_writeable"
	"gitlab.com/golang-utils/spectest"
)

// DONE

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.FS) *spectest.Spec {
	spec := spectest.NewSpec("FS", "test spec for the FS interface")

	fnRD := func(prepare fs.ReadOnly, p path.Absolute) fs.ReadOnly {
		fsrw := fn(prepare, p)

		if fsrw == nil {
			panic("not fs returned")
		}

		return fsrw
	}

	spec.AddSubSpec(core_readonly.Spec(c, fnRD))

	fnWr := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtWriteable {
		return fn(prepare, p)
	}
	spec.AddSubSpec(ext_writeable.Spec(c, fnWr))

	fndl := func(prepare fs.ReadOnly, p path.Absolute) fs.ExtDeleteable {
		fsrw := fn(prepare, p)

		if fsrw == nil {
			panic("not fs returned")
		}

		return fsrw
	}
	spec.AddSubSpec(ext_deleteable.Spec(c, fndl))

	return spec
}
