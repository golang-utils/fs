package ext_url

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base *path.Remote) fs.ExtURL) *spectest.Spec {
	spec := spectest.NewSpec("ExtURL", "test spec for the ExtURL interface")

	/*
		host := spectest.NewSpec("HostName", "HostName returns the hostname of the url")
		host.AddTest("empty", func(t *testing.T) {

		})
		host.AddTest("ip address", func(t *testing.T) {

		})
		host.AddTest("domain", func(t *testing.T) {

		})

		spec.AddSubSpec(host)
	*/

	/*
		port := spectest.NewSpec("Port", "Port returns the port number of the url")
		port.AddTest("empty", func(t *testing.T) {

		})
		spec.AddSubSpec(port)
	*/

	/*
		username := spectest.NewSpec("UserName", "UserName returns the user name of the url")
		username.AddTest("empty", func(t *testing.T) {

		})
		spec.AddSubSpec(username)
	*/

	/*
		password := spectest.NewSpec("Password", "Password returns the password of the url")
		password.AddTest("empty", func(t *testing.T) {

		})
		spec.AddSubSpec(password)
	*/

	/*
		scheme := spectest.NewSpec("Scheme", "Scheme returns the scheme of the url")
		scheme.AddTest("empty", func(t *testing.T) {

		})
		spec.AddSubSpec(scheme)
	*/
	return spec
}
