package core_remote

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_fs"
	"gitlab.com/golang-utils/fs/spec/specs/ext_meta"
	"gitlab.com/golang-utils/fs/spec/specs/ext_url"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base *path.Remote) fs.Remote) *spectest.Spec {
	c.IsLocal = false
	spec := spectest.NewSpec("Remote", "test spec for the Remote interface")

	fnFS := func(prefill fs.ReadOnly, p path.Absolute) fs.FS {
		rem, ok := p.(*path.Remote)
		if ok {
			return fn(prefill, rem)
		}

		//fmt.Printf("not a remote: %T\n", p)
		//return nil
		panic("not a remote")
	}
	spec.AddSubSpec(core_fs.Spec(c, fnFS))

	fnMeta := func(prefill fs.ReadOnly, p *path.Remote) fs.ExtMeta {
		return fn(prefill, p)
	}
	spec.AddSubSpec(ext_meta.Spec(c, fnMeta))

	fnURL := func(prefill fs.ReadOnly, p *path.Remote) fs.ExtURL {
		return fn(prefill, p)
	}
	spec.AddSubSpec(ext_url.Spec(c, fnURL))

	return spec
}
