package ext_moveable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtMoveable) *spectest.Spec {
	spec := spectest.NewSpec("ExtMoveable", "test spec for the ExtMoveable interface")

	/*
		mountP := spectest.NewSpec("Drive", "Drive returns the drive letter (windows), UNC (windows) or mountpoint (unix) of the given relative path")
		mountP.AddTest("drive letter windows", func(t *testing.T) {})
		mountP.AddTest("unc windows", func(t *testing.T) {})
		mountP.AddTest("mountpoint unix", func(t *testing.T) {})
		mountP.AddTest("error", func(t *testing.T) {})
		spec.AddSubSpec(mountP)
	*/
	/*
		move := spectest.NewSpec("Move", "move is an fs optimization for moving files and directories")
		move.AddTest("move src not found", func(t *testing.T) {})
		move.AddTest("move trgt exists", func(t *testing.T) {})
		move.AddTest("move different mountpoints", func(t *testing.T) {})
		move.AddTest("move file", func(t *testing.T) {})
		move.AddTest("move dir", func(t *testing.T) {})

		spec.AddSubSpec(move)
	*/
	return spec
}
