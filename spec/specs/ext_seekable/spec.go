package ext_seekable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

type spectests struct {
	conf spec.Config
	fn   func(prepare fs.ReadOnly, base path.Absolute) fs.ExtReadSeekable
}

type trackFS struct {
	currentFS fs.ReadOnly
}

func Spec(c spec.Config, fn func(prepare fs.ReadOnly, base path.Absolute) fs.ExtReadSeekable) *spectest.Spec {
	s := spectest.NewSpec("ExtSeekable", "test spec for the ExtSeekable interface")

	var track = &trackFS{}

	wrapCloser := func(pre fs.ReadOnly, ba path.Absolute) fs.ExtReadSeekable {
		if track.currentFS != nil {
			if cl, ok := track.currentFS.(fs.ExtClose); ok {
				cl.Close()
			}
		}
		fsys := fn(pre, ba)
		track.currentFS = fsys
		return fsys
	}

	tests := &spectests{c, wrapCloser}
	_ = tests

	rs := spectest.NewSpec("ReadSeeker", "// ReadSeeker reads and seeks.")

	rs.AddTest("read seeker", tests.testReadSeeker)
	/*

		del.AddTest("delete directory recursively", tests.testRmDirAll)
		del.AddTest("delete empty directory", tests.testRmDirEmpty)
		del.AddTest("delete directory with files", tests.testRmDirWithFiles)
		del.AddTest("delete directory with dirs", tests.testRmDirWithDirs)
		del.AddTest("delete non existing file", tests.testRmFileNotFound)
		del.AddTest("delete non existing dir", tests.testRmDirNotFound)
		del.AddTest("delete not root dir", tests.testRmDirRootDir)
	*/

	s.AddSubSpec(rs)
	return s
}
