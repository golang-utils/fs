package core_testfs

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/fs/spec/specs/core_local"
	"gitlab.com/golang-utils/fs/spec/specs/core_remote"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.TestFS) *spectest.Spec {
	spec := spectest.NewSpec("TestFS", "test spec for the TestFS interface")

	fnModloc := func(prefill fs.ReadOnly, p path.Local) fs.Local {
		return fn(prefill, p)
	}

	fnModrem := func(prefill fs.ReadOnly, p *path.Remote) fs.Remote {
		return fn(prefill, p)
	}

	spec.AddSubSpec(core_local.Spec(c, fnModloc))
	spec.AddSubSpec(core_remote.Spec(c, fnModrem))

	return spec
}
