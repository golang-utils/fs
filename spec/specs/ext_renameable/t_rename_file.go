package ext_renameable

import (
	"strings"
	"testing"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
)

func (fstest *spectests) testRenameFile(t *testing.T) {
	if fstest.conf.Unsupported.RenameFile {
		t.Skip()
		return
	}

	prepareFS, base := spec.PrepareTest(fstest.conf, "testRenameFile")

	//panic("hiho")
	//wdfs.Resolve(path.Relative("tests/"))

	//dirfs := mustNewDirFS(path.Relative("tests/"))

	//path.MustWD()

	tests := []struct {
		forlocal bool
		fsroot   string
		file     string
		newName  string
		expected string
	}{
		//	{"Memory", Memory(), "testdir_a/b/c", "testdir_a/b/cd"},
		//{"Map", Map(), "testdir_c/d/e", "ef", "testdir_c/d/ef"},
		//{false, path.MustLocal("i:/"), "testdir_c/d/e", "ef", "testdir_c/d/ef"},
		//{"Dir-neu", Dir(path.MustNew("tests")), "f", "gf", "gf"},
		{true, fstest.conf.PathPrefix + "tests/", fstest.conf.PathPrefix + "fsdfs", "fsdfsgf", fstest.conf.PathPrefix + "fsdfsgf"},
		//{"Dir-neu", Dir(path.MustNew("tests")), "testdir_b/g/f", "gf", "testdir_b/g/gf"},
		{true, fstest.conf.PathPrefix + "tests/", fstest.conf.PathPrefix + "testdir_sdfsdfb/sdffg/fdf", "ssssfgf", fstest.conf.PathPrefix + "testdir_sdfsdfb/sdffg/ssssfgf"},
		//{false, path.MustLocal("C:"), "testdir_g/g/f/", "gh", "testdir_g/g/gh/"},
		//{"Dir-schon vorhanden", Dir("tests"), "testdir_b/g/f"},
	}

	for i, test := range tests {

		if fstest.conf.IsLocal != test.forlocal {
			continue
		}

		spec.ClearFS(prepareFS, base, fstest.conf)

		fsys := fstest.fn(prepareFS, base)

		wr, ok := fsys.(fs.ExtWriteable)

		if !ok {
			t.Fatalf("could not test %T because it does not implement the fs.ExtWriteable interface", fsys)
			return
		}

		file := path.Relative(test.file)

		if !fsys.Exists(file.Dir()) {
			//err := test.fs.Mkdir(file.Dir(), true, 0755)
			err := fs.MkDirAll(wr, file.Dir())

			//fmt.Printf("creating %q\n", path.Dir(file).Relative())
			if err != nil {
				t.Fatalf("[%v] %T(%q): could not create dir %q: %v", i, fsys, base, file.Dir(), err)
			}
		}

		rd := strings.NewReader("huhu")
		err := wr.Write(file, fs.ReadCloser(rd), true)

		if err != nil {
			t.Fatalf("[%v] %T(%q): could not write file %q: %T %v", i, fsys, base, test.file, err, err)
		}

		if !fsys.Exists(file) {
			t.Errorf("[%v] %T(%q): file %q not created", i, fsys, base, test.file)
		}

		/*
			if path.IsDir(file) {
				t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.file)
			}
		*/

		file = path.Relative(test.file)
		err = fsys.Rename(file, test.newName)

		if err != nil {
			/*
				e := err.(*os.LinkError)
				fmt.Println("Op: ", e.Op)
				fmt.Println("Old: ", e.Old)
				fmt.Println("New: ", e.New)
				fmt.Println("Err: ", e.Err)
				oserr := e.Err.(syscall.Errno)
				fmt.Printf("oserr: %v\n", oserr.Temporary())
			*/
			t.Fatalf("[%v] %T(%q): could not rename %q to %q: %v", i, fsys, test.fsroot, test.file, test.newName, err)
		}

		if !fsys.Exists(path.Relative(test.expected)) {
			t.Errorf("[%v] %T(%q): file %q not there", i, fsys, test.fsroot, test.expected)
		}

		/*
			if path.IsDir(path.Relative(test.expected)) {
				t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.expected)
			}
		*/
	}

}
