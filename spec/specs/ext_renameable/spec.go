package ext_renameable

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

type spectests struct {
	conf spec.Config
	fn   func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtRenameable
}

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtRenameable) *spectest.Spec {
	spec := spectest.NewSpec("ExtRenameable", "test spec for the ExtRenameable interface")

	tests := &spectests{c, fn}

	ren := spectest.NewSpec("Rename", "Rename renames either a file or a folder.")
	ren.AddTest("rename file", tests.testRenameFile)

	// ren.AddTest("rename dir", func(t *testing.T) {})
	spec.AddSubSpec(ren)
	return spec
}
