package ext_meta

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base *path.Remote) fs.ExtMeta) *spectest.Spec {
	spec := spectest.NewSpec("ExtMeta", "test spec for the ExtMeta interface")

	/*
		write := spectest.NewSpec("WriteWithMeta", "WriteWithMeta behaves like Writeable.Write and only differs that the given meta data is used within the creation.")


		write.AddTest("write file", func(t *testing.T) {

		})

		write.AddTest("overwrite file", func(t *testing.T) {

		})

		write.AddTest("write file recursive", func(t *testing.T) {

		})

		write.AddTest("write dir", func(t *testing.T) {

		})

		write.AddTest("overwrite dir", func(t *testing.T) {

		})

		write.AddTest("write dir recursive", func(t *testing.T) {

		})

		spec.AddSubSpec(write)
	*/

	/*
		get := spectest.NewSpec("GetMeta", "GetMeta returns the meta data of a file or a folder")
		get.AddTest("get meta file", func(t *testing.T) {})
		get.AddTest("get meta dir", func(t *testing.T) {})
		spec.AddSubSpec(get)
	*/

	/*
		set := spectest.NewSpec("SetMeta", "SetMeta sets the meta data of a file or a folder")
		set.AddTest("set meta file", func(t *testing.T) {})
		set.AddTest("set meta dir", func(t *testing.T) {})
		spec.AddSubSpec(set)
	*/
	return spec
}
