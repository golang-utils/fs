package ext_spacereporter

import (
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
	"gitlab.com/golang-utils/fs/spec"
	"gitlab.com/golang-utils/spectest"
)

func Spec(c spec.Config, fn func(prefillFS fs.ReadOnly, base path.Absolute) fs.ExtSpaceReporter) *spectest.Spec {
	spec := spectest.NewSpec("ExtSpaceReporter", "test spec for the ExtSpaceReporter interface")

	/*
		sr := spectest.NewSpec("FreeSpace", "FreeSpace returns how many bytes of space are left on the mountpoint of the given path")
		sr.AddTest("has space", func(t *testing.T) {})
		sr.AddTest("no space", func(t *testing.T) {})
		sr.AddTest("error", func(t *testing.T) {})
		spec.AddSubSpec(sr)
	*/
	return spec
}
