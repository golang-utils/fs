package spec_test

import (
	"io"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"
	"time"

	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/filesystems/dirfs"
	"gitlab.com/golang-utils/fs/filesystems/mapfs"

	//	"gitlab.com/golang-utils/fs/filesystems/memfs"
	//	"gitlab.com/golang-utils/fs/filesystems/treefs"
	"gitlab.com/golang-utils/fs/path"
)

/*
func mustNewtreefsDir(absPath string) *treefs.DirFS {
	loc, err := path.ParseLocal(absPath)

	if err != nil {
		panic(err.Error())
	}

	fs, err := treefs.NewDir(loc)

	if err != nil {
		panic(err.Error())
	}

	return fs
}
*/

func mustNewMapFS(absPath string) *mapfs.FS {
	loc, err := path.ParseLocal(absPath)

	if err != nil {
		panic(err.Error())
	}

	fs, err := mapfs.New(loc)

	if err != nil {
		panic(err.Error())
	}

	return fs
}

func mustNewDirFS(p path.Path) *dirfs.FS {
	absPath := p.String()

	if !path.IsAbs(p) {
		var err error
		absPath, err = filepath.Abs(p.Relative().String())

		if err != nil {
			panic(err.Error())
		}

		absPath += "/"
	}

	//	fmt.Printf("#mustNewDirFS absPath: %s\n", absPath)

	/*
		if path.IsDir(path.Relative(relPath)) {
			absPath += "/"
		}
	*/

	loc, err := path.ParseLocal(absPath)

	if err != nil {
		panic(err.Error())
	}

	fs, err := dirfs.New(loc)

	if err != nil {
		panic(err.Error())
	}

	return fs
}

/*
func mustNewMemoryFS(absPath string) *memfs.FS {
	loc, err := path.ParseLocal(absPath)

	if err != nil {
		panic(err.Error())
	}

	fs, err := memfs.New(loc)

	if err != nil {
		panic(err.Error())
	}

	return fs
}
*/

/*
func TestValidFSPath(t *testing.T) {
	tests := []struct {
		input    string
		expected bool
	}{
		{"a.txt", true},
		{"b/a", true},
		{"b\\a", true},
		{"b/a", true},
		{"./b/a", true},
		{"b/a/", true},
		{"c/d/../../b/a", true},

		//{"..\\b\\a", false},
		//{"..\\..\\b\\a", false},
		{"c\\..\\..\\b\\a", false},
		{"c\\d\\..\\..\\b\\a", true},

		{"c:\\b\\a", false},
		{"/b/a", false},
		{"\\\\b\\a", false},
	}

	for i, test := range tests {

		p := path.MustNew(test.input)

		if got, want := validFSPath(p), test.expected; got != want {
			t.Errorf("[%v]  validFSPath(%q) = %v // expected %v", i, test.input, got, want)
		}
	}
}
*/

type fullWriteable interface {
	fs.ReadOnly
	fs.ExtWriteable
}

/*
func mustAbs(fsys FileSystem, p path.Path) string {
	s, err := fsys.Abs(p)
	if err != nil {
		panic(err.Error())
	}

	return s
}
*/

func TestReadDirAbs(t *testing.T) {
	wd, err := os.Getwd()

	if err != nil {
		t.Fatalf("could not get working dir")
	}

	wd, err = filepath.Abs(wd)

	if err != nil {
		t.Fatalf("could not get abs path of working dir")
	}

	testDir := filepath.Join(wd, "tests")

	os.RemoveAll(testDir)

	os.Mkdir(testDir, 0755)

	wdfs, err := dirfs.New(path.MustWD())

	if err != nil {
		t.Fatalf("could not get mountpoint: %v", err)
	}

	mp, err := wdfs.Drive(path.Relative(""))

	//mp, err := path.MountPoint(path.MustNew(wd))

	if fs.OnWindows() {
		if path.DriveLetter(mp) != "i:" {
			t.Fatalf("mountpoint is not i:, but %q", path.DriveLetter(mp))
		}
	}

	testDirPath := path.MustLocal(testDir + "/")

	var rootFS *dirfs.FS

	if fs.OnWindows() {
		rootFS = mustNewDirFS(path.Local{"i:/", ""})
	} else {
		rootFS = mustNewDirFS(path.Local{"/", ""})
	}
	//fmt.Printf("testDir: %s\ntestDirPath: %s\nrootFS base: %s\n", testDir, testDirPath.String(), rootFS.base)

	tests := []struct {
		descr      string
		filesWrite []string
		dirRead    string
		expected   []string
	}{
		{"Dir-neu", []string{"a/b/f", "a/b/g", "a/b/h"}, "a/b/", []string{"f", "g", "h"}},
		{"Dir-neu", []string{"b/b/f", "b/b/g", "b/b/h"}, "b/", []string{"b"}},
		{"Dir-neu", []string{"c/b/f", "c/b/c/g", "c/b/h"}, "c/b/", []string{"c", "f", "h"}},
	}

	for i, test := range tests {
		//os.RemoveAll(testDir)
		//os.Mkdir(testDir, 0755)

		for _, filewrite := range test.filesWrite {
			file := testDirPath.Join(filewrite).Relative()
			dir := file.Dir()

			//	fmt.Printf("file: %s\ndir: %s\n", file, dir)

			if !rootFS.Exists(dir) {
				err := fs.MkDirAll(rootFS, dir)

				if err != nil {
					t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, dir, err)
				}
			}

			rd := strings.NewReader("content")
			err := rootFS.Write(file, fs.ReadCloser(rd), true)

			if err != nil {
				t.Fatalf("[%v] %s: could not write file %q: %T %v", i, test.descr, filewrite, err, err)
			}

			if !rootFS.Exists(file) {
				t.Errorf("[%v] %s: file %q not created", i, test.descr, filewrite)
			}

			if path.IsDir(file) {
				t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, filewrite)
			}
		}

	}

	for i, test := range tests {
		dir := filepath.Join(testDir, test.dirRead)

		entries, err := os.ReadDir(dir)

		if err != nil {
			t.Errorf("error while reading dir: %q: %v", dir, err)
			continue
		}

		var results = make([]string, len(entries))

		for di, dd := range entries {
			results[di] = dd.Name()
		}

		if got, want := results, test.expected; !reflect.DeepEqual(got, want) {
			t.Errorf("[%v]  content of directory %q = %#v // expected %#v", i, test.dirRead, got, want)
		}
	}
}

func TestReadDir(t *testing.T) {

	os.RemoveAll("tests")
	os.Mkdir("tests", 0755)
	os.Mkdir("tests/a b", 0755)
	wdstr, err := os.Getwd()

	if err != nil {
		t.Fatalf("can't get absolute WD: %v", err)
	}

	_ = wdstr

	tests := []struct {
		descr      string
		fs         fullWriteable
		filesWrite []string
		dirRead    string
		expected   []string
	}{
		{"Map", mustNewMapFS("/map/"), []string{"m/b/f", "m/b/g", "m/b/h"}, "m/b/", []string{"f", "g", "h"}},
		//		{"Memory", mustNewMemoryFS("/memory/"), []string{"e/b/f", "e/b/g", "e/b/h", "e/z"}, "e/b/", []string{"f", "g", "h"}},
		{"Dir-neu", mustNewDirFS(path.Local{wdstr + "/", "tests/"}), []string{"a/b/f", "a/b/g", "a/b/h"}, "a/b/", []string{"f", "g", "h"}},
		{"Dir-schon vorhanden", mustNewDirFS(path.Relative("tests/")), []string{"c/b/f", "c/b/c/g", "c/b/h"}, "c/b/", []string{"c/", "f", "h"}},
		//		{"DirTree", mustNewtreefsDir("C:\\tests\\"), []string{"c/b/f/", "c/b/c/g/", "c/b/h/"}, "c/b/", []string{"c/", "f/", "h/"}},
		//		{"DirTree space", mustNewtreefsDir("C:\\tests\\a b\\"), []string{"c/b c/f/", "c/b c/c/g/", "c/b c/h/"}, "c/b c/", []string{"c/", "f/", "h/"}},
		//		{"DirTree2", mustNewtreefsDir("C:\\tests\\"), []string{}, "", []string{}},
		//		{"Dir-neu", Dir(path.MustNew("tests")), []string{"b/b/f", "b/b/g", "b/b/h"}, "b", []string{"b"}},
		//	{"Dir-neu", Dir(path.MustNew("tests")), []string{"c/b/f", "c/b/c/g", "c/b/h"}, "c/b", []string{"c", "f", "h"}},
		//{"Dir-schon vorhanden", Dir("tests"), "testdir_b/g/f"},
	}

	for i, test := range tests {

		//test.fs.Rm(path.MustNew("tests/a"), true)

		for _, filewrite := range test.filesWrite {
			file := path.Relative(filewrite)
			dir := file.Dir()
			if !test.fs.Exists(dir) {
				err := fs.MkDirAll(test.fs, dir)
				//err := test.fs.Mkdir(path.Dir(file), true, 0755)

				if err != nil {
					t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, dir, err)
				}
			}

			rd := strings.NewReader("content")
			err := test.fs.Write(file, fs.ReadCloser(rd), true)

			if err != nil {
				t.Fatalf("[%v] %s: could not write file %q: %T %v", i, test.descr, filewrite, err, err)
			}

			if !test.fs.Exists(file) {
				t.Errorf("[%v] %s: file %q not created", i, test.descr, filewrite)
			}

			/*
				if path.IsDir(file) {
					t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, filewrite)
				}
			*/
		}

	}

	for i, test := range tests {
		dir := path.Relative(test.dirRead)

		if !test.fs.Exists(dir) {
			t.Errorf("[%v] %s: dir %q not there", i, test.descr, test.dirRead)
		}

		if !path.IsDir(dir) {
			t.Errorf("[%v] %s: %q is not a dir", i, test.descr, test.dirRead)
		}

		results, err := fs.ReadDirNames(test.fs, dir)

		if err != nil {
			t.Fatalf("[%v] %s: could not read dir %q: %v", i, test.descr, test.dirRead, err)
		}

		if got, want := results, test.expected; !reflect.DeepEqual(got, want) {
			t.Errorf("[%v] %s: content of directory %q = %#v // expected %#v", i, test.descr, test.dirRead, got, want)
		}
	}

}

func TestOpen(t *testing.T) {

	os.RemoveAll("tests")
	os.Mkdir("tests", 0755)

	tests := []struct {
		descr     string
		fs        fullWriteable
		fileWrite string
		data      string
		fileRead  string
	}{
		//		{"Memory", mustNewMemoryFS("/memory/"), "testdir_a/b/c", "content", "testdir_a/b/c"},
		{"Map", mustNewMapFS("/map/"), "testdir_c/d/e", "content", "testdir_c/d/e"},
		{"Dir-neu", mustNewDirFS(path.Relative("tests/")), "f", "content", "f"},
		{"Dir-neu", mustNewDirFS(path.Relative("tests/")), "testdir_b/g/f", "content", "testdir_b/g/f"},
		//{"Dir-schon vorhanden", Dir("tests"), "testdir_b/g/f"},
	}

	for i, test := range tests {
		file := path.Relative(test.fileWrite)

		if !test.fs.Exists(file.Dir()) {
			err := fs.MkDirAll(test.fs, file.Dir())
			if err != nil {
				t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, file.Dir().String(), err)
			}
		}

		rd := strings.NewReader(test.data)
		err := test.fs.Write(file, fs.ReadCloser(rd), false)

		if err != nil {
			t.Fatalf("[%v] %s: could not write file %q: %T %v", i, test.descr, test.fileWrite, err, err)
		}

		if !test.fs.Exists(file) {
			t.Errorf("[%v] %s: file %q not created", i, test.descr, test.fileWrite)
		}

		if path.IsDir(file) {
			t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.fileWrite)
		}
	}

	for i, test := range tests {
		file := path.Relative(test.fileRead)

		if !test.fs.Exists(file) {
			t.Errorf("[%v] %s: file %q not there", i, test.descr, test.fileRead)
		}

		if path.IsDir(file) {
			t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.fileRead)
		}

		f, err := test.fs.Reader(file)

		if err != nil {
			t.Fatalf("[%v] %s: could not open file %q: %v", i, test.descr, test.fileRead, err)
		}

		bt, err := io.ReadAll(f)
		f.Close()

		if err != nil {
			t.Fatalf("[%v] %s: could not read from file %q: %v", i, test.descr, test.fileRead, err)
		}

		if got, want := string(bt), test.data; got != want {
			t.Errorf("[%v]  content of %q = %q // expected %q", i, test.fileRead, got, want)
		}
	}

}

func TestWD(t *testing.T) {
	wd, err := os.Getwd()

	if err != nil {
		t.Fatalf("can't get wd: %v", err)
	}

	subdir := "tests"
	expected := strings.ToLower(filepath.ToSlash(filepath.Join(wd, subdir)) + "/")

	wdfs, err := dirfs.New(path.MustWD())

	if err != nil {
		t.Fatalf("can't get wd fs: %v", err)
	}

	got := wdfs.Abs(path.Relative(subdir + "/")).String()

	if got != expected {
		t.Errorf("WD().Abs(%q) is %q // expected %q", subdir+"/", got, expected)
	}

}

type fullRenameable interface {
	fs.FS
	fs.ExtRenameable
}

func TestRenameFile(t *testing.T) {

	os.RemoveAll("tests")
	os.Mkdir("tests", 0755)

	//wdfs.Resolve(path.Relative("tests/"))

	dirfs := mustNewDirFS(path.Relative("tests/"))

	tests := []struct {
		descr    string
		fs       fullRenameable
		file     string
		newName  string
		expected string
	}{
		//	{"Memory", Memory(), "testdir_a/b/c", "testdir_a/b/cd"},
		//{"Map", Map(), "testdir_c/d/e", "ef", "testdir_c/d/ef"},
		{"Map", mustNewMapFS("i:/"), "testdir_c/d/e", "ef", "testdir_c/d/ef"},
		//{"Dir-neu", Dir(path.MustNew("tests")), "f", "gf", "gf"},
		{"Dir-neu", dirfs, "fsdfs", "fsdfsgf", "fsdfsgf"},
		//{"Dir-neu", Dir(path.MustNew("tests")), "testdir_b/g/f", "gf", "testdir_b/g/gf"},
		{"Dir-neu", dirfs, "testdir_sdfsdfb/sdffg/fdf", "ssssfgf", "testdir_sdfsdfb/sdffg/ssssfgf"},
		//		{"DirTree", mustNewtreefsDir("C:"), "testdir_g/g/f/", "gh", "testdir_g/g/gh/"},
		//{"Dir-schon vorhanden", Dir("tests"), "testdir_b/g/f"},
	}

	for i, test := range tests {
		file := path.Relative(test.file)

		if !test.fs.Exists(file.Dir()) {
			//err := test.fs.Mkdir(file.Dir(), true, 0755)
			err := fs.MkDirAll(test.fs, file.Dir())

			//fmt.Printf("creating %q\n", path.Dir(file).Relative())
			if err != nil {
				t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, file.Dir(), err)
			}
		}

		rd := strings.NewReader("huhu")
		err := test.fs.Write(file, fs.ReadCloser(rd), false)

		if err != nil {
			t.Fatalf("[%v] %s: could not write file %q: %T %v", i, test.descr, test.file, err, err)
		}

		if !test.fs.Exists(file) {
			t.Errorf("[%v] %s: file %q not created", i, test.descr, test.file)
		}

		/*
			if path.IsDir(file) {
				t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.file)
			}
		*/
	}

	time.Sleep(1000 * time.Millisecond)

	for i, test := range tests {
		file := path.Relative(test.file)
		err := test.fs.Rename(file, test.newName)

		if err != nil {
			/*
				e := err.(*os.LinkError)
				fmt.Println("Op: ", e.Op)
				fmt.Println("Old: ", e.Old)
				fmt.Println("New: ", e.New)
				fmt.Println("Err: ", e.Err)
				oserr := e.Err.(syscall.Errno)
				fmt.Printf("oserr: %v\n", oserr.Temporary())
			*/
			t.Fatalf("[%v] %s: could not rename %q to %q: %v", i, test.descr, test.file, test.newName, err)
		}

		if !test.fs.Exists(path.Relative(test.expected)) {
			t.Errorf("[%v] %s: file %q not there", i, test.descr, test.expected)
		}

		/*
			if path.IsDir(path.Relative(test.expected)) {
				t.Errorf("[%v] %s: %q is a dir and not a file", i, test.descr, test.expected)
			}
		*/
	}

}

func TestMkdir(t *testing.T) {

	os.RemoveAll("tests")
	os.Mkdir("tests", 0755)

	tests := []struct {
		descr string
		fs    fullWriteable
		dir   string
	}{
		//	{"Memory", mustNewMemoryFS("/mem/"), "testdir_a/"},
		{"Map", mustNewMapFS("/map/"), "testdir_c/"},
		{"Dir-neu", mustNewDirFS(path.Relative("tests/")), "testdir_b/"},
		{"Dir-schon vorhanden", mustNewDirFS(path.Relative("tests/")), "testdir_b/"},
		//	{"DirTree", mustNewtreefsDir("C:/tests/"), "testdir_b/"},
	}

	for i, test := range tests {
		err := fs.MkDirAll(test.fs, path.Relative(test.dir))

		if err != nil {
			t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, test.dir, err)
		}

		if !test.fs.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %s: dir %q not created", i, test.descr, test.dir)
		}

		if !path.IsDir(path.Relative(test.dir)) {
			t.Errorf("[%v] %s: %q is not a dir", i, test.descr, test.dir)
		}
	}

}

func TestRmDir(t *testing.T) {
	//t.Skip()
	os.RemoveAll("tests")
	os.Mkdir("tests", 0755)

	tests := []struct {
		descr string
		fs    fs.FS
		dir   string
	}{
		//{"Memory", Memory(), "testdir_a"},
		{"Map", mustNewMapFS("/map/"), "testdir_c/"},
		{"Dir-neu", mustNewDirFS(path.Relative("tests/")), "testdir_f/"},
		//	{"DirTree", mustNewtreefsDir("C:/tests/"), "testdir_b/"},
	}

	for i, test := range tests {
		err := fs.MkDirAll(test.fs, path.Relative(test.dir))

		if err != nil {
			t.Fatalf("[%v] %s: could not create dir %q: %v", i, test.descr, test.dir, err)
		}

		if !test.fs.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %s: dir %q not created", i, test.descr, test.dir)
		}

		if !path.IsDir(path.Relative(test.dir)) {
			t.Errorf("[%v] %s: %q is not a dir", i, test.descr, test.dir)
		}
	}

	//time.Sleep(2 * time.Second)

	for i, test := range tests {
		err := test.fs.Delete(path.Relative(test.dir), false)
		//err := test.fs.Rm(path.MustNew(test.dir), false)

		if err != nil {
			t.Fatalf("[%v] %s: could not remove dir %q: %v", i, test.descr, test.dir, err)
		}

		if test.fs.Exists(path.Relative(test.dir)) {
			t.Errorf("[%v] %s: dir %q is not removed", i, test.descr, test.dir)
		}
	}

}
