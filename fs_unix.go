//go:build !windows

package fs

import (
	"path/filepath"
	"strings"

	"github.com/moby/sys/mountinfo"
	"gitlab.com/golang-utils/fs/path"
	"golang.org/x/sys/unix"
)

func OnUnix() bool {
	return true
}

func OnWindows() bool {
	return false
}

func Drive(loc path.Local) (string, error) {
	/*
		abs := fsys.Abs(rel)
		loc, err := path.ParseLocal(abs.String())
		if err != nil {
			return "", err
		}
	*/
	absPath := path.ToSystem(loc)
	if absPath == "" {
		return "", ErrExpectedAbsPath.Params("")
	}

	absPath, err := filepath.EvalSymlinks(absPath)

	if err != nil {
		return "", err
	}

	var filter mountinfo.FilterFunc = func(info *mountinfo.Info) (skip, stop bool) {
		if info.Mountpoint == absPath {
			return false, true
		}

		if strings.HasPrefix(absPath, info.Mountpoint+"/") {
			return false, false
		}

		return true, false
	}

	infos, err := mountinfo.GetMounts(filter)

	if err != nil {
		return "", err
	}

	if len(infos) == 0 {
		return "", nil
	}

	var length int = 0
	var longest string

	for _, info := range infos {
		if len(info.Mountpoint) > length {
			longest = info.Mountpoint
			length = len(info.Mountpoint)
		}
	}

	return longest, nil
}

// see https://stackoverflow.com/questions/20108520/get-amount-of-free-disk-space-using-go
func FreeSpace(drive string, freeBytesAvailable *uint64) error {
	var stat unix.Statfs_t
	err := unix.Statfs(drive, &stat)

	if err != nil {
		return err
	}

	// Available blocks * size per block = available space in bytes
	*freeBytesAvailable = stat.Bavail * uint64(stat.Bsize)

	return nil
}
