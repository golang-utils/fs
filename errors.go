package fs

import (
	"gitlab.com/golang-utils/errors"
)

var (
	ErrNotSupported      = errors.Error("%s not supported by %T")
	ErrNotFound          = errors.Error("%s not found")
	ErrNotEmpty          = errors.Error("%s not empty")
	ErrAlreadyExists     = errors.Error("%s already exists")
	ErrWhileReading      = errors.Error("could not read %s: %w")
	ErrExpectedFile      = errors.Error("%s is not a file")
	ErrExpectedDir       = errors.Error("%s is not a directory")
	ErrExpectedAbsPath   = errors.Error("%s is no absolute path")
	ErrExpectedWinDrive  = errors.Error("%s is no windows drive letter")
	ErrWhileChecksumming = errors.Error("could not make checksum for file %s: %w")
	ErrChecksumNotMatch  = errors.Error("checksums did not match %v vs %v")
)
