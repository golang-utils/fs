package importer

import (
	"bytes"
	"io"

	"gitlab.com/golang-utils/errors"
	"gitlab.com/golang-utils/fs"
	"gitlab.com/golang-utils/fs/path"
)

type Option func(*importer)

func OptionOverwrite() Option {
	return func(i *importer) {
		i.overwrite = true
	}
}

func OptionContinueOnError() Option {
	return func(i *importer) {
		i.continueOnError = true
	}
}

type importer struct {
	srcFS           fs.ReadOnly
	targetFS        fs.ExtWriteable
	overwrite       bool
	continueOnError bool
	errMap          errors.ErrMap
}

// shouldReturnError tracks the given error, if continueOnError is true and there is not already an error for the given path
// it returns !continueOnError
func (i *importer) shouldReturnError(p path.Relative, e error) (doReturnErr bool) {
	if e == nil {
		return false
	}
	if i.continueOnError {
		if _, has := i.errMap[p.String()]; !has {
			i.errMap[p.String()] = e
		}
	}
	return !i.continueOnError
}

func (i *importer) inLoop(it path.Relative) (err error) {
	if path.IsDir(it) {
		if !i.targetFS.Exists(it) {
			errMk := i.targetFS.Write(it, nil, true)
			if i.shouldReturnError(it, errMk) {
				return errMk
			}
		}
		errDr := i.importDir(it)
		if i.shouldReturnError(it, errDr) {
			return errDr
		}
		return nil
	}

	if i.targetFS.Exists(it) && !i.overwrite {
		errEx := fs.ErrAlreadyExists.Params(it.String())
		if i.shouldReturnError(it, errEx) {
			return errEx
		}
	}

	rd, errRd := i.srcFS.Reader(it)
	// defer rd.Close()

	if i.shouldReturnError(it, errRd) {
		return errRd
	}

	bt, errRd := io.ReadAll(rd)
	rd.Close()

	_ = bt

	if i.shouldReturnError(it, errRd) {
		return errRd
	}

	errWr := i.targetFS.Write(it, fs.ReadCloser(bytes.NewReader(bt)), true)
	if i.shouldReturnError(it, errWr) {
		return errWr
	}

	return nil
}

func (i *importer) importDir(dir path.Relative) error {

	items, err := fs.ReadDirPaths(i.srcFS, dir)

	if err != nil {
		return err
	}

	for _, it := range items {
		err := i.inLoop(it)
		if err != nil {
			return err
		}
	}

	return nil
}

func (i *importer) imp() error {
	if !i.continueOnError {
		return i.importDir(path.Relative(""))
	}

	i.importDir(path.Relative(""))
	if len(i.errMap) == 0 {
		return nil
	}

	return i.errMap
}

// Import imports all files and directories from srcfs to targetfs and stops and returns on the first error.
// On default it stops on the first error and returns it. however by passing OptionContinueOnError, the import continues and
// the collected errors are returned.
// On default it does return an error if a target file already exists, however by passing the option OptionOverwrite this is prevented
func Import(srcfs fs.ReadOnly, targetfs fs.ExtWriteable, opts ...Option) error {
	imp := &importer{
		srcFS:    srcfs,
		targetFS: targetfs,
	}

	for _, opt := range opts {
		opt(imp)
	}

	if imp.continueOnError {
		imp.errMap = errors.ErrMap{}
	}

	return imp.imp()
}
