package path

import (
	"net/url"
	"testing"
)

func TestRemoteJoin(t *testing.T) {

	tests := []struct {
		url      string
		parts    []string
		expected string
	}{
		// 0
		/*
			{"http://www.xyz.com/huhu.txt", "huhu.txt"},
			{"http://www.xyz.com/hoho/huhu.txt", "hoho/huhu.txt"},
			{"http://www.xyz.com/hiho/hoho/huhu.txt", "hiho/hoho/huhu.txt"},
			{"http://www.xyz.com/hiho/hoho/", "hiho/hoho/"},
			{"http://www.xyz.com/", ""},
		*/

		{"http://www.xyz.com/hoho/", []string{"huhu.txt"}, "http://www.xyz.com/hoho/huhu.txt"},
		{"https://www.xyz.com/hiho/hoho/", []string{}, "https://www.xyz.com/hiho/hoho/"},
		{"https://www.xyz.com/hiho/", []string{"hoho/"}, "https://www.xyz.com/hiho/hoho/"},
		{"https://www.xyz.com", []string{"hoho/"}, "https://www.xyz.com/hoho/"},
		{"https://test:hu@www.xyz.com", []string{"huhu.txt"}, "https://test:hu@www.xyz.com/huhu.txt"},
		/*
			{Relative("hiho/"), []string{"hoho/", "huhu.txt"}, "hiho/hoho/huhu.txt"},
			{Relative("hiho/hoho/"), []string{}, "hiho/hoho/"},
			{Relative("hiho/"), []string{"hoho/"}, "hiho/hoho/"},
			{Relative(""), []string{"hoho/"}, "hoho/"},
			{Relative(""), []string{"huhu.txt"}, "huhu.txt"},

			// 6
			{Absolute{"/", "hoho/"}, []string{"huhu.txt"}, "/hoho/huhu.txt"},
			{Absolute{"/", "hiho/"}, []string{"hoho/", "huhu.txt"}, "/hiho/hoho/huhu.txt"},
			{Absolute{"/", "hiho/hoho/"}, []string{}, "/hiho/hoho/"},
			{Absolute{"/", "hiho/"}, []string{"hoho/"}, "/hiho/hoho/"},
			{Absolute{"/", ""}, []string{"hoho/"}, "/hoho/"},
			{Absolute{"/", ""}, []string{"huhu.txt"}, "/huhu.txt"},
		*/
	}

	for i, test := range tests {

		u, err := url.Parse(test.url)

		if err != nil {
			t.Fatalf("can't parse url %q: %v\n", test.url, err)
		}

		u2 := &Remote{u}

		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := u2.Join(test.parts...).String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] URL(%q).Join(%v) returned %q // expected %q", i, test.url, test.parts, got, expected)
		}

	}

	_ = tests
}

func TestRemoteRelative(t *testing.T) {

	tests := []struct {
		url      string
		expected Relative
	}{
		// 0
		{"http://www.xyz.com/huhu.txt", "huhu.txt"},
		{"http://www.xyz.com/hoho/huhu.txt", "hoho/huhu.txt"},
		{"http://www.xyz.com/hiho/hoho/huhu.txt", "hiho/hoho/huhu.txt"},
		{"http://www.xyz.com/hiho/hoho/", "hiho/hoho/"},
		{"http://www.xyz.com/", ""},
	}

	for i, test := range tests {
		u, err := url.Parse(test.url)

		if err != nil {
			t.Fatalf("can't parse url %q: %v\n", test.url, err)
		}

		u2 := &Remote{u}

		got := u2.Relative()
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] &URL{%q}.Relative() returned %q // expected %q", i, test.url, got, expected)
		}

	}

	_ = tests
}

func TestRemoteHead(t *testing.T) {

	tests := []struct {
		url      string
		expected string
	}{
		// 0
		{"http://www.xyz.com/huhu.txt", "http://www.xyz.com/"},
		{"http://testuser@www.xyz.com/hoho/huhu.txt", "http://testuser@www.xyz.com/"},
		{"https://testuser:hi@xyz.com/hiho/hoho/huhu.txt", "https://testuser:hi@xyz.com/"},
		{"ftp://testuser:hi@xyz.com/hiho/hoho/", "ftp://testuser:hi@xyz.com/"},
		{"sftp://www.xyz.com/", "sftp://www.xyz.com/"},
		{"sftp://www.xyz.com", "sftp://www.xyz.com/"},
		{"unknown://127.0.0.1", "unknown://127.0.0.1/"},

		{"http://", "http://"},
		{"/hiho/hoho/", "/"},
		{"hiho/hoho/", "/"},
	}

	for i, test := range tests {
		u, err := url.Parse(test.url)

		if err != nil {
			t.Fatalf("can't parse url %q: %v\n", test.url, err)
		}

		u2 := &Remote{u}

		got := u2.Head()
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] &URL{%q}.Head() returned %q // expected %q", i, test.url, got, expected)
		}

	}

	_ = tests
}

func TestRemoteResolve(t *testing.T) {

	tests := []struct {
		url      string
		relative string
		expected string
	}{
		// 0
		{"http://www.xyz.com/hoho/", "huhu.txt", "http://www.xyz.com/huhu.txt"},
		{"http://www.xyz.com/hiho/hoho/", "", "http://www.xyz.com/"},
		{"http://www.xyz.com/hiho/", "hoho/", "http://www.xyz.com/hoho/"},
		{"http://www.xyz.com/", "hoho/", "http://www.xyz.com/hoho/"},
		{"http://www.xyz.com/", "huhu.txt", "http://www.xyz.com/huhu.txt"},
	}

	for i, test := range tests {
		u, err := url.Parse(test.url)

		if err != nil {
			t.Fatalf("can't parse url %q: %v\n", test.url, err)
		}

		u2 := &Remote{u}

		got := u2.Resolve(Relative(test.relative)).String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] url.Parse(%q).Resolve(%v) returned %q // expected %q", i, test.url, test.relative, got, expected)
		}

	}

	_ = tests
}

func TestRemoteDir(t *testing.T) {

	tests := []struct {
		url      string
		expected string
	}{
		// 0
		{"http://www.xyz.com/huhu.txt", "http://www.xyz.com/"},
		{"http://www.xyz.com/hoho/huhu.txt", "http://www.xyz.com/hoho/"},
		{"http://www.xyz.com/hiho/hoho/huhu.txt", "http://www.xyz.com/hiho/hoho/"},
		{"http://www.xyz.com/hiho/hoho/", "http://www.xyz.com/hiho/"},
		{"http://www.xyz.com/", "http://www.xyz.com/"},
	}

	for i, test := range tests {
		u, err := url.Parse(test.url)

		if err != nil {
			t.Fatalf("can't parse url %q: %v\n", test.url, err)
		}

		u2 := &Remote{u}

		got := u2.Dir().String()
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] &URL{%q}.Dir() returned %q // expected %q", i, test.url, got, expected)
		}

	}

	_ = tests
}
