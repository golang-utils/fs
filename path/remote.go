package path

import (
	"net/url"
	"strings"
)

var (
	_ Path     = &Remote{}
	_ Absolute = &Remote{}
)

// ToRemote creates a Remote path out of an absolute path
func ToRemote(abs Absolute) (*Remote, error) {
	u, err := url.Parse(abs.String())
	if err != nil {
		return nil, err
	}
	return &Remote{u}, nil
}

type Remote struct {
	*url.URL
}

func (u *Remote) Relative() Relative {
	if u.IsAbs() {
		if len(u.Path) < 2 {
			return Relative("")
		}
		return Relative(u.Path[1:])
	}
	return Relative(u.Path)
}

// Join joins based on the relative path
func (u *Remote) Join(parts ...string) *Remote {
	u2 := u.URL.JoinPath(parts...)
	return &Remote{u2}
}

// Resolve resolves in the context of Head()
func (u *Remote) Resolve(r Relative) *Remote {
	return MustRemote(join(u.Head(), r.String()))
}

func (u *Remote) Head() string {
	return headForUrl(u.URL)
}

func (p *Remote) Dir() *Remote {
	str := p.Relative().String()
	if str == "" {
		return MustRemote(p.Head())
	}
	var relStr string
	parts := strings.Split(strings.TrimRight(str, "/"), "/")
	if len(parts) > 1 {
		relStr = strings.Join(parts[:len(parts)-1], "/") + "/"
	}

	return MustRemote(join(p.Head(), relStr))
}

func headForUrl(u *url.URL) string {
	// stolen (and modified) from
	// https://cs.opensource.google/go/go/+/refs/tags/go1.22.1:src/net/url/url.go;l=815
	var buf strings.Builder
	if u.Scheme != "" {
		buf.WriteString(u.Scheme)
		buf.WriteByte(':')
		buf.WriteString("/")
	}
	if u.Opaque != "" {
		buf.WriteString(u.Opaque)
	} else {
		if u.Scheme != "" || u.Host != "" || u.User != nil {
			if u.OmitHost && u.Host == "" && u.User == nil {
				// omit empty host
			} else {
				if u.Host != "" || u.Path != "" || u.User != nil {
					buf.WriteString("/")
				}
				if ui := u.User; ui != nil {
					buf.WriteString(ui.String())
					buf.WriteByte('@')
				}
				if h := u.Host; h != "" {
					buf.WriteString(u.Host)
				}
			}
		}
		path := u.EscapedPath()
		if path != "" && path[0] != '/' && u.Host != "" {
			buf.WriteByte('/')
		}
		if buf.Len() == 0 {
			// RFC 3986 §4.2
			// A path segment that contains a colon character (e.g., "this:that")
			// cannot be used as the first segment of a relative-path reference, as
			// it would be mistaken for a scheme name. Such a segment must be
			// preceded by a dot-segment (e.g., "./this:that") to make a relative-
			// path reference.
			if segment, _, _ := strings.Cut(path, "/"); strings.Contains(segment, ":") {
				buf.WriteString("./")
			}
		}
	}
	res := buf.String()
	if len(res) == 0 {
		return "/"
	}

	return res + "/"
}
