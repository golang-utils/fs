package path

import (
	"path/filepath"
	"strings"
)

var _ Path = Relative("")

// Relative is a relative path, i.e. a path that does neither start with a slash, nor with
// a schema.
type Relative string

// Depth returns the depth of the file / dir relative to its starting position, i.e.
// the number of parent directories.
// Depth return -1 for the root directory
func (r Relative) Depth() int {
	if r.IsRoot() {
		return -1
	}
	s := strings.TrimRight(string(r), "/")
	return len(strings.Split(s, "/")) - 1
}

func (r Relative) IsRoot() bool {
	return string(r) == "" || string(r) == "." || string(r) == "/"
}

// String return the representation of the path as a string.
func (r Relative) String() string {
	return string(r)
}

// Relative fullfills the the Path interface and returns the relative part of the path, which is always itself.
func (r Relative) Relative() Relative {
	return r
}

func (p Relative) Dir() Relative {
	str := p.Relative().String()
	if str == "" || str == "./" {
		return Relative("")
	}
	var relStr string
	parts := strings.Split(strings.TrimRight(str, "/"), "/")
	if len(parts) > 1 {
		relStr = strings.Join(parts[:len(parts)-1], "/") + "/"
	}

	return Relative(relStr)
}

// Join returns a new relative path by joining the old one with
// the given parts. within the parts, directories must end with a slash /
func (r Relative) Join(parts ...string) Relative {
	var all []string
	all = append(all, r.String())
	all = append(all, parts...)
	return Relative(join(all...))
}

// ToGoPath only cuts away slashes at the end and by this
// returns a path as expected inside the io/fs package of the standard library.
func (r Relative) ToGoPath() string {
	str := r.String()
	return strings.TrimRight(str, "/")
}

// ToSystem represents the relative path in the most common way for the current system
// Slashes at the end will be chopped off
func (r Relative) ToSystem() string {
	str := r.String()
	str = strings.TrimRight(str, "/")
	return filepath.FromSlash(str)
}
