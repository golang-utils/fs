package path

import (
	"testing"
)

func TestRelativeJoin(t *testing.T) {

	tests := []struct {
		input    string
		parts    []string
		expected string
	}{
		// 0
		{"hoho/", []string{"huhu.txt"}, "hoho/huhu.txt"},
		{"hiho/hoho/", []string{}, "hiho/hoho/"},
		{"hiho/", []string{"hoho/"}, "hiho/hoho/"},
		{"", []string{"hoho/"}, "hoho/"},
		{"", []string{"huhu.txt"}, "huhu.txt"},
	}

	for i, test := range tests {

		rel := Relative(test.input)
		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := rel.Join(test.parts...).String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Relative(%q).Join(%v) returned %q // expected %q", i, test.input, test.parts, got, expected)
		}

	}

	_ = tests
}

func TestRelativeDir(t *testing.T) {

	tests := []struct {
		input    string
		expected string
	}{
		// 0
		{"hoho/", ""},
		{"hiho/hoho/", "hiho/"},
		{"hiho/hoho/huhu/", "hiho/hoho/"},
		{"hiho/", ""},
		{"hoho/huhu.txt", "hoho/"},
		{"hoho/hihi/huhu.txt", "hoho/hihi/"},
		{"huhu.txt", ""},
	}

	for i, test := range tests {

		rel := Relative(test.input)
		got := rel.Dir().String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Relative(%q).Dir() returned %q // expected %q", i, test.input, got, expected)
		}

	}

	_ = tests
}
