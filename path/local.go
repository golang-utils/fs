package path

import (
	"strings"
)

var (
	_ Path     = Local{}
	_ Absolute = Local{}
)

type Local [2]string

func (a Local) Head() string {
	return a[0]
}

func (a Local) String() string {
	return a[0] + a[1]
}

// RootRelative transforms a local path into a path relative to the rootfs, so that it is usable within rootfs
// it returns a normal relative path on unix
// and on windows a relative path to an imaginary root
// that would reside above the drive letters, so that
// the returned relative path would start with the
// lowercase drive letter, followed by a slash and the normal
// rest of the path
// e.g.
//
//		C:\Windows\Temp  => c/Windows/Temp
//		D:\files         => d/files
//	 /hi/ho           => hi/ho
//
// It is supposed to be used with the rootfs in order to have
// a crossplattform root fs.
func (p Local) RootRelative() Relative {
	if strings.Index(p.Head(), ":") == -1 {
		return p.Relative()
	}

	return Relative(strings.Replace(p.Head(), ":", "", 1) + p.Relative().String())
}

func (a Local) Relative() Relative {
	return Relative(a[1])
}

func (p Local) Dir() Local {
	str := p.Relative().String()
	if str == "" {
		return Local{p.Head(), ""}
	}
	var relStr string
	parts := strings.Split(strings.TrimRight(str, "/"), "/")
	if len(parts) > 1 {
		relStr = strings.Join(parts[:len(parts)-1], "/") + "/"
	}

	return Local{p.Head(), relStr}
}

// Resolve resolves in the context of the path of local
func (a Local) Resolve(r Relative) Local {
	var anew Local
	anew[0] = a[0] + a[1]
	anew[1] = r.String()
	return anew
}

// Join returns a new Absolute that has the same head
// but a tail that is the join of the old tail and the given parts
// within the parts, directories must end with a slash /
func (a Local) Join(parts ...string) Local {
	var all []string
	all = append(all, a[1])
	all = append(all, parts...)
	var anew Local
	anew[0] = a[0]
	anew[1] = join(all...)
	return anew
}
