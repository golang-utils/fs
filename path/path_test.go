package path

import (
	"testing"
)

func TestExt(t *testing.T) {
	//func Ext(p Path) string {
	tests := []struct {
		path     Relative
		expected string
	}{
		{"", "/"},
		{".", "/"},
		{"test", ""},
		{"test/", "/"},
		{"test/hu.txt", ".txt"},
		{"test/hu", ""},
		{"test/hu/", "/"},
	}

	for i, test := range tests {
		got := Ext(test.path)
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Ext(%q) = %v // expected %v", i, test.path, got, expected)
		}
	}
}

func TestDepth(t *testing.T) {
	tests := []struct {
		path     Relative
		expected int
	}{
		{"", -1},
		{".", -1},
		{"test", 0},
		{"test/", 0},
		{"test/hu", 1},
		{"test/hu/", 1},
		{"test/hu/ho", 2},
		{"test/hu/ho/", 2},
	}

	for i, test := range tests {
		got := test.path.Depth()
		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Relative(%q).Depth = %v // expected %v", i, test.path, got, expected)
		}
	}
}

func TestJoin(t *testing.T) {

	tests := []struct {
		input    Path
		parts    []string
		expected string
	}{
		// 0
		{Relative("hoho/"), []string{"huhu.txt"}, "hoho/huhu.txt"},
		{Relative("hiho/"), []string{"hoho/", "huhu.txt"}, "hiho/hoho/huhu.txt"},
		{Relative("hiho/hoho/"), []string{}, "hiho/hoho/"},
		{Relative("hiho/"), []string{"hoho/"}, "hiho/hoho/"},
		{Relative(""), []string{"hoho/"}, "hoho/"},
		{Relative(""), []string{"huhu.txt"}, "huhu.txt"},

		// 6
		{Local{"/", "hoho/"}, []string{"huhu.txt"}, "/hoho/huhu.txt"},
		{Local{"/", "hiho/"}, []string{"hoho/", "huhu.txt"}, "/hiho/hoho/huhu.txt"},
		{Local{"/", "hiho/hoho/"}, []string{}, "/hiho/hoho/"},
		{Local{"/", "hiho/"}, []string{"hoho/"}, "/hiho/hoho/"},
		{Local{"/", ""}, []string{"hoho/"}, "/hoho/"},
		{Local{"/", ""}, []string{"huhu.txt"}, "/huhu.txt"},
	}

	for i, test := range tests {
		all := []string{test.input.String()}
		all = append(all, test.parts...)
		got := join(all...)

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] Relative(%q).Join(%v) returned %q // expected %q", i, test.input, test.parts, got, expected)
		}

	}

	_ = tests
}

func TestName(t *testing.T) {

	tests := []struct {
		input    string
		relative bool
		expected string
	}{
		// 0
		{"huhu.txt", true, "huhu.txt"},
		{"hoho/huhu.txt", true, "huhu.txt"},
		{"hiho/hoho/huhu.txt", true, "huhu.txt"},
		{"hiho/hoho/", true, "hoho"},
		{"", true, ""},

		// 5
		{"huhu.txt", false, "huhu.txt"},
		{"hoho/huhu.txt", false, "huhu.txt"},
		{"hiho/hoho/huhu.txt", false, "huhu.txt"},
		{"hiho/hoho/", false, "hoho"},
		{"", false, ""},
	}

	for i, test := range tests {
		var p Path = Relative(test.input)

		if !test.relative {
			p = Local{"/", test.input}
		}

		isRel := !IsAbs(p)
		if isRel != test.relative {
			t.Errorf("isRel is %v // expected %v\n", isRel, test.relative)
		}

		got := Name(p)
		expected := test.expected

		if got != expected {
			inp := test.input
			if !test.relative {
				inp = "/" + inp
			}
			t.Errorf("[%v] Name(%q) returned %q // expected %q", i, inp, got, expected)
		}

	}

	_ = tests
}

func TestBase(t *testing.T) {

	tests := []struct {
		input    string
		relative bool
		expected string
	}{
		// 0
		{"huhu.txt", true, "huhu.txt"},
		{"hoho/huhu.txt", true, "huhu.txt"},
		{"hiho/hoho/huhu.txt", true, "huhu.txt"},
		{"hiho/hoho/", true, "hoho/"},

		// 4
		{"huhu.txt", false, "huhu.txt"},
		{"hoho/huhu.txt", false, "huhu.txt"},
		{"hiho/hoho/huhu.txt", false, "huhu.txt"},
		{"hiho/hoho/", false, "hoho/"},
	}

	for i, test := range tests {
		got := Base(Relative(test.input))

		if !test.relative {
			got = Base(Local{"/", test.input})
		}

		expected := test.expected

		if got != expected {
			inp := test.input
			if !test.relative {
				inp = "/" + inp
			}
			t.Errorf("[%v] Base(%q) returned %q // expected %q", i, inp, got, expected)
		}

	}

	_ = tests
}

func TestIsDir(t *testing.T) {

	tests := []struct {
		input    string
		relative bool
		expected bool
	}{
		// 0
		{"huhu.txt", true, false},
		{"hoho/huhu.txt", true, false},
		{"hiho/hoho/huhu.txt", true, false},
		{"hiho/hoho/", true, true},
		{"hiho/", true, true},
		{"", true, true},

		// 6
		{"huhu.txt", false, false},
		{"hoho/huhu.txt", false, false},
		{"hiho/hoho/huhu.txt", false, false},
		{"hiho/hoho/", false, true},
		{"hiho/", false, true},
		{"", false, true},
	}

	for i, test := range tests {
		got := IsDir(Relative(test.input))

		if !test.relative {
			got = IsDir(Local{"/", test.input})
		}

		expected := test.expected

		if got != expected {
			inp := test.input
			if !test.relative {
				inp = "/" + inp
			}
			t.Errorf("[%v] IsDir(%q) returned %v // expected %v", i, inp, got, expected)
		}

	}

	_ = tests
}
