package path

import (
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestParseDirFromSystem(t *testing.T) {

	wd, err := os.Getwd()

	if err != nil {
		t.Fatalf("can't get wd: %v\n", err.Error())
	}

	os.RemoveAll(filepath.Join(wd, "tests"))
	os.MkdirAll(filepath.Join(wd, "tests", "a"), 0755)
	os.WriteFile(filepath.Join(wd, "tests", "a", "b.txt"), []byte("hello"), 0644)

	wd = ToSlash(wd) + "/"

	wdabs, _ := filepath.Abs(wd)

	_ = wdabs
	wdloc := MustLocal(wd)
	wddrive := DriveLetter(wdloc)

	tests := []struct {
		input       string
		head        string
		tail        string
		expectError bool
		system      string
	}{
		// 0
		{`.`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{"", wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`./`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`.\`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{wdabs, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{wdabs + "/", wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`./tests`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{`./tests/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{`./tests/a`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/"))},
		{`./tests/a/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/"))},
		{`./tests/a/b.txt`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt/"))},
		{`.\tests`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{`.\tests/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{`.\tests\a`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/"))},
		{`.\tests\a/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/"))},
		{`.\tests\a\b.txt`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt/"))},
		{wdabs + "/tests", wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{wdabs + "/tests/", wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/"))},
		{wdabs + "/tests/a/b.txt", wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt/", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt/"))},
	}

	for i, test := range tests {
		abs, err := ParseDirFromSystem(test.input)

		if err != nil && !test.expectError {
			t.Fatalf("[%v] ParseDirFromSystem(%q) returned an error, but should not: %v", i, test.input, err)
		}

		if err == nil && test.expectError {
			t.Fatalf("[%v] ParseDirFromSystem(%q) did not return an error, but should have", i, test.input)
		}

		if err != nil {
			continue
		}

		if abs.Head() != test.head {
			t.Errorf("[%v] ParseDirFromSystem(%q).Head() returned %q // expected %q", i, test.input, abs.Head(), test.head)
		}

		if abs.Relative().String() != test.tail {
			t.Errorf("[%v] ParseDirFromSystem(%q).Relative() returned %q // expected %q", i, test.input, abs.Relative().String(), test.tail)
		}

		gotLoc := ToSystem(abs)

		if gotLoc != test.system {
			t.Errorf("[%v] ToSystem(ParseDirFromSystem(%q)) returned %q // expected %q", i, test.input, gotLoc, test.system)
		}

	}

	_ = tests
}

func TestParseFileFromSystem(t *testing.T) {

	wd, err := os.Getwd()

	if err != nil {
		t.Fatalf("can't get wd: %v\n", err.Error())
	}

	os.RemoveAll(filepath.Join(wd, "tests"))
	os.MkdirAll(filepath.Join(wd, "tests", "a"), 0755)
	os.WriteFile(filepath.Join(wd, "tests", "a", "b.txt"), []byte("hello"), 0644)

	wd = filepath.ToSlash(wd) + "/"

	wdabs, _ := filepath.Abs(wd)

	_ = wdabs
	wdloc := MustLocal(wd)
	wddrive := DriveLetter(wdloc)

	tests := []struct {
		input       string
		head        string
		tail        string
		expectError bool
		system      string
	}{
		// 0
		{`.`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), true, ToSystem(wdloc)},
		{"", wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), true, ToSystem(wdloc)},
		{`./`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), true, ToSystem(wdloc)},
		{`.\`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), true, ToSystem(wdloc)},
		{wdabs, wddrive + `/`, strings.TrimRight(strings.Join(strings.Split(wd, "/")[1:], "/"), "/"), false, ToSystem(wdloc)},
		{wdabs + "/", wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), true, ToSystem(wdloc)},
		{`./tests`, wddrive + `/`, strings.Join(strings.Split(wd+"tests", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests"))},
		{`./tests/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), true, ToSystem(wdloc.Join("tests/"))},
		{`./tests/a`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a"))},
		{`./tests/a/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), true, ToSystem(wdloc.Join("tests/a/"))},
		{`./tests/a/b.txt`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt"))},
		{`.\tests`, wddrive + `/`, strings.Join(strings.Split(wd+"tests", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests"))},
		{`.\tests/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), true, ToSystem(wdloc.Join("tests/"))},
		{`.\tests\a`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a"))},
		{`.\tests\a/`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/", "/")[1:], "/"), true, ToSystem(wdloc.Join("tests/a/"))},
		{`.\tests\a\b.txt`, wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt"))},
		{wdabs + "/tests", wddrive + `/`, strings.Join(strings.Split(wd+"tests", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests"))},
		{wdabs + "/tests/", wddrive + `/`, strings.Join(strings.Split(wd+"tests/", "/")[1:], "/"), true, ToSystem(wdloc.Join("tests/"))},
		{wdabs + "/tests/a/b.txt", wddrive + `/`, strings.Join(strings.Split(wd+"tests/a/b.txt", "/")[1:], "/"), false, ToSystem(wdloc.Join("tests/a/b.txt"))},
	}

	for i, test := range tests {
		abs, err := ParseFileFromSystem(test.input)

		if err != nil && !test.expectError {
			t.Errorf("[%v] ParseFileFromSystem(%q) returned an error, but should not: %v", i, test.input, err)
			continue
		}

		if err == nil && test.expectError {
			t.Errorf("[%v] ParseFileFromSystem(%q) did not return an error, but should have", i, test.input)
			continue
		}

		if err != nil {
			continue
		}

		if abs.Head() != test.head {
			t.Errorf("[%v] ParseFileFromSystem(%q).Head() returned %q // expected %q", i, test.input, abs.Head(), test.head)
		}

		if abs.Relative().String() != test.tail {
			t.Errorf("[%v] ParseFileFromSystem(%q).Relative() returned %q // expected %q", i, test.input, abs.Relative().String(), test.tail)
		}

		gotLoc := ToSystem(abs)

		if gotLoc != test.system {
			t.Errorf("[%v] ToSystem(ParseFileFromSystem(%q)) returned %q // expected %q", i, test.input, gotLoc, test.system)
		}

	}

	_ = tests
}

/*
ParseLocal

//   accepted:
//
//   - "", `.`, `./` or `.\`: then the current local working directory is parsed and returned
//   - starting with `./` or `.\`: then the rest of the path would be considered to be relative
//     to the local working directory
//   - a local windows path that is starting with a drive letter, followed by a colon and optionally
//     a rest path starting with a separator that can be either the slash / or the backslash \, e.g.
//     `c:`, `G:`, `c:/`, `G:/`, `c:\`, `G:\`, `c:/a.txt`, `G:/b.txt`, `C:\a\b\c\`, `g:/a/b/c/`
//   - a windows network share in UNC notation (must start with two baslslashes \\)
//     optionally followed by a rest path with the above separator, e.g.
//     `\\host\share\`, `\\host/share/`, `\\host\share\sub\dir\`, `\\host/share/sub/dir/`, `\\host\share\file.txt`
//   - an absolute unix path (must start with a slash), e.g.
//     `/`, `/a.txt`, `/a/`, `/a/b/c.txt`
//
// Resulting heads and tails:
//
//   - "", `.`, `./` or `.\`: head is the absolute path of the working directory, tail is empty
//   - starting with `./` or `.\` : head is the absolute path of the working directory, tail is the relative path that is
//     following where backslashes are replaced by slashes, e.g. `.\a\b\c.txt` results in the tail `a/b/c.txt`
//   - a local windows path that is starting with a drive letter: head is the drive letter and tail the rest, e.g.
//     `C:\a\b\c\` would result in a head of `c:/` and a tail of `a/b/c/`
//   - a windows network share in UNC notation would end up with a head of the host and share and the tail would be
//     the rest, e.g.
//     `\\host\share\sub\dir\a.txt`: would result in the head `\\host\share/` and the tail `sub/dir/a.txt`
//   - an absolute unix path would result in the head being the root dir and the tail being the rest, e.g.
//     `/a/b/c.txt` would result in a head of `/` and a tail of `a/b/c.txt`
//

*/

type localTest struct {
	input       string
	head        string
	tail        string
	expectError bool
	system      string
}

func TestParseLocal(t *testing.T) {
	wd, err := os.Getwd()

	if err != nil {
		t.Fatalf("can't get wd: %v\n", err.Error())
	}

	wd = ToSlash(wd) + "/"

	wdloc := MustLocal(wd)
	wddrive := DriveLetter(wdloc)

	tests := getLocalTests(wd, wdloc, wddrive)

	for i, test := range tests {
		abs, err := ParseLocal(test.input)

		if err != nil && !test.expectError {
			t.Fatalf("[%v] ParseLocal(%q) returned an error, but should not: %v", i, test.input, err)
		}

		if err == nil && test.expectError {
			t.Fatalf("[%v] ParseLocal(%q) did not return an error, but should have", i, test.input)
		}

		if err != nil {
			continue
		}

		if abs.Head() != test.head {
			t.Errorf("[%v] ParseLocal(%q).Head() returned %q // expected %q", i, test.input, abs.Head(), test.head)
		}

		if abs.Relative().String() != test.tail {
			t.Errorf("[%v] ParseLocal(%q).Relative() returned %q // expected %q", i, test.input, abs.Relative().String(), test.tail)
		}

		gotLoc := ToSystem(abs)

		if gotLoc != test.system {
			t.Errorf("[%v] ToSystem(ParseLocal(%q)) returned %q // expected %q", i, test.input, gotLoc, test.system)
		}

	}

	_ = tests
}

func TestIsUNC(t *testing.T) {

	tests := []struct {
		input    string
		expected bool
	}{
		// 0

		{``, false},
		{`/`, false},
		{`//`, false},
		{`\\`, false},
		{`\\-`, false},
		{`\\\`, false},

		{`\\share`, false},
		{`\\share\a`, true},
		{`\\share\a\`, true},
		{`\\share\a\b.txt`, true},
		{`\\share/a/b.txt`, true},
		{`\\share/a/`, true},

		{`\\share.net`, false},
		{`\\share.net\a`, true},
		{`\\share.net\a\`, true},
		{`\\share.net\a\b.txt`, true},
		{`\\share.net/a/b.txt`, true},
		{`\\share.net/a/`, true},

		{`\\120.0.0.1`, false},
		{`\\120.0.0.1\a`, true},
		{`\\120.0.0.1\a\`, true},
		{`\\120.0.0.1\a\b.txt`, true},
		{`\\120.0.0.1/a/b.txt`, true},
		{`\\120.0.0.1/a/`, true},
	}

	for i, test := range tests {
		got := isUNC(test.input)

		if got != test.expected {
			t.Errorf("[%v] isUNC(%q) returned %v // expected %v", i, test.input, got, test.expected)
		}

	}

	_ = tests
}

func TestIsWinPath(t *testing.T) {

	tests := []struct {
		input    string
		expected bool
	}{
		// 0

		{``, false},
		{`/`, false},
		{`//`, false},
		{`\\`, false},
		{`\\-`, false},
		{`\\\`, false},

		{`C:`, true},
		{`D:`, true},
		{`c:`, true},
		{`d:`, true},
		{`C:\`, true},
		{`c:\`, true},
		{`D:\`, true},
		{`d:\`, true},
		{`C:/`, true},
		{`c:/`, true},
		{`D:/`, true},
		{`d:/`, true},

		{`C:B`, false},
		{`C:\\`, false},
		{`C://`, false},
		{`C:\B`, true},
		{`C:/B`, true},
		{`C:\B\`, true},
		{`C:/B/`, true},
		{`C:\B\b.txt`, true},
		{`C:/B/b.txt`, true},
	}

	for i, test := range tests {
		got := isWinPath(test.input)

		if got != test.expected {
			t.Errorf("[%v] isWinPath(%q) returned %v // expected %v", i, test.input, got, test.expected)
		}

	}

	_ = tests
}

func TestIsUnixPath(t *testing.T) {

	tests := []struct {
		input    string
		expected bool
	}{
		// 0

		{``, false},
		{`/`, true},
		{`//`, false},
		{`\\`, false},
		{`\\-`, false},
		{`\\\`, false},

		{`C:`, false},
		{`D:`, false},
		{`c:`, false},
		{`d:`, false},
		{`C:\`, false},
		{`c:\`, false},
		{`D:\`, false},
		{`d:\`, false},
		{`C:/`, false},
		{`c:/`, false},
		{`D:/`, false},
		{`d:/`, false},
		{`\\share.net\a`, false},

		{`/C`, true},
		{`/C/b`, true},
		{`/C/b/`, true},
		{`/C/b/c.txt`, true},
	}

	for i, test := range tests {
		got := isUnixPath(test.input)

		if got != test.expected {
			t.Errorf("[%v] isUnixPath(%q) returned %v // expected %v", i, test.input, got, test.expected)
		}

	}

	_ = tests
}

func TestSplitNetshare(t *testing.T) {

	tests := []struct {
		input string
		share string
		rest  string
	}{
		// 0
		{``, ``, ""},
		{`\\`, ``, ""},
		{`\\share`, ``, ""},
		{`\\share/a`, `\\share\a`, ""},
		{`\\share\a`, `\\share\a`, ""},
		{`\\share\a\`, `\\share\a`, ""},
		{`\\share\a\b.txt`, `\\share\a`, "b.txt"},
		{`\\share\a/b.txt`, `\\share\a`, "b.txt"},
		{`\\share\a\b\c.txt`, `\\share\a`, "b/c.txt"},
		{`\\share/a/b/c.txt`, `\\share\a`, "b/c.txt"},
		{`\\share/a/`, `\\share\a`, ""},
	}

	for i, test := range tests {
		gotShare, gotRest := splitNetshare(test.input)

		if gotShare != test.share || gotRest != test.rest {
			t.Errorf("[%v] splitNetshare(%q) returned %q,%q // expected %q,%q", i, test.input, gotShare, gotRest, test.share, test.rest)
		}

	}

	_ = tests
}

func TestNetshare(t *testing.T) {

	tests := []struct {
		input string
		share string
	}{
		// 0
		//	{``, ``},
		//	{`\\`, ``},
		//		{`\\share`, ``},
		{`\\share/a`, `\\share\a`},
		{`\\share\a`, `\\share\a`},
		{`\\share\a\`, `\\share\a`},
		{`\\share\a\b.txt`, `\\share\a`},
		{`\\share\a/b.txt`, `\\share\a`},
		{`\\share\a\b\c.txt`, `\\share\a`},
		{`\\share/a/b/c.txt`, `\\share\a`},
		{`\\share/a/`, `\\share\a`},
	}

	for i, test := range tests {
		got := NetShare(MustLocal(test.input))
		if got != test.share {
			t.Errorf("[%v] NetShare(MustLocal(%q)) returned %q // expected %q", i, test.input, got, test.share)
		}

	}

	_ = tests
}

func TestDrive(t *testing.T) {

	tests := []struct {
		input string
		drive string
	}{
		// 0
		//	{``, ``},
		//	{`\\`, ``},
		//		{`\\share`, ``},
		{`C:`, "c:"},
		{`D:`, "d:"},
		{`c:`, "c:"},
		{`d:`, "d:"},
		{`C:\`, "c:"},
		{`c:\`, "c:"},
		{`D:\`, "d:"},
		{`d:\`, "d:"},
		{`C:/`, "c:"},
		{`c:/`, "c:"},
		{`D:/`, "d:"},
		{`d:/`, "d:"},

		{`C:\B`, "c:"},
		{`C:/B`, "c:"},
		{`C:\B\`, "c:"},
		{`C:/B/`, "c:"},
		{`C:\B\b.txt`, "c:"},
		{`C:/B/b.txt`, "c:"},
	}

	for i, test := range tests {
		got := DriveLetter(MustLocal(test.input))
		if got != test.drive {
			t.Errorf("[%v] Drive(MustLocal(%q)) returned %q // expected %q", i, test.input, got, test.drive)
		}

	}

	_ = tests
}

func TestMustWD(t *testing.T) {

	wd := MustWD().String()
	suff := "fs/path/"
	if !strings.HasSuffix(wd, suff) {
		t.Errorf("wd %q does not have suffix %q", wd, suff)
	}
}
