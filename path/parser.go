package path

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

// ParseRemote parses remote fs paths, i.e. urls (they are always absolute).
// the given url should not have querys, nor fragments
func ParseRemote(u string) (*Remote, error) {
	ur, err := url.ParseRequestURI(u)
	if err != nil {
		return nil, err
	}

	return &Remote{ur}, nil
}

// MustRemote calls ParseRemote, panics if the later returns an error and returns the Remote path otherwise
func MustRemote(u string) *Remote {
	a, err := ParseRemote(u)

	if err != nil {
		panic(err.Error())
	}

	return a
}

/*
TODO: check paths with spaces on windows!!
*/

// MustLocal calls ParseLocal, panics if the later returns an error and returns the Local path otherwise
func MustLocal(abs string) Local {
	a, err := ParseLocal(abs)

	if err != nil {
		panic(err.Error())
	}

	return a
}

func clean2(s string) string {
	return Clean(strings.ReplaceAll(s, `\`, `/`))
}

func Clean(s string) string {
	addSlash := strings.HasSuffix(s, "/")
	nu := path.Clean(s)
	nu = strings.ReplaceAll(nu, "./", "")
	if nu == "." {
		return ""
	}
	if addSlash {
		return nu + "/"
	}
	return nu
}

// MustDirFromSystem is like ParseDirFromSystem, just that it panics on error
func MustDirFromSystem(p string) (l Local) {
	l, err := ParseDirFromSystem(p)
	if err != nil {
		panic(err.Error())
	}
	return l
}

// MustFileFromSystem is like ParseFileFromSystem, just that it panics on error
func MustFileFromSystem(p string) (l Local) {
	l, err := ParseFileFromSystem(p)
	if err != nil {
		panic(err.Error())
	}
	return l
}

// ParseDirFromSystem parses an existing local path that must be a directory
// the path might be absolute or relative (than it is relative to the current working directory)
// the given path might end in a slash or not.
// the resultig Local will end in a slash
// if p is an empty string, or a . the working directory is chosen
func ParseDirFromSystem(p string) (l Local, err error) {
	if len(p) == 0 || p == "." {
		return ParseWD()
	}

	p = strings.TrimRight(p, `/\`)

	var str string
	str, err = filepath.Abs(p)

	if err != nil {
		return
	}

	return ParseLocal(str + "/")
}

// ParseFileFromSystem parses an existing local path that must be a file
// the path might be absolute or relative (than it is relative to the current working directory)
// the given path must not end in a slash.
// if p must not be an empty string, or .
func ParseFileFromSystem(p string) (l Local, err error) {
	if len(p) == 0 || p == "." {
		return l, fmt.Errorf("invalid file path %q (must not be empty or .)", p)
	}

	if strings.HasSuffix(p, "/") || strings.HasSuffix(p, `\`) {
		return l, fmt.Errorf("invalid file path %q (must not end in /)", p)
	}

	var str string
	str, err = filepath.Abs(p)

	if err != nil {
		return
	}

	return ParseLocal(str)
}

// ParseLocal parses a local fs path. The returned path is a Local, which in turn is an absolute path.
// If the given path does not have the correct syntax, ParseLocal will return an error.
// Syntax
// A path that is a directory must always end in a separator, which is the slash / on unix and might be
// a slash / or backslash \ on windows systems.
// path might be one of the following:
//   - "", `.`, `./` or `.\`: then the current local working directory is parsed and returned
//   - starting with `./` or `.\`: then the rest of the path would be considered to be relative
//     to the local working directory
//   - a local windows path that is starting with a drive letter, followed by a colon and optionally
//     a rest path starting with a separator that can be either the slash / or the backslash \, e.g.
//     `c:`, `G:`, `c:/`, `G:/`, `c:\`, `G:\`, `c:/a.txt`, `G:/b.txt`, `C:\a\b\c\`, `g:/a/b/c/`
//   - a windows network share in UNC notation (must start with two baslslashes \\)
//     optionally followed by a rest path with the above separator, e.g.
//     `\\host\share\`, `\\host/share/`, `\\host\share\sub\dir\`, `\\host/share/sub/dir/`, `\\host\share\file.txt`
//   - an absolute unix path (must start with a slash), e.g.
//     `/`, `/a.txt`, `/a/`, `/a/b/c.txt`
//
// Resulting heads and tails:
// Since a Local is just a fixed array of two strings, where the first one is the absolute head and
// the second on is the relative tail to that here are some examples, how the resulting heads and tails would be.
//   - "", `.`, `./` or `.\`: head is the absolute path of the working directory, tail is empty
//   - starting with `./` or `.\` : head is the absolute path of the working directory, tail is the relative path that is
//     following where backslashes are replaced by slashes, e.g. `.\a\b\c.txt` results in the tail `a/b/c.txt`
//   - a local windows path that is starting with a drive letter: head is the drive letter and tail the rest, e.g.
//     `C:\a\b\c\` would result in a head of `c:/` and a tail of `a/b/c/`
//   - a windows network share in UNC notation would end up with a head of the host and share and the tail would be
//     the rest, e.g.
//     `\\host\share\sub\dir\a.txt`: would result in the head `\\host\share/` and the tail `sub/dir/a.txt`
//   - an absolute unix path would result in the head being the root dir and the tail being the rest, e.g.
//     `/a/b/c.txt` would result in a head of `/` and a tail of `a/b/c.txt`
//
// The plattform specific string notation of a Local (for the compiled plattform) can always be returned via Local.ToSystem()
func ParseLocal(path string) (a Local, err error) {
	//ParseWD

	switch {
	case len(path) == 0:
		return ParseWD()
	case len(path) == 1:
		if path == "." {
			return ParseWD()
		}
	case path[0:2] == "./" || path[0:2] == `.\`:
		wd, err := ParseWD()
		if err != nil {
			return wd, err
		}
		if len(path) == 2 {
			return wd, nil
		}
		if len(path) > 2 {
			return wd.Join(clean2(path[2:])), nil
		}
	case path[0:2] == "..":
		wd, err := ParseWD()
		if err != nil {
			return wd, err
		}

		hasSlash := strings.HasSuffix(path, "/")

		path = filepath.Clean(filepath.Join(ToSystem(wd), path))
		path, err = filepath.Abs(path)
		if err != nil {
			return wd, err
		}

		if hasSlash {
			path += "/"
		}
	}

	s := ToSlash(path)
	switch {
	case isUNC(path):
		share, rest := splitNetshare(path)
		ToSlash(rest)
		a[0] = share + "/"
		a[1] = Clean(rest)
	case isUnixPath(s):
		a[0] = "/"
		a[1] = Clean(s[1:])
	case isWinPath(s):
		a[0] = strings.ToLower(s[:2] + "/")
		if len(s) > 3 {
			a[1] = Clean(s[3:])
		}
	default:
		err = ErrInvalidPath.Params(path)
		return
	}

	return
}

// DriveLetter returns the drive letter, followed by a : for a windows absolute path and an empty string for non windows paths
func DriveLetter(a Local) string {
	if isWinPath(a.Head()) {
		return strings.ToLower(a.Head()[:2])
	}
	return ""
}

// splitNetshare splits the raw string into the share
// (everything up to the first slash or backslash)
// and the rest (with slashes instead of backslashes)
func splitNetshare(raw string) (share string, rest string) {
	if !isUNC(raw) {
		return "", ""
	}

	r := ToSlash(raw[2:])

	arr := strings.SplitN(r, "/", 3)

	if len(arr) < 2 {
		return "", ""
	}

	share = `\\` + arr[0] + `\` + arr[1]

	if len(arr) == 2 {
		return
	}

	rest = arr[2]
	return
}

// NetShare returns the network share for a windows UNC absolute path and an empty string otherwise
func NetShare(a Local) string {
	sh, _ := splitNetshare(a.Head())
	return sh
}

// WD returns the working directory as an absolute path
// Local calls ParseLocal, panics if the later returns an error and returns the Absolute path otherwise
func MustWD() Local {
	a, err := ParseWD()

	if err != nil {
		panic(err.Error())
	}

	return a
}

func ParseWD() (a Local, err error) {
	dir, err2 := os.Getwd()
	if err2 != nil {
		err = err2
		return
	}

	ab, err3 := filepath.Abs(dir)
	if err3 != nil {
		err = err3
		return
	}

	return ParseLocal(ab + "/")
}

/*
	TODO do name validation (complete it)
*/

func Validate(name string) error {
	switch {
	case strings.Contains(name, "/"):
		return ErrInvalidName.Params(name)
	}
	return nil
}

func IsValid(name string) bool {
	return Validate(name) == nil
}

// ToLocal creates a Local path out of an absolute path
func ToLocal(abs Absolute) Local {
	var l Local
	l[0] = abs.Head()
	l[1] = abs.Relative().String()
	return l
}

// ToSystem transforms the given Path into
// the most common string notation for the running system.
// (e.g. directories without ending slashes, backslashes instead of slashes as separators etc)
// For Local paths it returns the absolute path.
// For Relative path, it returns the relative path.
// For everything else, it it calling the Relative method to transform it into a relative path
// and uses this relative paths transformation.
// The resulting path is a canoncial cleaned path of the running system, without "jumps" (like ./ and ../).
func ToSystem(p Path) string {
	switch v := p.(type) {
	case Local:
		return v.ToSystem()
	case Relative:
		return v.ToSystem()
	default:
		return v.Relative().ToSystem()
	}
}

// ToSystem transforms the given Local path into
// the most common string notation for the running system.
// That means that the final path-seperator for a directory is dropped. Also the resulting string is absolute / complete
// and does not contain jumpers like . and .. (it is cleaned)
// Unix system paths are returned as is, just without a final /, if it is a directory
// Windows system paths are transformed by replacing the / with a backslash \ e.g.
// C:\a\b\c   (the final backslash would also be dropped, if it is a directory)
// Windows UNC paths are also transformed by replacing the / with a backslash \ e.g.
// \\myshare\a\b\c
// if a is non of the supported paths, an empty string is returned
func (a Local) ToSystem() string {
	switch {
	case isUnixPath(a.Head()):
		s := a.String()
		switch s {
		case "":
			return ""
		case "/":
			return "/"
		default:
			if s[len(s)-1] == '/' {
				return s[:len(s)-1]
			}
			return s
		}
	case isUNC(a.Head()):
		s := a.String()
		bs := filepath.FromSlash(s)
		if len(bs) > 3 && bs[len(bs)-1] == '\\' {
			return bs[:len(bs)-1]
		}
		return bs
	case isWinPath(a.Head()):
		s := a.String()
		bs := filepath.FromSlash(s)
		if len(bs) > 3 && bs[len(bs)-1] == '\\' {
			return bs[:len(bs)-1]
		}
		return bs
	default:
		return ""
	}
}

//var regexpWinUNC = regexp.MustCompile("^" + regexp.QuoteMeta(`\\`) + "[a-zA-Z]+")

/*
 UNC                = "\\" host-name "\" share-name  [ "\" object-name ]
 host-name          = IPv6address / IPv4address / reg-name
    ; IPv4address, and reg-name are as specified in [RFC3986], see following for IPV6
 share-name         = 1*80pchar
 pchar              = %x20-21 / %x23-29 / %x2D-2E / %x30-39 / %x40-5A / %x5E-7B / %x7D-FF
 object-name        = *path-name [ "\" file-name ]
 path-name          = 1*255pchar
 file-name          = 1*255fchar [ ":" stream-name [ ":" stream-type ] ]
 fchar              = %x20-21 / %x23-29 / %x2B-2E / %x30-39 / %x3B / %x3D / %x40-5B / %x5D-7B / %x7D-FF
 stream-name        = *schar
 schar              = %x01-2E / %x30-39 / %x3B-5B /%x5D-FF
 stream-type        = 1*schar
*/

// basically, a UNC is defined as
// "\\" host-name "\" share-name then optionally followed by [ "\" object-name ]
// we don't care about the oject-name and leave it to the implementation of the corresponding filesystem to deal with that.
// so for us, a UNC is represented as
// "\\" host-name "\" share-name "/"
// and the rest is the usual relative path
// - the hostname follows the same spec as url hosts, so we just use the url parser to verify it.
// - the sharename we don't really check, as long as it has no \
func isUNC(head string) bool {
	if len(head) < 5 {
		return false
	}

	//fmt.Println(head[:2])

	if head[:2] != `\\` {
		return false
	}

	//	fmt.Println(head[2:])

	h := head[2:]

	h2 := strings.ReplaceAll(h, `/`, `\`)
	arr := strings.SplitN(h2, `\`, 3)

	if len(arr) < 2 {
		return false
	}

	u, err := url.Parse("http://" + arr[0])

	if err != nil {
		return false
	}

	if u.Host != arr[0] {
		return false
	}

	if len(arr[1]) < 1 {
		return false
	}

	//return strings.HasPrefix(head, `\\`)
	return true
}

var regexpWinHead = regexp.MustCompile("^[a-zA-Z]" + regexp.QuoteMeta(":"))
var regexpWinHead2 = regexp.MustCompile("^[a-zA-Z]" + regexp.QuoteMeta(`:/`))
var regexpWinHead3 = regexp.MustCompile("^[a-zA-Z]" + regexp.QuoteMeta(`:/`) + "[^" + regexp.QuoteMeta(`/`) + "]+")

func isWinPath(head string) bool {
	h := ToSlash(head)
	if len(h) > 3 {
		return regexpWinHead3.MatchString(h)
	}
	if len(h) > 2 {
		return regexpWinHead2.MatchString(h)
	}
	return regexpWinHead.MatchString(h)
}

func isUnixPath(head string) bool {
	if strings.HasPrefix(head, `//`) {
		return false
	}
	return strings.HasPrefix(head, `/`)
}

/*
ParseWindows gets an absolut path
func ParseWindows(abs string) (Absolute, error) {
	// TODO: parse, panics if not valid
	return Absolute{}, nil
}

func ParseWindowsNetworkShare(s string) (Absolute, error) {
	// TODO: parse, panics if not valid
	return Absolute{}, nil
}
*/
