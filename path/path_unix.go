//go:build !windows

package path

import (
	"strings"
)

func ToSlash(in string) string {
	return strings.ReplaceAll(in, `\`, `/`)
}
