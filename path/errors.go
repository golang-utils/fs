package path

import (
	"gitlab.com/golang-utils/errors"
)

var (
	ErrInvalidPath = errors.Error("invalid path: %s")
	ErrInvalidName = errors.Error("invalid name: %s")
)
