package path

import (
	"path/filepath"
	"strings"
)

type Path interface {
	String() string
	Relative() Relative
}

type Absolute interface {
	Path
	Head() string
}

func IsAbs(p Path) bool {
	_, ok := p.(Absolute)
	return ok
}

// returns the name (the part after the last slash)
// if p is a directory, the last slash will not be part of the name
// that means, that joining Dir() and Name() will not be a directory name, but a filename
// However: Base(p Path) returns the last part, i.e. for directories including the / at the end
// so joining Dir() and Base() will give the complete relative path
func Name(p Path) string {
	rel := p.Relative().String()
	if rel == "" {
		return ""
	}
	all := strings.Split(rel, "/")

	/*
		if len(all) == 0 {
			return ""
		}
	*/

	if IsDir(p) {
		return all[len(all)-2]
	}

	return all[len(all)-1]
}

// Ext returns the extension of the given path.
// If the path has no extension, it returns the empty string
// If the path is a directory, it returns the slash /
// Otherwise it returns the extension, as filepath.Ext (i.e. with prefixed dot)
func Ext(p Path) string {
	if IsDir(p) {
		return "/"
	}

	return filepath.Ext(ToSystem(p.Relative()))
}

// However: Base(p Path) returns the last part, i.e. for directories including the / at the end
// so joining Dir() and Base() will give the complete relative path
func Base(p Path) string {
	name := Name(p)
	if IsDir(p) {
		return name + "/"
	}

	return name
}

// returns true, if the path ends in a slash, is empty or is a dot
func IsDir(p Path) bool {
	rel := p.Relative().String()
	switch rel {
	case "", ".", "/":
		return true
	}

	return rel[len(rel)-1] == '/'
}

// join joins the given parts to a new path.
// In this case the parts are given as a String and the resulting path is also
// given as a String and can be easily converted to a relative of absolute path.
// within the parts, directories must end with a slash /
func join(parts ...string) string {
	return strings.Join(parts, "")
}
