//go:build windows

package path

import (
	"path/filepath"
)

func ToSlash(in string) string {
	return filepath.ToSlash(in)
}
