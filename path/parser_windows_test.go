//go:build windows

package path

import (
	//	"os"
	"path/filepath"
	"strings"
	// "testing"
)

func getLocalTests(wd string, wdloc Local, wddrive string) []localTest {
	//wdloc := MustLocal(wd)
	//wddrive := DriveLetter(wdloc)

	return []localTest{
		// 0
		{`.`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{"", wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`./`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`.\`, wddrive + `/`, strings.Join(strings.Split(wd, "/")[1:], "/"), false, ToSystem(wdloc)},
		{`./hu`, wddrive + `/`, strings.Join(strings.Split(wd+"hu", "/")[1:], "/"), false, ToSystem(wdloc.Join("hu"))},
		{`.\hu`, wddrive + `/`, strings.Join(strings.Split(wd+"hu", "/")[1:], "/"), false, ToSystem(wdloc.Join("hu"))},
		{`./hu/ho/`, wddrive + `/`, strings.Join(strings.Split(wd+"hu/ho/", "/")[1:], "/"), false, ToSystem(wdloc.Join("hu/ho/"))},
		{`./hu/ho.txt`, wddrive + `/`, strings.Join(strings.Split(wd+"hu/ho.txt", "/")[1:], "/"), false, ToSystem(wdloc.Join("hu/ho.txt"))},

		{`/`, `/`, ``, false, `/`},
		{`/.`, `/`, ``, false, `/`},
		{`/hu/ho/../hey/./hach`, `/`, `hu/hey/hach`, false, `/hu/hey/hach`},
		{`/hu/ho/../hey/./hach/`, `/`, `hu/hey/hach/`, false, `/hu/hey/hach`},
		{`/huho`, `/`, `huho`, false, `/huho`},
		{`/huho/ha.txt`, `/`, `huho/ha.txt`, false, `/huho/ha.txt`},
		{`/hu/ho/ha.txt`, `/`, `hu/ho/ha.txt`, false, `/hu/ho/ha.txt`},
		{`/hu/ho/`, `/`, `hu/ho/`, false, `/hu/ho`},
		{`/hu/ho ha/`, `/`, `hu/ho ha/`, false, `/hu/ho ha`},

		{`C:`, `c:/`, ``, false, `c:\`},
		{`c:`, `c:/`, ``, false, `c:\`},
		{`C:\`, `c:/`, ``, false, `c:\`},
		{`c:/`, `c:/`, ``, false, `c:\`},
		{`C:\B`, `c:/`, `B`, false, `c:\B`},
		{`C:\B\`, `c:/`, `B/`, false, `c:\B`},
		{`c:/B`, `c:/`, `B`, false, `c:\B`},
		{`c:/B/`, `c:/`, `B/`, false, `c:\B`},
		{`C:\B\b.txt`, `c:/`, `B/b.txt`, false, `c:\B\b.txt`},
		{`C:/B/b.txt`, `c:/`, `B/b.txt`, false, `c:\B\b.txt`},
		{`C:/B c/b.txt`, `c:/`, `B c/b.txt`, false, `c:\B c\b.txt`},
		{`C:\B c\b.txt`, `c:/`, `B c/b.txt`, false, `c:\B c\b.txt`},
		{`C:/hu/ho/../hey/./hach`, `c:/`, `hu/hey/hach`, false, `c:\hu\hey\hach`},
		{`C:/hu/ho/../hey/./hach/`, `c:/`, `hu/hey/hach/`, false, `c:\hu\hey\hach`},
		{`C:\hu\ho\..\hey\.\hach`, `c:/`, `hu/hey/hach`, false, `c:\hu\hey\hach`},
		{`C:\hu\ho\..\hey\.\hach\`, `c:/`, `hu/hey/hach/`, false, `c:\hu\hey\hach`},

		{`\\share.net\a`, `\\share.net\a/`, ``, false, `\\share.net\a`},
		{`\\share.net\a\`, `\\share.net\a/`, ``, false, `\\share.net\a`},
		{`\\share.net/a/`, `\\share.net\a/`, ``, false, `\\share.net\a`},
		{`\\share.net\a\b\`, `\\share.net\a/`, `b/`, false, `\\share.net\a\b`},
		{`\\share.net/a/b/`, `\\share.net\a/`, `b/`, false, `\\share.net\a\b`},
		{`\\share.net\a\b.txt`, `\\share.net\a/`, `b.txt`, false, `\\share.net\a\b.txt`},
		{`\\share.net/a/b.txt`, `\\share.net\a/`, `b.txt`, false, `\\share.net\a\b.txt`},

		{`\\share.net/a/hu/ho/../hey/./hach`, `\\share.net\a/`, `hu/hey/hach`, false, `\\share.net\a\hu\hey\hach`},
		{`\\share.net/a/hu/ho/../hey/./hach/`, `\\share.net\a/`, `hu/hey/hach/`, false, `\\share.net\a\hu\hey\hach`},
		{`\\share.net\a\hu\ho\..\hey\.\hach`, `\\share.net\a/`, `hu/hey/hach`, false, `\\share.net\a\hu\hey\hach`},
		{`\\share.net\a\hu\ho\..\hey\.\hach\`, `\\share.net\a/`, `hu/hey/hach/`, false, `\\share.net\a\hu\hey\hach`},

		{`\\120.0.0.1\a`, `\\120.0.0.1\a/`, ``, false, `\\120.0.0.1\a`},
		{`\\120.0.0.1\a\`, `\\120.0.0.1\a/`, ``, false, `\\120.0.0.1\a`},
		{`\\120.0.0.1\a\b.txt`, `\\120.0.0.1\a/`, `b.txt`, false, `\\120.0.0.1\a\b.txt`},
		{`\\120.0.0.1/a/b.txt`, `\\120.0.0.1\a/`, `b.txt`, false, `\\120.0.0.1\a\b.txt`},
		{`\\120.0.0.1/a/`, `\\120.0.0.1\a/`, ``, false, `\\120.0.0.1\a`},

		{`../hu`,
			wddrive + `/`,
			strings.Join(strings.Split(filepath.ToSlash(filepath.Clean(filepath.Join(wd, "../hu"))), "/")[1:], "/"),
			false,
			filepath.Clean(ToSystem(wdloc.Join("../hu"))),
		},

		{`../ho/`,
			wddrive + `/`,
			strings.Join(strings.Split(filepath.ToSlash(filepath.Clean(filepath.Join(wd, "../ho"))), "/")[1:], "/") + "/",
			false,
			filepath.Clean(ToSystem(wdloc.Join("../ho/"))),
		},

		// errors
		{`C:b`, ``, ``, true, ``},
		{`\\120.0.0.1`, ``, ``, true, ``},
		{`//`, ``, ``, true, ``},
		{`b`, ``, ``, true, ``},
	}
}
