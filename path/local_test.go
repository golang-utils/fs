package path

import (
	"testing"
)

func TestLocalJoin(t *testing.T) {

	tests := []struct {
		input    string
		parts    []string
		expected string
	}{
		// 0
		{"/hoho/", []string{"huhu.txt"}, "/hoho/huhu.txt"},
		{"/hiho/hoho/", []string{}, "/hiho/hoho/"},
		{"/hiho/", []string{"hoho/"}, "/hiho/hoho/"},
		{"/", []string{"hoho/"}, "/hoho/"},
		{"/", []string{"huhu.txt"}, "/huhu.txt"},

		{"C:/hoho/", []string{"huhu.txt"}, "c:/hoho/huhu.txt"},
		{"C:/hiho/hoho/", []string{}, "c:/hiho/hoho/"},
		{"C:/hiho/", []string{"hoho/"}, "c:/hiho/hoho/"},
		{"C:/", []string{"hoho/"}, "c:/hoho/"},
		{"C:/", []string{"huhu.txt"}, "c:/huhu.txt"},
		{`C:\`, []string{"huhu.txt"}, "c:/huhu.txt"},
		/*
			{Relative("hiho/"), []string{"hoho/", "huhu.txt"}, "hiho/hoho/huhu.txt"},
			{Relative("hiho/hoho/"), []string{}, "hiho/hoho/"},
			{Relative("hiho/"), []string{"hoho/"}, "hiho/hoho/"},
			{Relative(""), []string{"hoho/"}, "hoho/"},
			{Relative(""), []string{"huhu.txt"}, "huhu.txt"},

			// 6
			{Absolute{"/", "hoho/"}, []string{"huhu.txt"}, "/hoho/huhu.txt"},
			{Absolute{"/", "hiho/"}, []string{"hoho/", "huhu.txt"}, "/hiho/hoho/huhu.txt"},
			{Absolute{"/", "hiho/hoho/"}, []string{}, "/hiho/hoho/"},
			{Absolute{"/", "hiho/"}, []string{"hoho/"}, "/hiho/hoho/"},
			{Absolute{"/", ""}, []string{"hoho/"}, "/hoho/"},
			{Absolute{"/", ""}, []string{"huhu.txt"}, "/huhu.txt"},
		*/
	}

	for i, test := range tests {

		loc := MustLocal(test.input)
		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := loc.Join(test.parts...).String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] MustLocal(%q).Join(%v) returned %q // expected %q", i, test.input, test.parts, got, expected)
		}

	}

	_ = tests
}

func TestLocalHead(t *testing.T) {

	tests := []struct {
		input    string
		expected string
	}{
		// 0
		{"/", "/"},
		{"/home", "/"},
		{"/home/", "/"},
		{"C:/", "c:/"},
		{"C:/home", "c:/"},
		{"C:/home/", "c:/"},
		{"D:/", "d:/"},
	}

	for i, test := range tests {

		loc := MustLocal(test.input)
		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := loc.Head()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] MustLocal(%q).Head() returned %q // expected %q", i, test.input, got, expected)
		}

	}

	_ = tests
}

func TestLocalResolve(t *testing.T) {

	tests := []struct {
		input    string
		relative string
		expected string
	}{
		// 0
		{"/hoho/", "huhu.txt", "/hoho/huhu.txt"},
		{"/hiho/hoho/", "", "/hiho/hoho/"},
		{"/hiho/", "hoho/", "/hiho/hoho/"},
		{"/", "hoho/", "/hoho/"},
		{"/", "huhu.txt", "/huhu.txt"},

		{"C:/hoho/", "huhu.txt", "c:/hoho/huhu.txt"},
		{"C:/hiho/hoho/", "", "c:/hiho/hoho/"},
		{"C:/hiho/", "hoho/", "c:/hiho/hoho/"},
		{"C:/", "hoho/", "c:/hoho/"},
		{"C:/", "huhu.txt", "c:/huhu.txt"},
		/*
			{Relative("hiho/"), []string{"hoho/", "huhu.txt"}, "hiho/hoho/huhu.txt"},
			{Relative("hiho/hoho/"), []string{}, "hiho/hoho/"},
			{Relative("hiho/"), []string{"hoho/"}, "hiho/hoho/"},
			{Relative(""), []string{"hoho/"}, "hoho/"},
			{Relative(""), []string{"huhu.txt"}, "huhu.txt"},

			// 6
			{Absolute{"/", "hoho/"}, []string{"huhu.txt"}, "/hoho/huhu.txt"},
			{Absolute{"/", "hiho/"}, []string{"hoho/", "huhu.txt"}, "/hiho/hoho/huhu.txt"},
			{Absolute{"/", "hiho/hoho/"}, []string{}, "/hiho/hoho/"},
			{Absolute{"/", "hiho/"}, []string{"hoho/"}, "/hiho/hoho/"},
			{Absolute{"/", ""}, []string{"hoho/"}, "/hoho/"},
			{Absolute{"/", ""}, []string{"huhu.txt"}, "/huhu.txt"},
		*/
	}

	for i, test := range tests {

		loc := MustLocal(test.input)
		//all := []string{test.input.String()}
		//all = append(all, test.parts...)
		got := loc.Resolve(Relative(test.relative)).String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] MustLocal(%q).Resolve(%v) returned %q // expected %q", i, test.input, test.relative, got, expected)
		}

	}

	_ = tests
}

func TestLocalDir(t *testing.T) {

	tests := []struct {
		input    string
		expected string
	}{
		// 0
		{"/hoho/", "/"},
		{"/hiho/hoho/", "/hiho/"},
		{"/hiho/hoho.txt", "/hiho/"},
		{"/hiho/haha/hoho.txt", "/hiho/haha/"},
		{"/hiho/", "/"},
		{"/", "/"},
		{"/", "/"},

		{"C:/hoho/", "c:/"},
		{"C:/hiho/hoho/", "c:/hiho/"},
		{"C:/hiho/", "c:/"},
		{"C:/", "c:/"},
		{"C:/", "c:/"},
	}

	for i, test := range tests {

		loc := MustLocal(test.input)
		got := loc.Dir().String()

		expected := test.expected

		if got != expected {
			t.Errorf("[%v] MustLocal(%q).Dir() returned %q // expected %q", i, test.input, got, expected)
		}

	}

	_ = tests
}
