package fs

import (
	"io"
	"time"

	"gitlab.com/golang-utils/fs/path"
)

/*
As a user and implementer, you should mainly care about and understand the basic interfaces,
that are:

ReadOnly
FS
Local
Remote
TestFS

From a users perspective:

- If you know that you are only dealing with local or remote data, you should use either Local or Remote.
- If you want to deal independent of the fs being remote or local, choose FS
- If you only need to read files, choose ReadOnly (which is compatible with io.FS)
- Take only what you need: try to go with ReadOnly unless you need deleting or renaming or write access,
  then go with FS. If you really need local or remote properties, take one of them.
- If you just need some additional properties on top of a basic interface, you might want to
  define an own interface, based on the basic one that is the most appropriate and the needed extension interfaces (Ext...)
- Only if you want to run tests against all possible interfaces and extensions, you might want to use the TestFS interface.

From a developers perspective:

- The easiest to implement (least requirements) is ReadOnly.
- implementing FS is the common middle ground that is recommended.
- if you have a local fs you should strive to implement Local, for a remote fs Remote
- always try to implement the most for your users (Local or Remote)
- only if you want to implement a general purpose test filesystem that should be able to run against all possible interfaces and extensions, you might want to implement the TestFS interface.

*/

// ReadOnly is a minimal readonly interface for a filesystem.
// Any "io/fs.FS" can be converted to the ReadOnly interface by calling wrapfs.New()
type ReadOnly interface {
	// Reader returns an io.ReadCloser that reads in the whole file or in case of a directory
	// the relative paths related to the directory path, separated by a unix linefeed (\n).
	// The io.ReadCloser should be closed after reading all.
	// An error is only returned, if p does not exist or in case of directories, if reading of directories is not supported.
	Reader(p path.Relative) (io.ReadCloser, error)

	// Exists does return, if a file or directory with the given path exists.
	Exists(p path.Relative) bool

	// ModTime return the time of the last modification of p.
	// It returns an error, if p does not exist.
	ModTime(p path.Relative) (time.Time, error) // wenn nicht unterstützt: immer fehler zurückgeben

	// Abs converts the given relative path to an absolute path, based on the base of the filesystem.
	Abs(p path.Relative) path.Absolute

	// Size returns the size of the given p in bytes
	// If the given p does not exist or the size could not be determined, -1 is returned.
	// The size of a folder is always 0. In order to calculate the real size of a folder,
	// use DirSize()
	Size(p path.Relative) int64 // int64 is large enough for any filesystem
}

// FS is an interface for a complete filesystem, that covers the ability to
// read, write, delete and rename files and folders.
type FS interface {
	ReadOnly
	ExtWriteable
	ExtDeleteable
}

// Local is the interface for a local filesystem
type Local interface {
	FS
	ExtReadSeekable
	ExtWriteSeekable
	ExtMoveable
	ExtModeable
	ExtRenameable
	ExtSpaceReporter
	ExtGlob
}

// Remote is the interface for a remote filesystem
type Remote interface {
	FS
	ExtMeta
	ExtURL
}

// TestFS is an interface that is only interesting for testing.
// It includes all interfaces and extension.
type TestFS interface {
	Local
	Remote
}
